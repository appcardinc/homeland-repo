/****************************************************************************/
/*                                                                          */
/*  Filename   : GNPTMAIN.CPP                                               */
/*                                                                          */
/*  Description: Migrate S&H GreenPoints to ACE                             */
/*                                                                          */
/*  Author:      JAI                                                        */
/*                                                                          */
/*  Date:        8-27-02                                                    */
/****************************************************************************/
/****************************************************************************/
/*                                                                          */
/*  CHANGES: Start                                                          */
/*   SNRV0002 29-Aug-07 'BJV' OPTION protect S&H Greenpoints code           */
/*   SHGR0007 11-Feb-09 'CME' Customer Display Info; M message added to     */
/*                            display a 2x20 message at anypoint in the     */
/*                            transaction.; Using any item code with the C  */
/*                            message.                                      */
/*   SHGR0012 22-Apr-10 'CME' Adding GS1 Barcode support                    */
/*   D06569   20-May-10 'CME' When doing an overide IOData didn't get loaded*/
/*   D07952   29-Sep-10 'CME' After a MFR or Store CPN from SH is played    */
/*                            then if an item is scanned the message 173    */
/*                            is displayed.  Made them look more like       */
/*                            electronic coupons.                           */
/*   D08346   11-Jan-11 'CME' When an alt ID was rung up it only notified   */
/*                            sh code that a card was rung.  This fix allows*/
/*                            EM entry of the customer card also.           */
/*   D08426   25-Jan-11 'CME' Tossed out messages that did not match the    */
/*                            current transaction number.  Also allowed     */
/*                            pipe reading during prompts.                  */
/*   P1005357 05-Apr-11 'JM'  Support multiple accounts from phone number   */
/*                            requests                                      */
/*   D01125   22-Apr-11 'JM'  Issue with terminal stuck in a loop with Alt  */
/*                            ID key.                                       */
/*   D08426.1 25-May-11 'RBS' Update transaction number when between        */
/*                            transactions.                                 */
/*   D01520   02-Jun-11 'RBS' Remove forced total with phone number lookup  */
/*   D01675   06-Jul-11 'RBS' Total and tender messages not sent for WIC    */
/*                            transactions                                  */
/*   D01707   13-Jul-11 'RBS' Qty coupons not working correctly             */
/*   D02611   09-Feb-12 'RBS' Price required GS1 coupons causing terminal   */
/*                            to dump intermittently                        */
/*   D05295   18-Jan-13 'RBS' Terminal hangs in terminal offline mode (TOF) */
/*   D07543   10-Sep-13 'TRN' Add the full GS1 databar information to the   */
/*                            item information message for a GS1 coupon     */
/*   D25833   22-Apr-19 'DG'  Issue with NTR adding customer number         */
/*   D26775   31-Mar-20 'TDW' Duplicate customer number on receipt and in   */
/*                            TLOG after NTR code added                     */
/*  CHANGES: End                                                            */
/*                                                                          */
/****************************************************************************/

#include <swami.h>
#include <samincln.h>

/****************************************************************************/
/*  GreenPoints Include Files                                               */
/****************************************************************************/

#include ISGreenPointsApplication_i
#include ISGreenPointsMessage_i
#include ISGreenPointsMessageFactory_i
#include SAMGreenPointsCardEnhancement_i

/****************************************************************************/
/*  ACE Include Files                                                       */
/****************************************************************************/
//#include <cflexext.h>
#include ISLocale_i
// jai - temp
#include ISSAItemStorage_i
// jai - temp

#include ISSalesTransaction_i
#include ISAddDiscountCommand_i
#include ISTransactionTotalCommand_i
#include ISCommand_i
#include ISSalesTransactionDialog2x20_i
#include ISCommandFactory_i
#include ISAddItemCommand_i
#include ISOptions_i
#include ISItemEntry_i
#include ISOperator_i
#include ISInputSequence_i
#include ISFormattedStringList_i
#include CommonEventDefinitions_i
#include TransactionEventDefinitions_i
#include ISTenderEntry_i
#include ISWorkstation_i
#include SalesTransactionEventDefinitions_i
#include EventDefinitions_i
#include PricingPolicy_i
#include OptionsTypes_i
#include ISNLSFactory_i
#include ISINIFile_i
#include ISProcedureId_i
#include ISPromptDetails_i
#include ISSystem_i
#include ISTenderEntry_i
#include ISTenderType_i
#include ISTransaction_i
#include ISApplication2x20_i
#include ISOverrideList_i
#include ISOverride_i
#include ISTerminal_i
#include ISSignOnCommand_i
#include ISKeyboardDefinition_i
#include ISTenderTypeCache_i
#include ISGuidanceFactory_i
#include ISTaxExemptEntry_i
#include ISFile_i
#include ApplicationExceptions_i
#include ISTimeUtilities_i
#include ISInputState_i
#include ISWindow2x20_i                                    //SHGR0007
#include ISPosIOProcessor_i
#include ISDeviceFactory_i
#include ISCustomer_i                                      //D26775

#include TheLastThingIncluded_i

/****************************************************************************/
/*  Singleton Implementation                                                */
/****************************************************************************/

ISSINGLETON_IMPLEMENT_INSTANCE(ISGreenPointsApplication);

//***************************************************************************/
//  Constructor - default                                                   */
//***************************************************************************/
ISGreenPointsApplication::ISGreenPointsApplication(Void) :
  theOptionStatus(OFF),
  theIdleCount(0),
  alreadyLoaded(0),
  theDebugLevel(0),
  theWriteMessagePipe(0),
  theWriteMessagePipeID(""),
  theWriteMessagePipeName(""),
  theReadMessagePipe(0),
  theReadMessagePipeID(""),
  theReadMessagePipeName(""),
  theManagerOverrideFlag(0),
  alreadyChecked(0),
  promptForCard(1),
  theBOTMessageSent(FALSE),
  alreadyPrompted(0),
  theInTrainingMode(0),
  theSentSignOnMessage(0),
  theSuspendedTotalAmount(""),
  theSuspendedTaxAmount(""),
  theSuspendedRegisterID(""),
  theSuspendedTransactionNumber(""),
  theAlreadyPromptedFlag(FALSE),
  theItemDiscountCount(0),
  theFieldSeperator(0x1C),
  theAllowWelcomePromptFlag(0),
  theGreenPointsWelcomeMessage(""),
  theGreenPointsReceiptWelcomeMessage(""),
  theAllowReceiptWelcomePromptFlag(0),
  theReceiptMessagePrinted(FALSE),
  theAllowApplyingDiscountPromptFlag(0),
  theApplyingDiscountPromptMessage(""),
  theAlreadyPromptedApplyingDiscount(FALSE),
  theFirstRead(FALSE),
  theAllowCardNumberWelcomePromptFlag(0),
  theGreenPointsReceiptCardNumberMessage(""),
  theReceipCardNumberPrinted(FALSE),
  theDepartmentPrefix(""),
  theReadPipeSize(0),
  theCustomerCardNumber(""),
  theCustomerAccountName(""),
  theAllowTerminalPriceVerifyMessageFlag(0),
  theAllowInvalidCardNumberPromptFlag(0),
  theAddingItemFlag(FALSE),
  theGreenPointsValidAccountNameMessage(""),
  theGreenPointsStoredItemsCount(0),
  theSavedTransaction(0),
  theAddingTotalDiscountFlag(FALSE),
  theCustomerPointBalance(""),
  //lastIOData(""),                                        //SHGR0012 //D02611
  wasLastItemAnSHItem(FALSE),                              //D07952
  showNextAccountFlag(FALSE),                              //P1005357
  theItemEntered(FALSE)                                    //P1005357

{
  theInstance = this;

  //subscribeToOptionsLoaded(this);                         //SNRV0002
}

/****************************************************************************/
/*  Destructor                                                              */
/****************************************************************************/
ISGreenPointsApplication::~ISGreenPointsApplication(Void)
{
  theInstance = 0;
  cancelEvents();                                           //SNRV0002
  if (theWriteMessagePipe)
  {
    delete theWriteMessagePipe;
  }

  if (theReadMessagePipe)
  {
    delete theReadMessagePipe;
  }
}

/****************************************************************************/
/*  Log Info                                                                */
/****************************************************************************/

Void ISGreenPointsApplication::logInfo(Char * theMessage, UInt aDebugLevel) const
{
  if (aDebugLevel <= theDebugLevel)
  {
    ISBuffer theLogInformation(theMessage);
    theLogInformation.append("\x0D\x0A");

    ISSequentialFile theLogFile("<GNPTLOG1>",
                                ISFile::AccessExclusiveReadWrite,
                                ISFile::PriorityLow,
                                ISFile::ActionOpenCreateOpen,
                                ISFile::DistMirroredAtClose);

    theLogFile.seek(0, ISFile::SeekFromEnd);
    theLogFile.write(theLogInformation.getDataLength(), theLogInformation);
    theLogFile.close();
  }
}

/****************************************************************************/
/*  Options Loaded                                                          */
/****************************************************************************/
Void ISGreenPointsApplication::optionsLoaded(ISOptions* theOptions)
{
  cancelEvents();                                     //SNRV0002
  if (readINISettings() == TRUE)
  {
    setOptionStatus(ON);
    logInfo("Option is Enabled", FullDebug);
  }
  else
  {
    setOptionStatus(OFF);
    logInfo("Option is Disabled", FullDebug);
  }
  logInfo("exiting optionsLoaded", EnterExitMethodDebug);
}

/****************************************************************************/
/*  set Card Number message sent                                            */
/****************************************************************************/
Void ISGreenPointsApplication::setBOTMessageSent(Boolean anAlreadySentBOTFlag)
{
  logInfo("in setBOTMessageSent", FullDebug);
  theBOTMessageSent = anAlreadySentBOTFlag;
}

/****************************************************************************/
/*  get Card Number message sent                                            */
/****************************************************************************/
Boolean ISGreenPointsApplication::getBOTMessageSent(Void)
{
  logInfo("in getBOTMessageSent", FullDebug);
  return theBOTMessageSent;
}

/****************************************************************************/
/*  Override Entered                                                        */
/****************************************************************************/
Void ISGreenPointsApplication::overrideEntered(ISOverrideList * anOverrideList)
{
  logInfo("in overrideEntered", FullDebug);
  theManagerOverrideFlag = anOverrideList->isManagerOverride();
}
/****************************************************************************/
/*  Override Entered                                                        */
/****************************************************************************/
Void ISGreenPointsApplication::exceptionLogged(ISApplicationException * anException)
{
  logInfo("in exceptionLogged", FullDebug);
  UInt exceptionLogged =  anException->exceptionType();
}

/****************************************************************************/
/*  Initialize Pipe                                                         */
/****************************************************************************/
Void ISGreenPointsApplication::initializePipe(Void)
{
  if (optionStatus() == SUSPENDED)
  {
    logInfo("in initializePipe..option status = SUSPENDED", FullDebug);
    theWriteMessagePipe->close();
    logInfo("in initializePipe..closing pipe", FullDebug);
    theWriteMessagePipeResult = theWriteMessagePipe->open(theWriteMessagePipeID);
    if (!theWriteMessagePipeResult.isError())
    {
      logInfo("in initializePipe..no error opening write pipe", FullDebug);
      theReadMessagePipe->close();
      logInfo("in initializePipe..read pipe closing for initialize", FullDebug);
      theReadMessagePipeResult = theReadMessagePipe->create(theReadMessagePipeID, theReadPipeSize);
      if (!theReadMessagePipeResult.isError())
      {
        logInfo("in initializePipe..setting Status ON..no error with read pipe..setting Option Status ON", FullDebug);
        cancelEvents();                    //SNRV0002
        setOptionStatus(ON);
      }
    }
  }
}

/****************************************************************************/
/*  Operator Sign On                                                        */
/****************************************************************************/
Void ISGreenPointsApplication::operatorSignedOn(ISWorkstation* theWorkstation)
{
  logInfo("in operatorSignedOn", FullDebug);
  Int aRegisterId = 0;
  initializePipe();
  if (optionStatus() == ON)
  {
    logInfo("in operatorSignedOn..status is ON..", FullDebug);
    ISString aDateTime = TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S").remove(0, 2);
    theInTrainingMode =  SAMGreenPointsCardEnhancement::instance().getInTraining();
    ISString aSignOnMode = TheLocale.asString(theInTrainingMode)
                           .append(TheLocale.asString(theManagerOverrideFlag));
    ISGreenPointsSignOnMessage* aMessage =
      ISGreenPointsMessageFactory::instance().makeSignOnMessage();
    //ASHGR0007
    if (expandedHeaderRecord)
    {
      aMessage->setExpandedHeader(expandedHeaderRecord);
      aMessage->setRetailerId(retailerID.asInteger());
      aMessage->setStoreId(storeID.asInteger());
    }
    //ESHGR0007
    TheLocale.stringToNum(theWorkstation->getId(), aRegisterId);
    aMessage->setRegisterId(aRegisterId);
    aMessage->setTransactionNumber(theWorkstation->transactionNumber());
    aMessage->setSignOnFlag(aSignOnMode);
    aMessage->setOperatorNumber(theWorkstation->getCurrentOperatorId());
    aMessage->setDateTime(aDateTime);
    writeMessage(aMessage);
    delete aMessage;
  }
}

/****************************************************************************/
/*  Operator Sign Off                                                       */
/****************************************************************************/
Void ISGreenPointsApplication::operatorSignedOff(ISWorkstation* aWorkstation)
{
  logInfo("in operatorSignedOff", FullDebug);
  UInt aManagerSignOff = 0;
  Int aRegisterId = 0;
  ISString aDateTime = TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S").remove(0, 2);
  if (theManagerOverrideFlag == 1)
    aManagerSignOff = 3;
  else
    aManagerSignOff = 2;

  theInTrainingMode =  SAMGreenPointsCardEnhancement::instance().getInTraining();
  ISString aSignOffMode = TheLocale.asString(theInTrainingMode)
                          .append(TheLocale.asString(aManagerSignOff));

  ISGreenPointsSignOffMessage* aMessage =
    ISGreenPointsMessageFactory::instance().makeSignOffMessage();
  //ASHGR0007
  if (expandedHeaderRecord)
  {
    aMessage->setExpandedHeader(expandedHeaderRecord);
    aMessage->setRetailerId(retailerID.asInteger());
    aMessage->setStoreId(storeID.asInteger());
  }
  //ESHGR0007
  TheLocale.stringToNum(aWorkstation->getId(), aRegisterId);
  aMessage->setRegisterId(aRegisterId);
  aMessage->setTransactionNumber(aWorkstation->transactionNumber());
  aMessage->setSignOffFlag(aSignOffMode);
  aMessage->setOperatorNumber(aWorkstation->getCurrentOperatorId());
  aMessage->setDateTime(aDateTime);
  writeMessage(aMessage);
  delete aMessage;

  theSentSignOnMessage = 0;
  theManagerOverrideFlag = 0;
  theInTrainingMode = 0;
}

/****************************************************************************/
/*  Recover Transaction                                                     */
/****************************************************************************/
Void ISGreenPointsApplication::recallTransaction(ISTransaction* aTransaction)
{
  logInfo("in recallTransaction", FullDebug);
  theSuspendedRegisterID =  aTransaction->workstationId();
  theSuspendedTransactionNumber = TheLocale.asString(aTransaction->number());
  theSuspendedTotalAmount = TheLocale.asString(aTransaction->grossPositive());
}

/****************************************************************************/
/*  Start Transaction                                                       */
/****************************************************************************/
Void ISGreenPointsApplication::startTransaction(ISTransaction * aTransaction)
{
  logInfo("in startTransaction", FullDebug);
  Byte procedureNumber = aTransaction->transactionType();
  theSavedTransaction = aTransaction;
  initializePipe();
  if (procedureNumber != ISProcedureId::SignOff)
  {
    theInTrainingMode = SAMGreenPointsCardEnhancement::instance().getInTraining();
    ISString theTempOperator = aTransaction->currentOperatorId();
    UInt recall = aTransaction->isRecoveryInProgress();
    UInt suspended = aTransaction->isRecalledTransaction();
    Int aRegisterId = 0;
    if (SAMGreenPointsCardEnhancement::instance().getBOTMessageSent() == FALSE)
    {
      if (!recall)
      {
        ISString aBeginTransactionMode = TheLocale.asString(theInTrainingMode)
                                         .append(TheLocale.asString(suspended));
        theTempDateTime = (TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S")).remove(0, 2);
        ISGreenPointsBeginTransactionMessage * aMessage =
          ISGreenPointsMessageFactory::instance().makeBeginTransactionMessage();
        //ASHGR0007
        if (expandedHeaderRecord)
        {
          aMessage->setExpandedHeader(expandedHeaderRecord);
          aMessage->setRetailerId(retailerID.asInteger());
          aMessage->setStoreId(storeID.asInteger());
        }
        //ESHGR0007
        TheLocale.stringToNum(aTransaction->workstationId(), aRegisterId);
        aMessage->setRegisterId(aRegisterId);
        aMessage->setTransactionNumber(aTransaction->number());
        aMessage->setBeginTransactionFlag(aBeginTransactionMode);
        aMessage->setOperatorNumber(aTransaction->currentOperatorId());
        aMessage->setDateTime(theTempDateTime);
        aMessage->setBeginTransactionNumber(TheLocale.asString(aTransaction->number()));
        aMessage->setSuspendedRegisterId("");
        aMessage->setSuspendedTransNumber("");
        aMessage->setSuspendedTransTotal("");
        aMessage->setSuspendedTaxTotal("");
        writeMessage(aMessage);
        SAMGreenPointsCardEnhancement::instance().setBOTMessageSent(TRUE);
        delete aMessage;
      }
      else if ((recall) && (suspended))
      {
        ISString aBeginTransactionMode = TheLocale.asString(theInTrainingMode)
                                         .append(TheLocale.asString(suspended));
        theTempDateTime = (TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S")).remove(0, 2);
        ISGreenPointsBeginTransactionMessage * aMessage =
          ISGreenPointsMessageFactory::instance().makeBeginTransactionMessage();
        //ASHGR0007
        if (expandedHeaderRecord)
        {
          aMessage->setExpandedHeader(expandedHeaderRecord);
          aMessage->setRetailerId(retailerID.asInteger());
          aMessage->setStoreId(storeID.asInteger());
        }
        //ESHGR0007
        TheLocale.stringToNum(aTransaction->workstationId(), aRegisterId);
        aMessage->setRegisterId(aRegisterId);
        aMessage->setTransactionNumber(aTransaction->number());
        aMessage->setBeginTransactionFlag(aBeginTransactionMode);
        aMessage->setOperatorNumber(aTransaction->currentOperatorId());
        aMessage->setDateTime(theTempDateTime);
        aMessage->setBeginTransactionNumber(TheLocale.asString(aTransaction->number()));
        aMessage->setSuspendedRegisterId(theSuspendedRegisterID);
        aMessage->setSuspendedTransNumber(theSuspendedTransactionNumber);
        aMessage->setSuspendedTransTotal(theSuspendedTotalAmount);
        aMessage->setSuspendedTaxTotal(theSuspendedTaxAmount);
        writeMessage(aMessage);
        SAMGreenPointsCardEnhancement::instance().setBOTMessageSent(TRUE);
        delete aMessage;
      }
    }
  }
}

//****************************************************************************
//
// aBarCode  -- (ISString &) representing the barcode to be checked for
//              validity
//
// The Boolean return code represents whether the barcode conforms to the
// modulo check used to verify the label.
//
//****************************************************************************
Boolean ISGreenPointsApplication::isValidCheckDigit (ISString &aBarCode)
{
  logInfo("in isValidCheckDigit", FullDebug);
  ISTRACE_IN(isValid);
  UInt totalOdd = 0;
  UInt totalEven = 0;
  ISString checkDigit;
  Boolean answer;

  UInt aBeginPos = aBarCode.length();
  aBeginPos -= 1;

  // Start at the next to last item (beside check digit)
  // Total every other digit
  for (Int index = aBeginPos - 1; index >= 0; index-=2)
  {
    totalEven += (aBarCode[index] - '0');
  }

  // Start at the two from check digit
  // Total every other digit
  for (index = aBeginPos - 2; index >= 0;  index-=2)
  {
    totalOdd += (aBarCode[index] - '0');
  }

  checkDigit = (aBarCode[aBeginPos]);
  UInt aCheckDigit = 0;
  TheLocale.stringToNum(checkDigit, aCheckDigit);
  answer = ((((totalEven * 3) + totalOdd + aCheckDigit) % 10) == 0);
  return answer;
}

/****************************************************************************/
/*  Item Entry                                                              */
/****************************************************************************/
Void ISGreenPointsApplication::addItemEntry(ISItemEntry* anItemEntry)
{
  logInfo("in addItemEntry", FullDebug);
  wasLastItemAnSHItem = FALSE;                           //D07952

  if (!anItemEntry->transaction()->isRecoveryInProgress())
  {
    UInt       itemType     = 0;
    Byte       messageFlag  = 0x00;
    ISQuantity itemQuantity = 0;
    UInt       modeAndOperation = 0;
    ISString   anItemType ="0";
    UInt       anItemAttribute = 0;
    ISString   anItemPrice = "";
    ISString   anItemDescription = "";
    ISString   aTempItemCode = "";
    UInt       aBarCodeScan = 0;
    ISString   anItemCode = "";
    ISString   anItemDataBar = "";                     //D07543

    Int aRegisterId = 0;

    if (getAddingItem() == TRUE)
    {

      wasLastItemAnSHItem = TRUE;                    //D07952
      ISString aDescription = getItemDescriptor();
      if (aDescription != "")
      {
        anItemEntry->item().setDescription(aDescription);
      }
    }

    if ((SAMGreenPointsCardEnhancement::instance().getGreenPointsCardEntered() == TRUE) &&
        (theAllowReceiptWelcomePromptFlag == 1) &&
        (theReceiptMessagePrinted == FALSE))
    {
      ISFormattedStringList anItemNumberPrintLine(ISFormattedStringList::SalesDescriptor, 0, 40);
      anItemNumberPrintLine.insertString(theGreenPointsReceiptWelcomeMessage);
      ISReportOutputEvent::raiseWith(&anItemNumberPrintLine);
      theReceiptMessagePrinted = TRUE;
    }

    if ((SAMGreenPointsCardEnhancement::instance().getGreenPointsCardEntered() == TRUE) &&
        (theAllowCardNumberWelcomePromptFlag == 1) &&
        (theReceipCardNumberPrinted == FALSE))
    {
      ISString aCardString = theGreenPointsReceiptCardNumberMessage;
      aCardString.append(" ");
      //AP1005357
      theCustomerCardNumber = SAMGreenPointsCardEnhancement::instance().getGreenPointsCardNumber().strip(ISString::leading, '0');
      aCardString.append(theCustomerCardNumber);
      //aCardString.append(SAMGreenPointsCardEnhancement::instance().getGreenPointsCardNumber());
      //EP1005357
      if (aCardString.contains("?"))
      {
        UInt aTempIndex = aCardString.index("?");
        aCardString.remove(aTempIndex);
      }
      centerReceiptMessage(aCardString);
      ISFormattedStringList anItemNumberPrintLine(ISFormattedStringList::DepartmentTotals, 0, 40);
      anItemNumberPrintLine.insertString(aCardString);
      ISReportOutputEvent::raiseWith(&anItemNumberPrintLine);
      theReceipCardNumberPrinted = TRUE;
    }

    //mode and operation flag
    if (anItemEntry->isVoid())
      modeAndOperation += 1;
    if (anItemEntry->isRefundKeyed())
      modeAndOperation += 2;

    //item type
    itemType = anItemEntry->itemType();
    ISString anItem = anItemEntry->itemCodeAsString();
    if (anItemEntry->item().isPriceRequired())
    {
      anItemType = "c";
      if (anItemEntry->pricingMethod().unitPrice() == 0)
      {
        anItemType = "0";
      }
    }

    if (itemType == ISItem::MiscReceiptItemType)
      anItemType = "b";
    if (itemType == ISItem::MiscPayoutItemType)
      anItemType = "8";
    if (itemType == ISItem::StoreCouponItemType)
      anItemType = "2";
    if (itemType == ISItem::ManfCouponItemType)
      anItemType = "4";
    if (itemType == ISItem::DepositItemType)
      anItemType = "9";
    if (itemType  == ISItem::DepositReturnItemType)
      anItemType = "a";
    if (getAddingItem() == TRUE)
      anItemType = "6";

    //item code
    anItemCode = anItemEntry->itemCodeAsString();

    //item type for department items
    if (anItemCode.index(theDepartmentPrefix, 0) == 0)
    {
      anItemType = "1";
      if (itemType == ISItem::StoreCouponItemType)
        anItemType = "3";
      if (itemType == ISItem::ManfCouponItemType)
        anItemType = "5";
    }

    //item attributes
    if ((!anItemEntry->item().isFoodstampable()) &&
        (anItemEntry->item().isTaxable()))
      anItemAttribute = 1;
    else if ((anItemEntry->item().isFoodstampable()) &&
             (!anItemEntry->item().isTaxable()))
      anItemAttribute = 2;
    else if ((anItemEntry->item().isFoodstampable()) &&
             (anItemEntry->item().isTaxable()))
      anItemAttribute = 3;

    if (anItemCode.length() == 7)
    {
      ISString aCheckDataCode = anItemEntry->itemCode();
      UInt theValidCheckDigit = isValidCheckDigit(aCheckDataCode);
      if (anItemCode.index("0", 0) != 0)
      {
        ISString aTempItemCode = "00000000000000";
        aTempItemCode.append(anItemCode).append("?");
        anItemCode = aTempItemCode;
        anItemCode.remove(0, (aTempItemCode.length() - 14));
      }
    }
    else
    {
      if ((anItemCode.length() == 12) ||
          (anItemCode.length() == 13))
      {
        ISString aTempItemCode = "00000000000000";
        aTempItemCode.append(anItemCode);
        aTempItemCode.append("?");
        aTempItemCode.remove(0, (aTempItemCode.length() - 14));
        anItemCode = aTempItemCode;
      }
      else
      {
        anItemCode = anItemCode.prepend("00000000000000");
        anItemCode.append("?");
        anItemCode.remove(0, (anItemCode.length() - 14));
      }
    }

    if ((SAMGreenPointsCardEnhancement::instance().getGreenPointsCardMessageSent() == FALSE) &&
        (!alreadyPrompted) &&
        (theAllowCardPromptFlag == 1))
    {
      ISPromptDetails* thePrompt = ISGuidanceFactory::instance().makePromptDetails(theGreenPointsCardMessage, "");
      thePrompt->setClearKeyRequired(TRUE);
      thePrompt->setErrorToneRequired(TRUE);
      thePrompt->setRestoreDisplayRequired(TRUE);
      ISPromptUserEvent::raiseWith(thePrompt);
      delete thePrompt;
      alreadyPrompted = 1;
    }

    //item quantity
    ULong aQuantity = abs(anItemEntry->saleQuantity());
    ISAmount aPrice = anItemEntry->price();
    ISString aQuantityWeight;
    if (anItemEntry->isWeightItem())
    {
      UInt aDecimalPlace = TheLocale.weightDecimalPlaces();
      if (aDecimalPlace > 5)
        aDecimalPlace = 5;
      else
      {
        if (aDecimalPlace < 2)
          aDecimalPlace = 2;
      }
      aQuantityWeight = TheLocale.numberAsDecimalString(anItemEntry->weight(), aDecimalPlace);
      if (aQuantityWeight.contains(" "))
      {
        UInt aTempIndex = aQuantityWeight.index(" ");
        aQuantityWeight.remove(aTempIndex);
      }
      aQuantityWeight = TheLocale.numberAsDecimalString(anItemEntry->weight(), aDecimalPlace);
    }
    else
      aQuantityWeight = TheLocale.asString(aQuantity);

    // base quantity
    ULong aBaseQuantity = anItemEntry->pricingMethod().baseQuantity();
    ISString aBaseQuantityString;
    ISString aUnitPriceString;
    if (aBaseQuantity > 0)
    {
      aBaseQuantityString = TheLocale.asString(aBaseQuantity);
      // unit price
      ULong aUnitPrice = anItemEntry->pricingMethod().unitPrice();
      aUnitPriceString = TheLocale.asString(aUnitPrice);
    }
    else
    {
      aBaseQuantityString = "";
      aUnitPriceString = "";
    }

    //ASHGR0012
#ifndef __SHACE_V6__
    if (anItemEntry->isGS1DataBarCoupon())
    {
      //AD07543
      anItemDataBar = anItemEntry->itemCode().couponDataBar()->getDataBar();
      if (anItemDataBar != "")
        anItemCode = "";
      else
      {
        //ED07543
        anItemCode = anItemCode.prepend("00000000000000");
        //AD02611
        //ISBuffer IOData(lastIOData);
        ISBuffer IOData(anItemEntry->itemCode().couponDataBar()->getDataBar());
        //ED02611
        ISString fifthdigit = IOData.stringAt(4,1);
        int numofdigitstoget = 0;
        TheLocale.stringToNum(fifthdigit, numofdigitstoget);
        numofdigitstoget = numofdigitstoget + 12;
        anItemCode = IOData.stringAt(5,numofdigitstoget);
        anItemCode.append("?");
        anItemCode.remove(0, (anItemCode.length() - 14));
        //AD07543
      }
      //ED07543
    }
#endif
    //ESHGR0012

    if (anItemType == "1")
      anItemCode = "";

    theInTrainingMode = SAMGreenPointsCardEnhancement::instance().getInTraining();
    ISGreenPointsItemLookUpMessage* aMessage =
      ISGreenPointsMessageFactory::instance().makeItemLookUpMessage();
    //ASHGR0007
    if (expandedHeaderRecord)
    {
      aMessage->setExpandedHeader(expandedHeaderRecord);
      aMessage->setRetailerId(retailerID.asInteger());
      aMessage->setStoreId(storeID.asInteger());
    }
    //ESHGR0007
    TheLocale.stringToNum(anItemEntry->transaction()->workstationId(), aRegisterId);
    aMessage->setRegisterId(aRegisterId);
    aMessage->setTransactionNumber(anItemEntry->transaction()->number());
    aMessage->setItemLookUpFlag(TheLocale.asString(theInTrainingMode).append(TheLocale.asString(modeAndOperation)));
    aMessage->setItemType(anItemType);
    aMessage->setItemAttributes(TheLocale.asString(anItemAttribute));
    aMessage->setItemCode(anItemCode);
    aMessage->setItemDept(TheLocale.asString(anItemEntry->departmentNumber()));
    aMessage->setItemQuantity(aQuantityWeight);
    aMessage->setItemPrice(TheLocale.asString(aPrice));
    aMessage->setItemDescription(anItemEntry->description());
    aMessage->setItemUnitDealQuantity(aBaseQuantityString);
    aMessage->setItemUnitDealPrice(aUnitPriceString);
    aMessage->setItemGS1DataBar(anItemDataBar);        //D07543
    writeMessage(aMessage);
    delete aMessage;
    ISTransaction *aTransaction = anItemEntry->transaction();
  }

  //ASHGR0007
  if (setItemAsNonDiscountable)
  {
    anItemEntry->item().setLinkedItemCode("0");
    setItemAsNonDiscountable = FALSE;
  }
  //ESHGR0007

  theItemEntered = TRUE;                                 //P1005357
  logInfo("exiting addItemEntry", EnterExitMethodDebug);
}

/****************************************************************************/
/*  Transaction Total                                                       */
/****************************************************************************/
Void ISGreenPointsApplication::transactionTotal(ISTransaction* aTransaction)
{

  logInfo("in transactionTotal", FullDebug);
  //AD01675
  ////ASHGR0007 Defect 4001
  ////if (!aTransaction->isRecoveryInProgress())
  //if ((!aTransaction->isRecoveryInProgress()) && (aTransaction->transactionType() == ISProcedureId::SalesTransaction))
  ////ESHGR0007 Defect 4001
  if ((!aTransaction->isRecoveryInProgress()) &&
      ((aTransaction->transactionType() == ISTransaction::CheckoutTransaction) ||
       (aTransaction->transactionType() == ISTransaction::WICTransaction)))
  //ED01675
  {
    UInt aTotalType = aTransaction->transactionType();
    UInt theTotalType = 0;
    ISString aTotalDescription = "TOTAL";
    if (aTotalType == ISProcedureId::SalesTransaction)
      theTotalType = 0;
    else if (aTotalType == ISProcedureId::NSWICTransaction)
      theTotalType = 3;

    ISAmount aTaxAmount = 0;
    for (UInt anIndex = 1; anIndex <= ISTaxPlan::MaxTaxPlans; anIndex++)
    {
      aTaxAmount = aTaxAmount + aTransaction->taxes(anIndex);
    }
    ISAmount anAmount = 0;
    anAmount = aTransaction->balanceDue() - aTaxAmount;

    UInt aBool = aTransaction->isSuspendedTransaction();
    theInTrainingMode =  SAMGreenPointsCardEnhancement::instance().getInTraining();
    ISGreenPointsTotalMessage* aMessage =
      ISGreenPointsMessageFactory::instance().makeTotalMessage();
    //ASHGR0007
    if (expandedHeaderRecord)
    {
      aMessage->setExpandedHeader(expandedHeaderRecord);
      aMessage->setRetailerId(retailerID.asInteger());
      aMessage->setStoreId(storeID.asInteger());
    }
    //ESHGR0007
    Int aRegisterId = 0;
    TheLocale.stringToNum(aTransaction->workstationId(), aRegisterId);
    aMessage->setRegisterId(aRegisterId);
    aMessage->setTransactionNumber(aTransaction->number());
    aMessage->setTransTotalFlag(TheLocale.asString(theInTrainingMode).append(TheLocale.asString(theTotalType)));
    aMessage->setTotalAmount(TheLocale.asString(anAmount));
    aMessage->setTaxAmount(TheLocale.asString(aTaxAmount));
    aMessage->setTotalDescription(aTotalDescription);

    ISBuffer aReadBuffer;
    Char  returnPipe[11];
    writeMessage(aMessage);
    delete aMessage;
    readMessage(theTotalReadDelay);
  }
}

/****************************************************************************/
/* Add Item to the Transaction if a discount is received from InfoPilot     */
/****************************************************************************/
Void ISGreenPointsApplication::doTotal(Void)
{
  logInfo("in doTotal", FullDebug);
  ISCommandCallbackBase* theItemCallback;

  // Create the commnad to report errors to our commandError() member
  theItemCallback = new ISCommandCallback<ISGreenPointsApplication>(this,
                                                                    &ISGreenPointsApplication::addItemError);
  ISTransactionTotalCommand * command =
    ISCommandFactory::instance().makeTransactionTotalCommand(*theItemCallback);
  command->transaction(theSavedTransaction);
  command->addAttribute(ISCommand::AttrSubtotalOnly);
  command->execute();
  delete command;
}


/****************************************************************************/
/* Add Item to the Transaction if a discount is received from InfoPilot     */
/****************************************************************************/
Void ISGreenPointsApplication::addGreenPointsItem(ISString& aDiscountType,
                                                  ISString& anItemCode,
                                                  ISString& anItemDesc,
                                                  UInt aQuantity,
                                                  ISTransaction& transaction)
{
  logInfo("in addGreenPointsItem", FullDebug);
  UInt anInputState = ISApplication2x20::instance().activeView().inputState().id();
  if (anInputState == ISInputState::MainState)
  {
    if ((theAlreadyPromptedApplyingDiscount == FALSE) &&
        (theAllowApplyingDiscountPromptFlag == 1))
    {
      ISPromptDetails* thePrompt = ISGuidanceFactory::instance().makePromptDetails(theApplyingDiscountPromptMessage, "");
      thePrompt->setClearKeyRequired(FALSE);
      thePrompt->setErrorToneRequired(FALSE);
      thePrompt->setRestoreDisplayRequired(TRUE);
      ISPromptUserEvent::raiseWith(thePrompt);
      delete thePrompt;
      theAlreadyPromptedApplyingDiscount = TRUE;
    }

    Char theMessage[200];
    ISCommandCallbackBase* theItemCallback;
    // Create the error callback objects to handle any errors
    theItemCallback = new ISCommandCallback<ISGreenPointsApplication>(this,
                                                                      &ISGreenPointsApplication::addItemError);

    ISAddItemCommand *theAddItemCommand =
      ISCommandFactory::instance().makeAddItemCommand(*theItemCallback);
    theAddItemCommand->transaction(&transaction);
    UInt theEntryMode = 0;
    theEntryMode = ISItemCode::KeyedLabel;
    theAddItemCommand->addAttribute(ISCommand::AttrItemEntryMode, theEntryMode);
    theAddItemCommand->addAttribute(ISCommand::AttrItemId, anItemCode);
    //ASHGR0007
    ISBuffer buffItemCode;
    buffItemCode.append(anItemCode);
    //AD01707
    //if (!(anItemCode.length() == 11) && ((Char)buffItemCode[0] == '5'))
    if (!((anItemCode.length() == 11) && ((Char)buffItemCode[0] == '5')))
    //ED01707
    {
      //ESHGR0007
      theAddItemCommand->addAttribute(ISCommand::AttrSalesQuantity, aQuantity);
      //ASHGR0007
    }
    //ESHGR0007

    if (aDiscountType == "2")
      theAddItemCommand->addAttribute(ISCommand::AttrVoid);

    if (anItemDesc.length() != 0)
    {
      anItemDesc = anItemDesc.append(0x20, (18 - anItemDesc.length()));
      anItemDesc.append(0x1C);
    }

    setAddingItem(TRUE);
    setItemAsNonDiscountable = TRUE;                     //SHGR0007
    setItemDescriptor(anItemDesc);
    Boolean addedItem = (theAddItemCommand->execute());
    if (getAddingTotalDiscount() == TRUE)
      transactionTotal(&transaction);

    setAddingItem(FALSE);
    anItemDesc = "";
    setItemDescriptor(anItemDesc);
  }
  else
    addItemsToStorage(aDiscountType,anItemCode, anItemDesc, TheLocale.numberAsString(aQuantity));
}

/****************************************************************************/
/* Add Items To storage in case terminal state not ready to process items   */
/****************************************************************************/
Void ISGreenPointsApplication::addItemsToStorage(ISString aDiscountType,
                                                 ISString anItemCode,
                                                 ISString anItemDesc,
                                                 ISString aQuantity)
{
  logInfo("in addItemsToStorage", FullDebug);
  ISString aStoredItemString = "";
  aStoredItemString.append(":");
  aStoredItemString.append(aDiscountType);
  aStoredItemString.append(":");
  aStoredItemString.append(anItemCode);
  aStoredItemString.append(":");
  aStoredItemString.append(anItemDesc);
  aStoredItemString.append(":");
  aStoredItemString.append(aQuantity);
  aStoredItemString.append(":");

  theGreenPointsStoredItems[theGreenPointsStoredItemsCount] =  aStoredItemString;
  theGreenPointsStoredItemsCount = theGreenPointsStoredItemsCount + 1;
}

/****************************************************************************/
/* Add Items To storage in case terminal state not ready to process items   */
/****************************************************************************/
Void ISGreenPointsApplication::retrieveItemsFromStorage(Void)
{
  logInfo("in retrieveItemsFromStorage", FullDebug);
  Char theMessage[200];
  UInt theStartPos = 0;
  UInt theEndPos = 0;
  ISBuffer aTempBuffer;
  ISBuffer aMessage;
  UInt aFieldCount = 0;
  ISString aDiscountType = "";
  ISString aDiscountItemCode = "";
  ISString aDiscountItemDesc = "";
  ISString aBufferString = "";
  UInt aDiscountQuantity = 0;
  Byte aFieldSeperator(0x3A);
  aMessage.create(theGreenPointsStoredItems[theGreenPointsStoredItemsCount - 1].length(), (Byte) 0);
  aMessage.putStringAt(0, theGreenPointsStoredItems[theGreenPointsStoredItemsCount - 1].length(),
                       theGreenPointsStoredItems[theGreenPointsStoredItemsCount - 1]);
  aTempBuffer.create(1, (Byte) 0);
  aTempBuffer.putByteAt(0, aFieldSeperator);

  do
  {
    theStartPos = aMessage.locate(aTempBuffer, 1, theEndPos);
    theEndPos = aMessage.locate(aTempBuffer, 1, (theStartPos + 1));
    aBufferString = aMessage.stringAt((theStartPos + 1), ((theEndPos - theStartPos) - 1));
    switch (aFieldCount)
    {
      case 0:
        {
          aDiscountType = aBufferString;
          break;
        }
      case 1:
        {
          aDiscountItemCode = aBufferString;
          break;
        }
      case 2:
        {
          aDiscountItemDesc = aBufferString;
          break;
        }
      case 3:
        {
          TheLocale.stringToNum(aBufferString, aDiscountQuantity);
          break;
        }
    }
    aFieldCount = aFieldCount + 1;
  }while (aFieldCount < 4);

  //add the Item
  addGreenPointsItem(aDiscountType, aDiscountItemCode, aDiscountItemDesc, aDiscountQuantity,  *theSavedTransaction);
  theGreenPointsStoredItems[theGreenPointsStoredItemsCount - 1] = "";
  theGreenPointsStoredItemsCount = theGreenPointsStoredItemsCount - 1;
}

/****************************************************************************/
/* Add Item Entry Flag                                                      */
/****************************************************************************/
Void ISGreenPointsApplication::setItemDescriptor(ISString& anItemDescriptor)
{
  logInfo("in setItemDescriptor", FullDebug);
  theAddingItemDescriptor =  anItemDescriptor;
}

/****************************************************************************/
/* Add Item Entry Flag                                                      */
/****************************************************************************/
ISString ISGreenPointsApplication::getItemDescriptor(Void)
{
  logInfo("in getItemDescriptor", FullDebug);
  return theAddingItemDescriptor;
}

/****************************************************************************/
/* Add Item Entry Flag                                                      */
/****************************************************************************/
Void ISGreenPointsApplication::setAddingItem(Boolean anEntryFlag)
{
  logInfo("in setAddingItem", FullDebug);
  theAddingItemFlag =  anEntryFlag;
}

/****************************************************************************/
/* Add Item Entry Flag                                                      */
/****************************************************************************/
Boolean ISGreenPointsApplication::getAddingItem(Void)
{
  logInfo("in getAddingItem", FullDebug);
  return theAddingItemFlag;
}

/****************************************************************************/
/* Get Adding Total Discount Flag                                           */
/****************************************************************************/
Boolean ISGreenPointsApplication::getAddingTotalDiscount(Void)
{
  logInfo("in getAddingTotalDiscount", FullDebug);
  return theAddingTotalDiscountFlag;
}

/****************************************************************************/
/* Set Adding Total Discount Flag                                           */
/****************************************************************************/
Void ISGreenPointsApplication::setAddingTotalDiscount(Boolean anAddingTotalFlag)
{
  logInfo("in setAddingTotalDiscount", FullDebug);
  theAddingTotalDiscountFlag = anAddingTotalFlag;
}

/****************************************************************************/
/* Set Added Total Discount Flag                                           */
/****************************************************************************/
Boolean ISGreenPointsApplication::getAddedTotalDiscount(Void)
{
  logInfo("in getAddedTotalDiscount", FullDebug);
  return theTotalDiscountApplied;
}

/****************************************************************************/
/* Set Added Total Discount Flag                                           */
/****************************************************************************/
Void ISGreenPointsApplication::setAddedTotalDiscount(Boolean anAddedTotalFlag)
{
  logInfo("in setAddedTotalDiscount", FullDebug);
  theTotalDiscountApplied = anAddedTotalFlag;
}

/****************************************************************************/
/* Error Call Back                                                          */
/****************************************************************************/
Boolean ISGreenPointsApplication::addItemError(const ISCommandCallbackEvent & anEvent)
{
  logInfo("in addItemError", FullDebug);
  Boolean result = FALSE;
  ISSalesTransactionDialog2x20 *aTransactionDialog;
  // the error code in the event indicates the type of error ...
  switch (anEvent.errorId())
  {
    case ISCommand::ErrorMissingAttr:
      switch (anEvent.attribute())
      {
        case ISCommand::AttrPrice:
          aTransactionDialog->doPrompter(ISNLSFactory::Msg_PriceRequired);
          result = TRUE;
          break;
        case ISCommand::AttrSalesQuantity:
          aTransactionDialog->doPrompter(ISNLSFactory::Msg_QuantityRequired);
          result = TRUE;
          break;
        case ISCommand::AttrWeight:
          aTransactionDialog->doPrompter(ISNLSFactory::Msg_WeightRequired);
          result = TRUE;
          break;
        case ISCommand::AttrAmount:
          aTransactionDialog->doPrompter(ISNLSFactory::Msg_TakeTotal);
          result = TRUE;
          break;
        default:
          aTransactionDialog->doCheckKeySequence();
          result = TRUE;
          break;
      }
      break;
    case ISCommand::ErrorConfirmAttr:
      if (anEvent.attribute() == ISCommand::AttrPrice)
      {
        aTransactionDialog->doPrompter(ISNLSFactory::Msg_PriceRequired);
        result = TRUE;
      }
      else
      {
        aTransactionDialog->doCheckKeySequence();
        result = TRUE;
      }
      break;
    default:
      if (anEvent.guidance() != 0)
      {
        aTransactionDialog->doPrompter(*anEvent.guidance());
        result = TRUE;
      }
      else
      {
        aTransactionDialog->doCheckKeySequence();
      }
      break;
  }
  return result;
}

/****************************************************************************/
/* Food Stamp Transaction Total                                             */
/****************************************************************************/
Void ISGreenPointsApplication::foodstampTotal(ISTransaction* aTransaction)
{
  logInfo("in foodstampTotal", FullDebug);
  if (!aTransaction->isRecoveryInProgress())
  {
    UInt aTotalType = aTransaction->transactionType();
    UInt theTotalType = 1;
    ISString aTotalDescription = "TOTAL";
    ISAmount anAmount = (aTransaction->grossPositive() - aTransaction->grossNegative());
    ISAmount aTaxAmount = 0;
    for (UInt anIndex = 1; anIndex <= ISTaxPlan::MaxTaxPlans; anIndex++)
    {
      aTaxAmount = aTaxAmount + aTransaction->taxes(anIndex);
    }

    theInTrainingMode =  SAMGreenPointsCardEnhancement::instance().getInTraining();
    ISGreenPointsTotalMessage* aMessage =
      ISGreenPointsMessageFactory::instance().makeTotalMessage();
    //ASHGR0007
    if (expandedHeaderRecord)
    {
      aMessage->setExpandedHeader(expandedHeaderRecord);
      aMessage->setRetailerId(retailerID.asInteger());
      aMessage->setStoreId(storeID.asInteger());
    }
    //ESHGR0007
    Int aRegisterId = 0;
    TheLocale.stringToNum(aTransaction->workstationId(), aRegisterId);
    aMessage->setRegisterId(aRegisterId);
    aMessage->setTransactionNumber(aTransaction->number());
    aMessage->setTransTotalFlag(TheLocale.asString(theInTrainingMode).append(TheLocale.asString(theTotalType)));
    aMessage->setTotalAmount(TheLocale.asString(anAmount));
    aMessage->setTaxAmount(TheLocale.asString(aTaxAmount));
    aMessage->setTotalDescription(aTotalDescription);
    writeMessage(aMessage);
    delete aMessage;
  }
  logInfo("exiting foodstampTotal", EnterExitMethodDebug);
}

/****************************************************************************/
/* Void Transaction Total                                                   */
/****************************************************************************/
Void ISGreenPointsApplication::voidTransaction(ISTransaction* aTransaction)
{
  logInfo("in voidTransaction", FullDebug);

  if (aTransaction->isSuspendedTransaction() == FALSE)
  {
    logInfo("in voidTransaction", EnterExitMethodDebug);
    UInt voided = aTransaction->isVoidTransaction();
    UInt aTenderAmount = aTransaction->balanceDue();

    theInTrainingMode = SAMGreenPointsCardEnhancement::instance().getInTraining();
    ISGreenPointsTenderMessage* aMessage =
      ISGreenPointsMessageFactory::instance().makeTenderMessage();
    //ASHGR0007
    if (expandedHeaderRecord)
    {
      aMessage->setExpandedHeader(expandedHeaderRecord);
      aMessage->setRetailerId(retailerID.asInteger());
      aMessage->setStoreId(storeID.asInteger());
    }
    //ESHGR0007
    Int aRegisterId = 0;
    TheLocale.stringToNum(aTransaction->workstationId(), aRegisterId);
    aMessage->setRegisterId(aRegisterId);
    aMessage->setTransactionNumber(aTransaction->number());
    aMessage->setTenderFlag(TheLocale.asString(theInTrainingMode)
                            .append(TheLocale.asString(voided)));
    aMessage->setTenderType("");
    aMessage->setTenderAmount(TheLocale.asString(aTenderAmount));
    aMessage->setBalanceDue("");
    aMessage->setChangeAmount("");
    aMessage->setFoodStampChangeAmount("");
    aMessage->setTenderDescription("VOID TRANSACTION");
    logInfo("in voidTransaction..writing void message", FullDebug);
    writeMessage(aMessage);
    logInfo("in voidTransaction..void message written", FullDebug);
    delete aMessage;
    logInfo("exiting voidTransaction", EnterExitMethodDebug);
  }
}

/****************************************************************************/
/*  Add Tender Entry                                                        */
/****************************************************************************/
Void ISGreenPointsApplication::addTenderEntry(ISTenderEntry * theTenderEntry)
{
  logInfo("in addTenderEntry", FullDebug);

  //AD01675
  ////ASHGR0007 Defect 4001
  ////if (!theTenderEntry->transaction()->isRecoveryInProgress())
  //if ((!theTenderEntry->transaction()->isRecoveryInProgress()) &&
  //   (theTenderEntry->transaction()->transactionType() == ISProcedureId::SalesTransaction))
  ////ESHGR0007 Defect 4001
  if ((!theTenderEntry->transaction()->isRecoveryInProgress()) &&
      ((theTenderEntry->transaction()->transactionType() == ISTransaction::CheckoutTransaction) ||
       (theTenderEntry->transaction()->transactionType() == ISTransaction::WICTransaction)))
  //ED01675
  {

    UInt        anOperationFlag = 0;
    ISString    theTenderType = "0";
    ISString    theBalanceDue = "0";
    Int        aFoodStampChangeAmount = 0;
    Int        aChangeAmount = 0;
    ISString    thePaymentAccountDesc = "";

    //Operation Flag
    if ((theTenderEntry->transaction()->grossPositive() -
         theTenderEntry->transaction()->grossNegative()) < 0)
      anOperationFlag = 1;
    else
      anOperationFlag = 0;

    //tender type
    ISString aTenderType = TheLocale.asString(theTenderEntry->tenderType().id());
    for (Int i = 1; i <= theTotalAmountOfTenders; i++)
    {
      if (theTenderTypeVarieties[i].contains(aTenderType))
      {
        theTenderType = theGreenPointsTenderDescriptors[i];
        break;
      }
    }

    //tender amount
    Long aTenderAmount = theTenderEntry->tenderAmount();
    UInt aVoidTender = 0;
    if (aTenderAmount < 0)
      aVoidTender = 1;

    Long aBalanceDue = theTenderEntry->transaction()->balanceDue();
    if (aBalanceDue < 0)
    {
      theBalanceDue = "0";
      if (theTenderType == "3")
        aFoodStampChangeAmount = aBalanceDue;
      else
        aChangeAmount = aBalanceDue;
    }
    else
      theBalanceDue = TheLocale.asString(aBalanceDue);

    //description
    ISString aDescription = theTenderEntry->description();
    if ( theTenderType == "2")
    {
      thePaymentAccountDesc =  aDescription;
      thePaymentAccountDesc.strip().toUpper();
    }
    if (aBalanceDue != thePreviousBalanceDue)
      thePreviousBalanceDue = aBalanceDue;

    theInTrainingMode = SAMGreenPointsCardEnhancement::instance().getInTraining();
    ISGreenPointsTenderMessage* aMessage =
      ISGreenPointsMessageFactory::instance().makeTenderMessage();
    //ASHGR0007
    if (expandedHeaderRecord)
    {
      aMessage->setExpandedHeader(expandedHeaderRecord);
      aMessage->setRetailerId(retailerID.asInteger());
      aMessage->setStoreId(storeID.asInteger());
    }
    //ESHGR0007
    Int aRegisterId = 0;
    TheLocale.stringToNum(theTenderEntry->transaction()->workstationId(), aRegisterId);
    aMessage->setRegisterId(aRegisterId);
    aMessage->setTransactionNumber(theTenderEntry->transaction()->number());
    aMessage->setTenderFlag(TheLocale.asString(theInTrainingMode)
                            .append(TheLocale.asString(aVoidTender)));
    aMessage->setTenderType(theTenderType);
    aMessage->setTenderAmount(TheLocale.asString(aTenderAmount));
    aMessage->setBalanceDue(theBalanceDue);
    aMessage->setChangeAmount(TheLocale.asString(aChangeAmount));
    aMessage->setFoodStampChangeAmount(TheLocale.asString(aFoodStampChangeAmount));
    aMessage->setTenderDescription(aDescription);
    aMessage->setPaymentAccount(thePaymentAccountDesc);
    writeMessage(aMessage);
    delete aMessage;
  }
}

/****************************************************************************/
/*  End Transaction                                                         */
/****************************************************************************/
Void ISGreenPointsApplication::endTransaction(ISTransaction * aTransaction)
{
  logInfo("in endTransaction", FullDebug);
  Byte  procedureNumber = aTransaction->transactionType();
  UInt  theEOTType = 0;
  Long  aTotalAmount = 0;
  Long  aCouponAmount = 0;
  Long  aTaxAmount = 0;
  ISQuantity aQuantity = 0;
  if (procedureNumber != ISProcedureId::SignOff)
  {
    // EOT type
    if (aTransaction->isSuspendedTransaction())
      theEOTType = 1;
    else if (aTransaction->isVoidTransaction())
      theEOTType = 2;
    else
      theEOTType = 0;

    //cashier or manager id
    ISString anID = aTransaction->currentOperatorId();
    //date time
    ISString aDateTime = (TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S")).remove(0, 2);
    //total taxes
    for (UInt anIndex = 1; anIndex <= ISTaxPlan::MaxTaxPlans; anIndex++)
    {
      aTaxAmount = aTaxAmount + aTransaction->taxes(anIndex);
    }
    if (theEOTType == 1)
      aTotalAmount = 0;
    else
    {
      aTransaction->getCouponTotalsFor(aQuantity, aCouponAmount);
      aTotalAmount = aTransaction->grossNegative();
      aTotalAmount = (aTransaction->grossPositive()) - aCouponAmount;
      aTotalAmount = aTotalAmount -  aTaxAmount;
    }

    theInTrainingMode = SAMGreenPointsCardEnhancement::instance().getInTraining();
    ISGreenPointsEndTransactionMessage* aMessage =
      ISGreenPointsMessageFactory::instance().makeEndTransactionMessage();
    //ASHGR0007
    if (expandedHeaderRecord)
    {
      aMessage->setExpandedHeader(expandedHeaderRecord);
      aMessage->setRetailerId(retailerID.asInteger());
      aMessage->setStoreId(storeID.asInteger());
    }
    //ESHGR0007
    Int aRegisterId = 0;
    TheLocale.stringToNum(aTransaction->workstationId(), aRegisterId);
    aMessage->setRegisterId(aRegisterId);
    aMessage->setTransactionNumber(aTransaction->number());
    aMessage->setEndTransactionFlag(TheLocale.asString(theInTrainingMode)
                                    .append(TheLocale.asString(theEOTType)));
    aMessage->setOperatorId(aTransaction->currentOperatorId());
    aMessage->setDateTime(aDateTime);
    aMessage->setEndOfTransactionNumber(TheLocale.asString(aTransaction->number()));
    aMessage->setTotalAmount(TheLocale.asString(aTotalAmount));
    aMessage->setTaxAmount(TheLocale.asString(aTaxAmount));
    writeMessage(aMessage);
    delete aMessage;
  }

  thePreviousBalanceDue = 0;
  alreadyPrompted = 0;
  alreadyChecked = 0;
  theCapturedPhoneNumber = "";
  theSuspendedRegisterID = "";
  theSuspendedTransactionNumber = "";
  theSuspendedTotalAmount = "";
  alreadySentCardInfo = 0;
  theItemDiscountCount = 0;
  theAlreadyPromptedFlag = FALSE;
  theReceiptMessagePrinted = FALSE;
  theAlreadyPromptedApplyingDiscount = FALSE;
  theBOTMessageSent = FALSE;
  theFirstRead = FALSE;
  theReceipCardNumberPrinted = FALSE;
  theGreenPointsStoredItemsCount = 0;
  theSavedTransaction = 0;
  theItemEntered = FALSE;                                  //D01125
  setAddingTotalDiscount(FALSE);
  cardScanned = FALSE;                                     //SHGR0007
  ISBuffer aReadBuffer;
  Char  returnPipe[11];
  readMessage(theItemReadDelay);
  logInfo("exiting endTransaction", EnterExitMethodDebug);
}

//****************************************************************************
// Idle event handling - default behaviour is to do nothing
//****************************************************************************
void ISGreenPointsApplication::idle(ULong* aLong)
{
  logInfo("in idle", FullDebug);
  UInt anInputState = ISApplication2x20::instance().activeView().inputState().id();
  if ((anInputState == ISInputState::MainState) || (anInputState == ISInputState::EnterDataState)) //P1005357
  {
    if (theFirstRead == TRUE)
    {
      if (theIdleCount == theInitialPipeReadDelay)
      {
        readMessage(theItemReadDelay);
        theFirstRead = FALSE;
        theIdleCount = 0;
      }
      else
        theIdleCount++;
    }
    else
    {
      if (theIdleCount == thePipeReadDelay)
      {
        if (theGreenPointsStoredItemsCount != 0)
          retrieveItemsFromStorage();
        else
          readMessage(theItemReadDelay);
        theIdleCount = 0;
      }
      else
        theIdleCount++;
    }
  }
}

//****************************************************************************
//  Write the message to the pipe
//****************************************************************************
Void ISGreenPointsApplication::writeMessage(ISGreenPointsMessage* aMessage)
{
  logInfo("in writeMessage", FullDebug);

  UInt aLength = aMessage->messageLength();

  //AD05285
  //if coming out of offline mode and the option status is suspended,
  //the next transaction was not sending the BOT message or the card
  //number. the initializePipe method checks to see if the option status
  //is suspended.
  if (aMessage->messageType() == ISGreenPointsMessage::BeginTransaction)
    initializePipe();
  //ED05285

  if (optionStatus() == ON)
  {
    theWriteMessagePipeResult = theWriteMessagePipe->write(aMessage->messageLength(),
                                                           (Byte*) aMessage->messageData(),
                                                           theWriteMessagePipeID);
    if (theWriteMessagePipeResult.isError())
    {
      theWriteMessagePipe->close();
      theWriteMessagePipe->open(theWriteMessagePipeID);
      theWriteMessagePipeResult = theWriteMessagePipe->write(aMessage->messageLength(),
                                                             (Byte*) aMessage->messageData(),
                                                             theWriteMessagePipeID);
      if (theWriteMessagePipeResult.isError())
        setOptionStatus(SUSPENDED);
    }
  }
}

//****************************************************************************
//  seperate discount data from pipe
//****************************************************************************
Boolean ISGreenPointsApplication::seperateDiscount(Void)
{
  logInfo("in seperateDiscount..", FullDebug);
  return TRUE;
}

//****************************************************************************
//  seperate accSount data from pipe
//****************************************************************************
Boolean ISGreenPointsApplication::seperateAccountInfo(Void)
{
  logInfo("in seperateAccountInfo..", FullDebug);
  return TRUE;
}

//****************************************************************************
//  Read the message from the pipe
//****************************************************************************
Void ISGreenPointsApplication::validCardReceived(ISString aLookUpFlag)
{
  logInfo("in validCardReceived", FullDebug);

  //ISPromptDetails* thePrompt;
  ISString aPromptMessage = "";
  Boolean theValidInput = FALSE;
  Boolean aCorrectCardFlag = FALSE;
  Int aRegisterId = 0;
  showNextAccountFlag = FALSE;                             //P1005357

  //register number
  ISString aRegister = ISApplication2x20::instance().terminal().getId();
  //training mode
  UInt inTrainingMode = ISApplication2x20::instance().isInTrainingMode();
  //operator number
  ISString aCurrentOperator = ISApplication2x20::instance().terminal().getCurrentOperatorId();
  //transaction number
  UInt aTransNumber = ISApplication2x20::instance().terminal().transactionNumber();
  //AD08426.1
  //if the card number is entered first, the transaction number hasn't
  //incremented yet. go ahead and increment by 1 here so the
  //messages will match up.
  UInt state = ISApplication2x20::terminal().state();
  if ((ISApplication2x20::terminal().state() == ISTerminal::BetweenTransactions) &&
      (!inTrainingMode))
    aTransNumber++;
  //ED08426.1

  //card type
  ISString aCardType = TheLocale.asString(SAMGreenPointsCardEnhancement::instance().getKeyedOrScannedFlag());

  Char theMessage[200];
  sprintf(theMessage, "in validCardReceived..aLookUpFlag = %s", aLookUpFlag);
  logInfo(theMessage, FullDebug);

  //AP1005357
  ////Not found or network down
  //if (aLookUpFlag == "0")
  //{
  //   // Card was not found for the phone number
  //   if (theCustomerCardNumber != "")
  //   {
  //      aPromptMessage = theNoCardFromPhoneNumberMessage;
  //      aPromptMessage.append(theCustomerCardNumber);
  //Not found, already accepted, timeout/netowrk down
  if ((aLookUpFlag == "0") ||
      (aLookUpFlag == "S") ||
      (aLookUpFlag == "T") )
  {
    //set up the prompt message
    if (aLookUpFlag == "0")
      aPromptMessage = theNoCardFromPhoneNumberMessage;
    else if (aLookUpFlag == "S")
      aPromptMessage = theCardAlreadyAcceptedMessage;
    else if (aLookUpFlag == "T")
      aPromptMessage = theNetworkDownForPhoneNumberMessage;
    //EP1005357

    ISPromptDetails* thePrompt;
    //thePrompt = ISGuidanceFactory::instance().makePromptDetails(theNoCardFromPhoneNumberMessage, ""); //P1005357
    thePrompt = ISGuidanceFactory::instance().makePromptDetails(aPromptMessage, ""); //P1005357
    thePrompt->setClearKeyRequired(TRUE);
    thePrompt->setErrorToneRequired(TRUE);
    thePrompt->setRestoreDisplayRequired(TRUE);
    ISPromptUserEvent::raiseWith(thePrompt);
    delete thePrompt;

    SAMGreenPointsCardEnhancement::instance().setGreenPointsCardMessageSent(FALSE);
    SAMGreenPointsCardEnhancement::instance().setGreenPointsCardNumber("");
  }
  // Customer card number was found from phone number
  else
  {
    UInt anAccountNameLength = theCustomerAccountName.length();
    if (anAccountNameLength > 20)
      theCustomerAccountName.remove(20);
    else
      theCustomerAccountName.append(' ', (20 - anAccountNameLength));

    aPromptMessage.append(theCustomerAccountName);
    aPromptMessage.append(theGreenPointsValidAccountNameMessage);
    butNotDuringOurOwnCardEntry = TRUE;                   //D08346
    ISPosIOProcessor* posIOProcessor = (ISPosIOProcessor*)ISDeviceFactory::instance().getIOProcessor();
    UInt curState = posIOProcessor->currentState();
    ISPromptDetails* thePrompt;
    thePrompt = ISGuidanceFactory::instance().makePromptDetails(aPromptMessage, "");
    thePrompt->setClearKeyRequired(FALSE);
    thePrompt->setDataEntryRequired(TRUE);
    thePrompt->setRestoreDisplayRequired(TRUE);
    //AGUI
    UInt aState = 601002;
    ISSequencingStateChangedEvent::raiseWith(&aState);
    //EGUI
    ISPromptUserEvent::raiseWith(thePrompt);
    ISString aKey = thePrompt->getDataEntered();
    delete thePrompt;
    posIOProcessor->enableInput(curState);
    curState = posIOProcessor->currentState();

    butNotDuringOurOwnCardEntry = FALSE;                  //D08346
    if (aKey == "2")
    {
      SAMGreenPointsCardEnhancement::instance().setGreenPointsCardMessageSent(FALSE);
      SAMGreenPointsCardEnhancement::instance().setGreenPointsCardNumber("");
      clearScreen();
      showNextAccountFlag = TRUE;                        //P1005357
    }
    else
    {
      if (aKey == "1")
      {
        //AD08346
        //AP1005357
        //if ((altIDHasBeenDone) && (andWeHaveACardNumberToEnter))
        if ((altIDHasBeenDone) && (andWeHaveACardNumberToEnter) && SAMGreenPointsCardEnhancement::instance().getGreenPointsCardMessageSent())
        //EP1005357
        {
          altIDCardNeedsEMEntry = TRUE;
        }
        //ED08346
        theMessage[200];
        sprintf(theMessage, "in validCardReceived..ISKeyboardDefinition::Enter = TRUE");
        logInfo(theMessage, FullDebug);
        if (SAMGreenPointsCardEnhancement::instance().getBOTMessageSent() == FALSE)
        {
          ISString aBeginTransactionMode = TheLocale.asString(inTrainingMode).append("0");
          theTempDateTime = (TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S")).remove(0, 2);
          ISGreenPointsBeginTransactionMessage * aMessage =
            ISGreenPointsMessageFactory::instance().makeBeginTransactionMessage();
          //ASHGR0007
          if (expandedHeaderRecord)
          {
            aMessage->setExpandedHeader(expandedHeaderRecord);
            aMessage->setRetailerId(retailerID.asInteger());
            aMessage->setStoreId(storeID.asInteger());
          }
          //ESHGR0007
          TheLocale.stringToNum(aRegister, aRegisterId);
          aMessage->setRegisterId(aRegisterId);
          aMessage->setTransactionNumber(aTransNumber);
          aMessage->setBeginTransactionFlag(aBeginTransactionMode);
          aMessage->setOperatorNumber(aCurrentOperator);
          aMessage->setDateTime(theTempDateTime);
          aMessage->setBeginTransactionNumber(TheLocale.asString(aTransNumber));
          aMessage->setSuspendedRegisterId("");
          aMessage->setSuspendedTransNumber("");
          aMessage->setSuspendedTransTotal("");
          aMessage->setSuspendedTaxTotal("");
          writeMessage(aMessage);
          delete aMessage;
          setBOTMessageSent(TRUE);
        }

        ISGreenPointsFrequentBuyerMessage* aMessage =
          ISGreenPointsMessageFactory::instance().makeFrequentBuyerMessage();
        //ASHGR0007
        if (expandedHeaderRecord)
        {
          aMessage->setExpandedHeader(expandedHeaderRecord);
          aMessage->setRetailerId(retailerID.asInteger());
          aMessage->setStoreId(storeID.asInteger());
        }
        //ESHGR0007
        TheLocale.stringToNum(aRegister, aRegisterId);
        aMessage->setRegisterId(aRegisterId);
        aMessage->setTransactionNumber(aTransNumber);
        aMessage->setFrequentBuyerFlag(TheLocale.asString(inTrainingMode).append(aCardType));

        //AD08346
        if (theCustomerCardNumber.length() >= 12)
          theCustomerCardNumber = theCustomerCardNumber(0,11);
        //ED08346
        aMessage->setFrequentBuyerNumber(theCustomerCardNumber);
        ISGreenPointsApplication::instance().writeMessage(aMessage);
        delete aMessage;

        SAMGreenPointsCardEnhancement::instance().setGreenPointsCardMessageSent(TRUE);
        SAMGreenPointsCardEnhancement::instance().setGreenPointsCardNumber(theCustomerCardNumber);
        SAMGreenPointsCardEnhancement::instance().setGreenPointsCardEntered(TRUE);
        clearScreen();
        //AP1005357 - this code was taken from the code commented out in
        //the idle method with a few minor changes
        UInt anInputState = ISApplication2x20::instance().activeView().inputState().id();
        ISWindow2x20 *aWindow = &ISApplication2x20::instance().activeView();
        andWeHaveACardNumberToEnter = FALSE;
        altIDCardNeedsEMEntry = FALSE;
        altIDHasBeenDone = FALSE;
        if (theCustomerCardNumber.length() > 10)
        {
          ISInputSequence *sequence = new ISInputSequence();
          sequence->source(ISInputSequence::SourceInternal);
          sequence->motorKey(ISKeyboardDefinition::Enter);
          sequence->setManagersKeyPresent(FALSE);
          sequence->setError(ISInputSequence::None);
          sequence->definition(0);
          //AP1005357
          //strip leading zeros from card number
          theCustomerCardNumber = theCustomerCardNumber.strip(ISString::leading, '0');
          //EP1005357
          sequence->addEntry(ISKeyboardDefinition::Enter, theCustomerCardNumber);
          aWindow->setIdleWaitTime(1);
          changedWaitTimeNowChangeitBack = TRUE;
          aWindow->inputState().inputReceived(*sequence);
          if (sequence)
            delete sequence;
        }
        //EP1005357
      }
      else
      {
        SAMGreenPointsCardEnhancement::instance().setGreenPointsCardMessageSent(FALSE);
        SAMGreenPointsCardEnhancement::instance().setGreenPointsCardNumber("");
        ISPromptDetails* thePrompt;
        thePrompt = ISGuidanceFactory::instance().makePromptDetails("OTHER INPUT RECEIVED", "");
        thePrompt->setClearKeyRequired(TRUE);
        thePrompt->setDataEntryRequired(FALSE);
        thePrompt->setRestoreDisplayRequired(TRUE);
        ISPromptUserEvent::raiseWith(thePrompt);
        delete thePrompt;
      }
    }
  }
  logInfo("exiting validCardReceived", EnterExitMethodDebug);
}

//****************************************************************************
//  Read the message from the pipe
//****************************************************************************
//AD08426
//Void ISGreenPointsApplication::readMessage(UInt aReadDelay)
int ISGreenPointsApplication::readMessage(UInt aReadDelay)
//ED08426
{
  logInfo("in readMessage", FullDebug);

  Char theMessage[200];
  ISBuffer aDiscountBuffer;
  ISBuffer thePartialBuffer = 0;
  Int theStartPos = 0;
  Int theEndPos = 0;
  Int anAccountCount = 0;
  Int16 aFieldValue = 0;
  Boolean readAgain = FALSE;
  ISString theItemCode = "";
  Int aTempLocation = 0;
  Int aNewTempLocation = -1;
  ISBuffer aTempBuffer;
  theItemDiscountCount = 0;
  Int16 aMessageLength = 0;

  if (optionStatus() == ON)
  {
    ISBuffer aReadBuffer;
    Char  returnPipe[11];
    Char anSTX = 0x02;
    Char anETX = 0x03;

    ISString aBufferString = "";

    do
    {
      aReadBuffer.release();
      theReadMessagePipeResult = theReadMessagePipe->read(aReadDelay, theReadPipeSize, aReadBuffer, returnPipe);
      for (int j = 0; j <= aReadBuffer.getDataLength(); j++)
      {
        anSTX = aReadBuffer[j];
        if ((anSTX >= 48 && anSTX <= 57) || (anSTX >= 65 && anSTX <= 90) || (anSTX >= 97 && anSTX <= 122))
        {
          aBufferString.append(anSTX);
          aBufferString.append("");
        }
        else
          aBufferString.append(".");
      }
      sprintf(theMessage, "in readMessage..aReadBuffer length= %d", aReadBuffer.getDataLength());
      logInfo(theMessage, FullDebug);
      if (theReadMessagePipeResult.isSuccess() == TRUE)
      {
        //AD08346
        ISWindow2x20 *aWindow = &ISApplication2x20::instance().activeView();
        if (changedWaitTimeNowChangeitBack)
        {
          aWindow->setIdleWaitTime(50);
          changedWaitTimeNowChangeitBack = FALSE;
        }
        //ED08346
        if (readAgain == TRUE)
          aReadBuffer.prepend(thePartialBuffer);
        do
        {
          anSTX = aReadBuffer[0];
          sprintf(theMessage, "in readMessage..pipe read was success.. anSTX = %d", anSTX);
          logInfo(theMessage, FullDebug);
          if (anSTX == 2)
          {
            aTempBuffer.create(1, (Byte) 0);
            aTempBuffer.putByteAt(0, 0x03);
            aTempLocation  = 0;
            if (!expandedHeaderRecord)
              aTempLocation = aReadBuffer.locate(aTempBuffer, 1, 7, TRUE);
            else
              aTempLocation = aReadBuffer.locate(aTempBuffer, 1, 11, TRUE);
            //ESHGR0007
            sprintf(theMessage, "in readMessage..pipe read was success.. aTempLocation = %d", aTempLocation);
            logInfo(theMessage, FullDebug);
            if (aTempLocation == -1)
            {
              readAgain = TRUE;
              thePartialBuffer = aReadBuffer;
              logInfo("in readMessage..inside aTempLocation == -1", FullDebug);
              logInfo("in readMessage..no ETX found", FullDebug);
              aTempLocation = 0;
            }
            else
            {
              readAgain = FALSE;
              anETX = aReadBuffer[aTempLocation];
              sprintf(theMessage, "in readMessage..pipe read was success.. anETX = %d", anETX);
              logInfo(theMessage, FullDebug);
              if (anETX == 3)
              {
                theEndPos = aTempLocation + 1;
                Int16 aTransactionNumber = 0;      //D08426
                if ((anSTX == 2) &&
                    (anETX == 3))
                {
                  //get the transaction number
                  aMessageLength = aReadBuffer.bigEndianIntegerAt(1);
                  //ASHGR0007
                  if (expandedHeaderRecord)
                  {
                    Int16 aRetailerId = aReadBuffer.bigEndianIntegerAt(3);
                    Int16 aStoreId = aReadBuffer.bigEndianIntegerAt(5);
                    Int16 aRegisterId = aReadBuffer.bigEndianIntegerAt(7);
                    //AD08426
                    //Int16 aTransactionNumber = aReadBuffer.bigEndianIntegerAt(9);
                    aTransactionNumber = aReadBuffer.bigEndianIntegerAt(9);
                    //ED08426
                  }
                  else
                  {
                    Int16 aRegisterId = aReadBuffer.bigEndianIntegerAt(3);
                    aTransactionNumber = aReadBuffer.bigEndianIntegerAt(5);
                  }
                  //AD08426
                  UInt aTransNumber = ISApplication2x20::instance().terminal().transactionNumber();
                  //AD08426.1
                  //if the card number is entered first, the transaction number
                  //hasn't incremented yet. go ahead and increment by 1 here so the
                  //messages will match up.
                  UInt state = ISApplication2x20::terminal().state();
                  UInt BOTFlag = 0;//D25833
                  sprintf(theMessage, "in readMessage..Initialized BOTFlag");
                  logInfo(theMessage, FullDebug);
                  if (ISApplication2x20::terminal().state() == ISTerminal::BetweenTransactions)
                  {
                    sprintf(theMessage, "in readMessage..ISTerminal::BetweenTransactions = TRUE");
                    logInfo(theMessage, FullDebug);
                    aTransNumber++;
                    BOTFlag = 1;//D25833
                  }
                  sprintf(theMessage, "in readMessage..BOTFlag = %d", BOTFlag);
                  logInfo(theMessage, FullDebug);
                  //if (!((transactionHasStarted) && (aTransactionNumber == aTransNumber)))
                  if ((aTransactionNumber != aTransNumber) && !BOTFlag)//D25833
                  //ED08426.1
                  {
                    return 0;
                  }
                  //ED08426
                  //ESHGR0007

                  //ASHGR0007
                  ISString aMidType;
                  if (expandedHeaderRecord)
                    aMidType = aReadBuffer.stringAt(11, 1);
                  else
                    aMidType = aReadBuffer.stringAt(7, 1);
                  //ESHGR0007
                  sprintf(theMessage, "in readMessage..pipe read was success.. aMidType = %s", aMidType);
                  logInfo(theMessage, FullDebug);
                  if (aMidType == "C")
                  {
                    //ASHGR0007
                    ISString aDiscountFlag;
                    if (expandedHeaderRecord)
                      aDiscountFlag = aReadBuffer.stringAt(12, 1);
                    else
                      aDiscountFlag = aReadBuffer.stringAt(8, 1);
                    //ESHGR0007
                    sprintf(theMessage, "in readMessage..aMidType = C..aDiscountFlag = %s", aDiscountFlag);
                    logInfo(theMessage, FullDebug);
                    if ((aDiscountFlag == "1") ||
                        (aDiscountFlag == "2"))
                    {
                      aTempLocation = -1;
                      aTempBuffer.release();
                      aTempBuffer.create(1, (Byte) 0);
                      aTempBuffer.putByteAt(0, theFieldSeperator);
                      //discount item code
                      //ASHGR0007
                      if (expandedHeaderRecord)
                        aTempLocation = aReadBuffer.locate(aTempBuffer, 1, 14);
                      else
                        aTempLocation = aReadBuffer.locate(aTempBuffer, 1, 10);
                      //ESHGR0007
                      sprintf(theMessage, "in readMessage..aTempLocation = %d", aTempLocation);
                      logInfo(theMessage, FullDebug);
                      sprintf(theMessage, "in readMessage..theEndPos = %d", theEndPos);
                      logInfo(theMessage, FullDebug);
                      if ((aTempLocation < theEndPos) &&
                          (aTempLocation != 0) &&
                          (aTempLocation != -1))
                      {
                        ISString anItemDiscountDescriptor = "";
                        //ASHGR0007
                        ISString anItemCodeString;
                        if (expandedHeaderRecord)
                          anItemCodeString = aReadBuffer.stringAt(14, ((aTempLocation) - 14));
                        else
                          anItemCodeString = aReadBuffer.stringAt(10, ((aTempLocation) - 10));
                        //ESHGR0007
                        if (anItemCodeString.length() == 12)
                          anItemCodeString.remove(11, 1);

                        //discount description
                        aTempLocation += 1;
                        aNewTempLocation = aReadBuffer.locate(aTempBuffer, 1, aTempLocation);
                        sprintf(theMessage, "in readMessage..aNewTempLocation = %d", aNewTempLocation);
                        logInfo(theMessage, FullDebug);
                        if ((aTempLocation < theEndPos) &&
                            (aTempLocation != 0) &&
                            (aTempLocation != -1))
                        {
                          anItemDiscountDescriptor = aReadBuffer.stringAt(aTempLocation, (aNewTempLocation - aTempLocation));
                          ISString aSpaceString(0x20, anItemDiscountDescriptor.length());
                          if (anItemDiscountDescriptor == aSpaceString)
                            anItemDiscountDescriptor = "";

                          sprintf(theMessage, "in readMessage..anItemDiscountDescriptor = %s", anItemDiscountDescriptor);
                          logInfo(theMessage, FullDebug);

                          //discount quantity
                          UInt anItemDiscountQuantity = 0;
                          aTempLocation = aNewTempLocation + 1;
                          aNewTempLocation = aReadBuffer.locate(aTempBuffer, 1, aTempLocation);
                          sprintf(theMessage, "in readMessage..aNewTempLocation = %d", aNewTempLocation);
                          logInfo(theMessage, FullDebug);

                          if ((aTempLocation < theEndPos) &&
                              (aTempLocation != 0) &&
                              (aTempLocation != -1))
                          {
                            TheLocale.stringToNum(aReadBuffer.stringAt(aTempLocation, (aNewTempLocation - aTempLocation)), anItemDiscountQuantity);
                            sprintf(theMessage, "in readMessage..anItemDiscountQuantity = %d", anItemDiscountQuantity);
                            logInfo(theMessage, FullDebug);
                            if (aDiscountFlag == "1")
                              theItemDiscountCount += 1;
                            if (aDiscountFlag == "2")
                              theItemDiscountCount -= 1;

                            logInfo("in readMessage..adding item", FullDebug);
                            if (theSavedTransaction != 0)
                              addGreenPointsItem(aDiscountFlag, anItemCodeString, anItemDiscountDescriptor, anItemDiscountQuantity,  *theSavedTransaction);
                          }
                        }
                      }
                    }
                  }
                  else
                  {
                    //customer account information
                    if (aMidType == "D")
                    {
                      //ASHGR0007
                      ISString aValidFlag;
                      if (expandedHeaderRecord)
                        aValidFlag = aReadBuffer.stringAt(12, 1);
                      else
                        aValidFlag = aReadBuffer.stringAt(8, 1);
                      //ESHGR0007
                      sprintf(theMessage, "in readMessage..aValidFlag = %s",aValidFlag);
                      logInfo(theMessage, FullDebug);
                      if (aValidFlag == "1")
                      {
                        //AP1005357
                        Int aDelimterLocation = -1;
                        Int nextDelimterLocation = -1;
                        //EP1005357
                        aTempLocation = -1;
                        aTempBuffer.release();
                        aTempBuffer.create(1, (Byte) 0);
                        aTempBuffer.putByteAt(0, theFieldSeperator);

                        //frequent buyer card number
                        //AP1005357
                        //set starting delimiter depending on type of record
                        aDelimterLocation = expandedHeaderRecord ? 14 : 10;
                        //get frequent buyer card number
                        nextDelimterLocation = aReadBuffer.locate(aTempBuffer, 1, aDelimterLocation);
                        if ((nextDelimterLocation < theEndPos) &&
                            (nextDelimterLocation != 0) &&
                            (nextDelimterLocation != -1))  //found a delimiter
                        {
                          ISBuffer aNewBuffer = aReadBuffer.bufferAt(aDelimterLocation,((nextDelimterLocation) - aDelimterLocation) +5);
                          theCustomerCardNumber = aReadBuffer.stringAt(aDelimterLocation, ((nextDelimterLocation) - aDelimterLocation));
                          //strip leading zeros from card number
                          theCustomerCardNumber = theCustomerCardNumber.strip(ISString::leading, '0');
                          if (theCustomerCardNumber.length() >= 12)
                            theCustomerCardNumber = theCustomerCardNumber(0,11);
                          aTempLocation = nextDelimterLocation;
                          //EP1005357
                          theCustomerLoyalLevel = "";
                          aTempLocation += 1;
                          aNewTempLocation = aReadBuffer.locate(aTempBuffer, 1, aTempLocation);

                          // Customer Loyal Level
                          if ((aTempLocation < theEndPos) &&
                              (aTempLocation != 0) &&
                              (aTempLocation != -1))
                          {
                            theCustomerLoyalLevel = aReadBuffer.stringAt(aTempLocation, (aNewTempLocation - aTempLocation));
                            theCustomerAccountName = "";
                            aTempLocation = aNewTempLocation + 1;
                            aNewTempLocation = aReadBuffer.locate(aTempBuffer, 1, aTempLocation);
                            if ((aTempLocation < theEndPos) &&
                                (aTempLocation != 0) &&
                                (aTempLocation != -1))
                            {
                              theCustomerAccountName = aReadBuffer.stringAt(aTempLocation, (aNewTempLocation - aTempLocation));
                              if (theCustomerAccountName.length() > 20)
                                theCustomerAccountName.remove(20);
                              else
                                theCustomerAccountName.append(" ", (20-theCustomerAccountName.length()));

                              theCustomerPointBalance = "";
                              aTempLocation = aNewTempLocation + 1;
                              aNewTempLocation = aReadBuffer.locate(aTempBuffer, 1, aTempLocation);

                              theMessage[200];
                              sprintf(theMessage, "in readMessage..aNewTempLocation = %d", aNewTempLocation);
                              logInfo(theMessage, FullDebug);

                              if ((aTempLocation < theEndPos) &&
                                  (aTempLocation != 0) &&
                                  (aTempLocation != -1))
                              {
                                theCustomerPointBalance = aReadBuffer.stringAt(aTempLocation, (aNewTempLocation - aTempLocation));
                                theCustomerCardType = "";
                                aTempLocation = aNewTempLocation + 1;
                                aNewTempLocation = aReadBuffer.locate(aTempBuffer, 1, aTempLocation);
                                // Customer Card Type
                                if ((aTempLocation < theEndPos) &&
                                    (aTempLocation != 0) &&
                                    (aTempLocation != -1))
                                {
                                  theCustomerCardType = aReadBuffer.stringAt(aTempLocation, (aNewTempLocation - aTempLocation));
                                  theCustomerYearToDateSavings = "";
                                  aTempLocation = aNewTempLocation + 1;
                                  aNewTempLocation = aReadBuffer.locate(aTempBuffer, 1, aTempLocation);
                                  // Customer Year To Date Savings
                                  if ((aTempLocation < theEndPos) &&
                                      (aTempLocation != 0) &&
                                      (aTempLocation != -1))
                                  {
                                    theCustomerYearToDateSavings = aReadBuffer.stringAt(aTempLocation, (aNewTempLocation - aTempLocation));
                                    theCustomerPriceOfAPointInCent = "";
                                    theCustomerYearToDateSavings = "";
                                    aTempLocation = aNewTempLocation + 1;
                                    aNewTempLocation = aReadBuffer.locate(aTempBuffer, 1, aTempLocation);
                                    // Customer Price Of A Point In Cents
                                    if ((aTempLocation < theEndPos) &&
                                        (aTempLocation != 0) &&
                                        (aTempLocation != -1))
                                    {
                                      theCustomerPriceOfAPointInCent = aReadBuffer.stringAt(aTempLocation, (aNewTempLocation - aTempLocation));
                                      aReadBuffer.remove(0, (aTempLocation));
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                        //ASHGR0007
                        ISPromptDetails* thePrompt;
                        if (displayCustomerInfo)
                        {
                          cardScanned = TRUE;
                          ISString pointPrompt = ParsePointsPrompt(theCustomerAccountName,theCustomerPointBalance,theCustomerLoyalLevel,messageForAccountInfo);
                          thePrompt = ISGuidanceFactory::instance().makePromptDetails(pointPrompt, "");
                        }
                        else
                        {
                          thePrompt = ISGuidanceFactory::instance().makePromptDetails(theGreenPointsWelcomeMessage, "");
                        }
                        //ESHGR0007
                        thePrompt->setClearKeyRequired(FALSE);
                        thePrompt->setErrorToneRequired(FALSE);
                        thePrompt->setRestoreDisplayRequired(FALSE);
                        ISPromptUserEvent::raiseWith(thePrompt);
                        delete thePrompt;
                        sprintf(theMessage, "in readMessage..Entering Code for D1 message");
                        logInfo(theMessage, FullDebug);
                        //AD25833
                        if (SAMGreenPointsCardEnhancement::instance().getBOTMessageSent() == FALSE)
                        {
                          sprintf(theMessage, "in readMessage..getBOTMessageSent = FALSE");
                          logInfo(theMessage, FullDebug);
                          ISString aCurrentOperator = ISApplication2x20::instance().terminal().getCurrentOperatorId();
                          Int aRegisterId = 0;
                          //register number
                          ISString aRegister = ISApplication2x20::instance().terminal().getId();
                          //training mode
                          UInt inTrainingMode = ISApplication2x20::instance().isInTrainingMode();
                          ISString aBeginTransactionMode = TheLocale.asString(inTrainingMode).append("0");
                          theTempDateTime = (TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S")).remove(0, 2);
                          ISGreenPointsBeginTransactionMessage * aMessage =
                          ISGreenPointsMessageFactory::instance().makeBeginTransactionMessage();
                          aMessage->setBeginTransactionFlag(aBeginTransactionMode);
                          aMessage->setOperatorNumber(aCurrentOperator);
                          aMessage->setDateTime(theTempDateTime);
                          aMessage->setBeginTransactionNumber(TheLocale.asString(aTransNumber));
                          aMessage->setSuspendedRegisterId("");
                          aMessage->setSuspendedTransNumber("");
                          aMessage->setSuspendedTransTotal("");
                          aMessage->setSuspendedTaxTotal("");
                          if (expandedHeaderRecord)
                          {
                            aMessage->setExpandedHeader(expandedHeaderRecord);
                            aMessage->setRetailerId(retailerID.asInteger());
                            aMessage->setStoreId(storeID.asInteger());
                          }
                          //ESHGR0007
                          TheLocale.stringToNum(aRegister, aRegisterId);
                          aMessage->setRegisterId(aRegisterId);
                          aMessage->setTransactionNumber(aTransNumber);
                          ISGreenPointsApplication::instance().writeMessage(aMessage);
                          delete aMessage;
                          SAMGreenPointsCardEnhancement::instance().setBOTMessageSent(TRUE);
                        }
                        sprintf(theMessage, "in readMessage..aValidFlag = %s", aValidFlag);
                        logInfo(theMessage, FullDebug);
                        ISGreenPointsFrequentBuyerMessage* aMessage =
                         ISGreenPointsMessageFactory::instance().makeFrequentBuyerMessage();
                        Int aRegisterId = 0;
                        //register number
                        ISString aRegister = ISApplication2x20::instance().terminal().getId();
                        //training mode
                        UInt inTrainingMode = ISApplication2x20::instance().isInTrainingMode();
                        //card type
                        ISString aCardType = TheLocale.asString(SAMGreenPointsCardEnhancement::instance().getKeyedOrScannedFlag());
                        //ASHGR0007
                        if (expandedHeaderRecord)
                        {
                          aMessage->setExpandedHeader(expandedHeaderRecord);
                          aMessage->setRetailerId(retailerID.asInteger());
                          aMessage->setStoreId(storeID.asInteger());
                        }
                        //ESHGR0007
                        TheLocale.stringToNum(aRegister, aRegisterId);
                        aMessage->setRegisterId(aRegisterId);
                        aMessage->setTransactionNumber(aTransNumber);
                        aMessage->setFrequentBuyerFlag(TheLocale.asString(inTrainingMode).append(aCardType));
                        //AD08346
                        if (theCustomerCardNumber.length() >= 12)
                          theCustomerCardNumber = theCustomerCardNumber(0,11);
                        //ED08346
                        sprintf(theMessage, "in readMessage..theCustomerCardNumber = %s", theCustomerCardNumber);
                        logInfo(theMessage, FullDebug);
                        aMessage->setFrequentBuyerNumber(theCustomerCardNumber);
                        ISGreenPointsApplication::instance().writeMessage(aMessage);
                        delete aMessage;
                        //ED25833
                        SAMGreenPointsCardEnhancement::instance().setGreenPointsCardEntered(TRUE);
                        SAMGreenPointsCardEnhancement::instance().setGreenPointsCardMessageSent(TRUE);
                        SAMGreenPointsCardEnhancement::instance().setGreenPointsCardNumber(theCustomerCardNumber);
                        //AD25833
                        clearScreen();

                        //AD26775
                        // Only add the customer number from the JBrain to the transaction
                        // if the customer number is not the current one in the transaction
                        if (!customerInTrans(theCustomerCardNumber))
                        {
                          sprintf(theMessage, "in readMessage..theCustomerCardNumber not already in trans");
                          logInfo(theMessage, FullDebug);
                        //ED26775

                          //AP1005357 - this code was taken from the code commented out in
                         //the idle method with a few minor changes
                          UInt anInputState = ISApplication2x20::instance().activeView().inputState().id();
                          ISWindow2x20 *aWindow = &ISApplication2x20::instance().activeView();
                          andWeHaveACardNumberToEnter = FALSE;
                          altIDCardNeedsEMEntry = FALSE;
                          altIDHasBeenDone = FALSE;
                          if (theCustomerCardNumber.length() > 10)
                          {
                            sprintf(theMessage, "in readMessage..theCustomerCardNumber length greater than 10");
                            logInfo(theMessage, FullDebug);
                            ISInputSequence *sequence = new ISInputSequence();
                            sequence->source(ISInputSequence::SourceInternal);
                            sequence->motorKey(ISKeyboardDefinition::Enter);
                            sequence->setManagersKeyPresent(FALSE);
                            sequence->setError(ISInputSequence::None);
                            sequence->definition(0);
                            //AP1005357
                            //strip leading zeros from card number
                            theCustomerCardNumber = theCustomerCardNumber.strip(ISString::leading, '0');
                            //EP1005357
                            sequence->addEntry(ISKeyboardDefinition::Enter, theCustomerCardNumber);
                            aWindow->setIdleWaitTime(1);
                            changedWaitTimeNowChangeitBack = TRUE;
                            aWindow->inputState().inputReceived(*sequence);
                            if (sequence)
                              delete sequence;
                          }
                        }                                  //D26775

                        sprintf(theMessage, "in readMessage..exiting from the code to add phone number from ntr");
                        logInfo(theMessage, FullDebug);
                        //ED25833
                      }
                      else
                      {
                        if (aValidFlag == "0")
                        {
                          ISPromptDetails* thePrompt = ISGuidanceFactory::instance().makePromptDetails(theGreenPointsInvalidCardMessage, "");
                          thePrompt->setClearKeyRequired(TRUE);
                          thePrompt->setErrorToneRequired(TRUE);
                          thePrompt->setRestoreDisplayRequired(TRUE);
                          ISPromptUserEvent::raiseWith(thePrompt);
                          delete thePrompt;
                          SAMGreenPointsCardEnhancement::instance().setGreenPointsCardEntered(FALSE);
                          SAMGreenPointsCardEnhancement::instance().setGreenPointsCardMessageSent(FALSE);
                        }
                        else
                        {
                          if (aValidFlag == "2")
                          {
                            //AP1005357
                            Int aDelimterLocation = -1;
                            Int nextDelimterLocation = -1;
                            aTempLocation = -1;
                            //EP1005357
                            aTempBuffer.release();
                            aTempBuffer.create(1, (Byte) 0);
                            aTempBuffer.putByteAt(0, theFieldSeperator);
                            //AP1005357
                            //set starting delimiter depending on type of record
                            aDelimterLocation = expandedHeaderRecord ? 14 : 10;
                            //get frequent buyer card number
                            nextDelimterLocation = aReadBuffer.locate(aTempBuffer, 1, aDelimterLocation);
                            if ((nextDelimterLocation < theEndPos) &&
                                (nextDelimterLocation != 0) &&
                                (nextDelimterLocation != -1))  //found a delimiter
                            {
                              theCustomerCardNumber = aReadBuffer.stringAt(aDelimterLocation, ((nextDelimterLocation) - aDelimterLocation));
                              //strip leading zeros from card number
                              theCustomerCardNumber = theCustomerCardNumber.strip(ISString::leading, '0');
                              //AD08346
                              if (theCustomerCardNumber.length() >= 12)
                                theCustomerCardNumber = theCustomerCardNumber(0,11);
                              aReadBuffer.release();
                            }
                            ISPromptDetails* thePrompt = ISGuidanceFactory::instance().makePromptDetails(theGreenPointsWelcomeMessage, "");
                            thePrompt->setClearKeyRequired(FALSE);
                            thePrompt->setErrorToneRequired(FALSE);
                            thePrompt->setRestoreDisplayRequired(FALSE);
                            ISPromptUserEvent::raiseWith(thePrompt);
                            delete thePrompt;
                            sprintf(theMessage, "in readMessage..Entering Code for D2 message");
                            logInfo(theMessage, FullDebug);
                            //AD25833
                            if (SAMGreenPointsCardEnhancement::instance().getBOTMessageSent() == FALSE)
                            {
                              sprintf(theMessage, "in readMessage..getBOTMessageSent = FALSE");
                              logInfo(theMessage, FullDebug);
                              ISString aCurrentOperator = ISApplication2x20::instance().terminal().getCurrentOperatorId();
                              Int aRegisterId = 0;
                              //register number
                              ISString aRegister = ISApplication2x20::instance().terminal().getId();
                              //training mode
                              UInt inTrainingMode = ISApplication2x20::instance().isInTrainingMode();
                              ISString aBeginTransactionMode = TheLocale.asString(inTrainingMode).append("0");
                              theTempDateTime = (TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S")).remove(0, 2);
                              ISGreenPointsBeginTransactionMessage * aMessage =
                              ISGreenPointsMessageFactory::instance().makeBeginTransactionMessage();
                              aMessage->setBeginTransactionFlag(aBeginTransactionMode);
                              aMessage->setOperatorNumber(aCurrentOperator);
                              aMessage->setDateTime(theTempDateTime);
                              aMessage->setBeginTransactionNumber(TheLocale.asString(aTransNumber));
                              aMessage->setSuspendedRegisterId("");
                              aMessage->setSuspendedTransNumber("");
                              aMessage->setSuspendedTransTotal("");
                              aMessage->setSuspendedTaxTotal("");
                              if (expandedHeaderRecord)
                              {
                                aMessage->setExpandedHeader(expandedHeaderRecord);
                                aMessage->setRetailerId(retailerID.asInteger());
                                aMessage->setStoreId(storeID.asInteger());
                              }
                              //ESHGR0007
                              TheLocale.stringToNum(aRegister, aRegisterId);
                              aMessage->setRegisterId(aRegisterId);
                              aMessage->setTransactionNumber(aTransNumber);
                              ISGreenPointsApplication::instance().writeMessage(aMessage);
                              delete aMessage;
                              SAMGreenPointsCardEnhancement::instance().setBOTMessageSent(TRUE);
                            }
                            sprintf(theMessage, "in readMessage..aValidFlag = %s", aValidFlag);
                            logInfo(theMessage, FullDebug);
                            ISGreenPointsFrequentBuyerMessage* aMessage =
                             ISGreenPointsMessageFactory::instance().makeFrequentBuyerMessage();
                            Int aRegisterId = 0;
                            //register number
                            ISString aRegister = ISApplication2x20::instance().terminal().getId();
                            //training mode
                            UInt inTrainingMode = ISApplication2x20::instance().isInTrainingMode();
                            //card type
                            ISString aCardType = TheLocale.asString(SAMGreenPointsCardEnhancement::instance().getKeyedOrScannedFlag());
                            //ASHGR0007
                            if (expandedHeaderRecord)
                            {
                              aMessage->setExpandedHeader(expandedHeaderRecord);
                              aMessage->setRetailerId(retailerID.asInteger());
                              aMessage->setStoreId(storeID.asInteger());
                            }
                            //ESHGR0007
                            TheLocale.stringToNum(aRegister, aRegisterId);
                            aMessage->setRegisterId(aRegisterId);
                            aMessage->setTransactionNumber(aTransNumber);
                            aMessage->setFrequentBuyerFlag(TheLocale.asString(inTrainingMode).append(aCardType));
                            //AD08346
                            if (theCustomerCardNumber.length() >= 12)
                              theCustomerCardNumber = theCustomerCardNumber(0,11);
                            //ED08346
                            sprintf(theMessage, "in readMessage..theCustomerCardNumber = %s", theCustomerCardNumber);
                            logInfo(theMessage, FullDebug);
                            aMessage->setFrequentBuyerNumber(theCustomerCardNumber);
                            ISGreenPointsApplication::instance().writeMessage(aMessage);
                            delete aMessage;
                            //ED25833
                            SAMGreenPointsCardEnhancement::instance().setGreenPointsCardEntered(TRUE);
                            SAMGreenPointsCardEnhancement::instance().setGreenPointsCardMessageSent(TRUE);
                            SAMGreenPointsCardEnhancement::instance().setGreenPointsCardNumber(theCustomerCardNumber);
                            //AD25833
                            clearScreen();

                            //AD26775
                            // Only add the customer number from the JBrain to the transaction
                            // if the customer number is not the current one in the transaction
                            if (!customerInTrans(theCustomerCardNumber))
                            {
                              sprintf(theMessage, "in readMessage..theCustomerCardNumber not already in trans");
                              logInfo(theMessage, FullDebug);
                            //ED26775

                              //AP1005357 - this code was taken from the code commented out in
                              //the idle method with a few minor changes
                              UInt anInputState = ISApplication2x20::instance().activeView().inputState().id();
                              ISWindow2x20 *aWindow = &ISApplication2x20::instance().activeView();
                              andWeHaveACardNumberToEnter = FALSE;
                              altIDCardNeedsEMEntry = FALSE;
                              altIDHasBeenDone = FALSE;
                              if (theCustomerCardNumber.length() > 10)
                              {
                                sprintf(theMessage, "in readMessage..theCustomerCardNumber length greater than 10");
                                logInfo(theMessage, FullDebug);
                                ISInputSequence *sequence = new ISInputSequence();
                                sequence->source(ISInputSequence::SourceInternal);
                                sequence->motorKey(ISKeyboardDefinition::Enter);
                                sequence->setManagersKeyPresent(FALSE);
                                sequence->setError(ISInputSequence::None);
                                sequence->definition(0);
                                //AP1005357
                                //strip leading zeros from card number
                                theCustomerCardNumber = theCustomerCardNumber.strip(ISString::leading, '0');
                                //EP1005357
                                sequence->addEntry(ISKeyboardDefinition::Enter, theCustomerCardNumber);
                                aWindow->setIdleWaitTime(1);
                                changedWaitTimeNowChangeitBack = TRUE;
                                aWindow->inputState().inputReceived(*sequence);
                                if (sequence)
                                  delete sequence;
                              }
                            }                              //D26775

                            sprintf(theMessage, "in readMessage..exiting from the code to add phone number from ntr");
                            logInfo(theMessage, FullDebug);
                            //ED25833
                          }
                        }
                      }
                    }
                    else
                    {
                      //phone number lookup response
                      if (aMidType == "F")
                      {
                        Boolean bRecordsReturned = FALSE; //P1005357
                        //ASHGR0007
                        ISString aValidFlag;
                        if (expandedHeaderRecord)
                          aValidFlag = aReadBuffer.stringAt(12, 1);
                        else
                          aValidFlag = aReadBuffer.stringAt(8, 1);
                        //ESHGR0007
                        //AP1005357
                        //aValidFlag: 0:not found, 1-9: num of records, S: card already accepted, T: Timeout/network down
                        switch (aValidFlag[0])
                        {
                          case 'S':  //card already accepted
                            logInfo("Card Already Accepted", FullDebug);
                            break;
                          case 'T':  //network timeout
                            logInfo("Timeout Network Down", FullDebug);
                            break;
                          case '0':  //not found
                            logInfo("Card Not Found", FullDebug);
                            break;
                          case '1':  //num of records returned
                          case '2':
                          case '3':
                          case '4':
                          case '5':
                          case '6':
                          case '7':
                          case '8':
                          case '9':
                            Char theMessage[200];
                            sprintf(theMessage, "%s Number of Accounts returned", aValidFlag);
                            logInfo(theMessage, FullDebug);
                            bRecordsReturned = TRUE;
                            break;
                          default:
                            break;
                        }

                        if (bRecordsReturned)
                        {
                          Int aDelimterLocation = -1;
                          Int nextDelimterLocation = -1;
                          UInt numberOfAccounts;
                          TheLocale.stringToNum(aValidFlag, numberOfAccounts);
                          //need to loop through all the records if there are more than one
                          showNextAccountFlag = FALSE;
                          //create delimeter buffer
                          aTempBuffer.release();
                          aTempBuffer.create(1, (Byte) 0);
                          aTempBuffer.putByteAt(0, theFieldSeperator);
                          //set starting delimiter depending on type of record
                          aDelimterLocation = expandedHeaderRecord ? 14 : 10;
                          do
                          {
                            numberOfAccounts--;
                            //get first field (frequent buyer card number)
                            nextDelimterLocation = aReadBuffer.locate(aTempBuffer, 1, aDelimterLocation);
                            if ((nextDelimterLocation < theEndPos) &&
                                (nextDelimterLocation != 0) &&
                                (nextDelimterLocation != -1))  //found a delimiter
                            {
                              theCustomerCardNumber = aReadBuffer.stringAt(aDelimterLocation, ((nextDelimterLocation) - aDelimterLocation));
                              theCustomerCardNumber = theCustomerCardNumber.strip(ISString::leading, '0');
                              //AD08346
                              if (theCustomerCardNumber.length() >= 12)
                                theCustomerCardNumber = theCustomerCardNumber(0,11);
                              if (altIDHasBeenDone)
                                andWeHaveACardNumberToEnter = TRUE;
                              //ED08346
                              aDelimterLocation = nextDelimterLocation + 1;
                              //get second field (account name)
                              theCustomerAccountName = "";
                              nextDelimterLocation = aReadBuffer.locate(aTempBuffer, 1, aDelimterLocation);
                              if ((nextDelimterLocation < theEndPos) &&
                                  (nextDelimterLocation != 0) &&
                                  (nextDelimterLocation != -1))  //found a delimiter
                                theCustomerAccountName = aReadBuffer.stringAt(aDelimterLocation, ((nextDelimterLocation) - aDelimterLocation));

                              aDelimterLocation = nextDelimterLocation + 1;
                              //get third field (date of birth)
                              theCustomerDateOfBirth = "";
                              nextDelimterLocation = aReadBuffer.locate(aTempBuffer, 1, aDelimterLocation);
                              if ((nextDelimterLocation < theEndPos) &&
                                  (nextDelimterLocation != 0) &&
                                  (nextDelimterLocation != -1))  //found a delimiter
                              {
                                theCustomerDateOfBirth = aReadBuffer.stringAt(aDelimterLocation, ((nextDelimterLocation) - aDelimterLocation));
                                validCardReceived(aValidFlag);
                                aDelimterLocation = nextDelimterLocation + 1;
                              }
                            }
                          } while ((showNextAccountFlag) &&
                                   (numberOfAccounts > 0));

                          aReadBuffer.release();
                          //EP1005357

                        }
                        else
                        {
                          aTempLocation = -1;
                          aTempBuffer.release();
                          aTempBuffer.create(1, (Byte) 0);
                          aTempBuffer.putByteAt(0, theFieldSeperator);
                          //frequent buyer card number
                          //ASHGR0007
                          if (expandedHeaderRecord)
                            aTempLocation = aReadBuffer.locate(aTempBuffer, 1, 14);
                          else
                            aTempLocation = aReadBuffer.locate(aTempBuffer, 1, 10);
                          //ESHGR0007
                          if ((aTempLocation < theEndPos) &&
                              (aTempLocation != 0) &&
                              (aTempLocation != -1))
                          {
                            //ASHGR0007
                            if (expandedHeaderRecord)
                              theCustomerCardNumber = aReadBuffer.stringAt(14, ((aTempLocation) - 14));
                            else
                              theCustomerCardNumber = aReadBuffer.stringAt(10, ((aTempLocation) - 10));
                            //ESHGR0007

                            //AP1005357
                            //validCardReceived("0");
                            validCardReceived(aValidFlag);
                            //EP1005357
                          }
                        }
                      }
                      //ASHGR0007
                      else
                      {
                        if (aMidType == "M")
                        {
                          //ASHGR0007
                          ISBuffer aDisplayFlag;
                          if (expandedHeaderRecord)
                            aDisplayFlag = aReadBuffer.stringAt(12, 1);
                          else
                            aDisplayFlag = aReadBuffer.stringAt(8, 1);
                          //ESHGR0007
                          aTempLocation = -1;
                          aTempBuffer.release();
                          aTempBuffer.create(1, (Byte) 0);
                          aTempBuffer.putByteAt(0, theFieldSeperator);
                          //ASHGR0007
                          if (expandedHeaderRecord)
                            aTempLocation = aReadBuffer.locate(aTempBuffer, 1, 14);
                          else
                            aTempLocation = aReadBuffer.locate(aTempBuffer, 1, 10);
                          //ESHGR0007
                          if ((aTempLocation < theEndPos) && (aTempLocation != 0) && (aTempLocation != -1))
                          {
                            ISBuffer ScrollingJunk = aReadBuffer.stringAt(1, ((aTempLocation) - 1));
                            aTempLocation += 1;
                            aNewTempLocation = aReadBuffer.locate(aTempBuffer, 1, aTempLocation);
                            if ((aTempLocation < theEndPos) && (aTempLocation != 0) && (aTempLocation != -1))
                            {
                              ISBuffer theDuration = aReadBuffer.stringAt(aTempLocation, (aNewTempLocation - aTempLocation));
                              aTempLocation = aNewTempLocation + 1;
                              aNewTempLocation = aReadBuffer.locate(aTempBuffer, 1, aTempLocation);
                              if ((aTempLocation < theEndPos) && (aTempLocation != 0) && (aTempLocation != -1))
                              {
                                ISBuffer theNumberOfLines = aReadBuffer.stringAt(aTempLocation, (aNewTempLocation - aTempLocation));
                                aTempLocation = aNewTempLocation + 1;
                                aNewTempLocation = aReadBuffer.locate(aTempBuffer, 1, aTempLocation);
                                if ((aTempLocation < theEndPos) && (aTempLocation != 0) && (aTempLocation != -1))
                                {
                                  ISBuffer theCharsPerLine = aReadBuffer.stringAt(aTempLocation, (aNewTempLocation - aTempLocation));
                                  aTempLocation = aNewTempLocation + 1;
                                  aNewTempLocation = aReadBuffer.locate(aTempBuffer, 1, aTempLocation);
                                  if ((aTempLocation < theEndPos) && (aTempLocation != 0) && (aTempLocation != -1))
                                  {
                                    ISBuffer theMessageFromM = aReadBuffer.stringAt(aTempLocation, (aNewTempLocation - aTempLocation));
                                    ISWindow2x20 *aWindow = &ISApplication2x20::instance().activeView();
                                    aWindow->doPrompter(theMessageFromM.asString(),0,0,aDisplayFlag.asInteger(),theDuration.asInteger());
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                      //ESHGR0007
                    }
                  }
                  UInt aBufferLen = aReadBuffer.getDataLength();
                }
              }
            }
          }
          aReadBuffer.remove(0, (aTempLocation));
        } while ((aReadBuffer.getDataLength() != 0) &&
                (readAgain == TRUE));
      }
      else
      {
        //AD05295
        //if (theReadMessagePipeResult.code() != ISPipe::PipeErrorTimeOut)
        if ((theReadMessagePipeResult.code() != ISPipe::PipeErrorTimeOut) &&
            (theReadMessagePipeResult.code() != ISPipe::PipeErrorNotOpen) &&
            (theReadMessagePipeResult.code() != ISPipe::PipeErrorWait))
        //ED05295
        {
          theReadMessagePipe->close();
          theReadMessagePipe->create(theReadMessagePipeID, theReadPipeSize);
          readAgain = TRUE;
        }
        else
          readAgain = FALSE;
      }
    } while (readAgain == TRUE);
  }
  theAlreadyPromptedApplyingDiscount = FALSE;
  return 0;                                                //D08426
}

//ASHGR0007
//****************************************************************************
// put the prompt details in the right spots
//****************************************************************************
ISString ISGreenPointsApplication::ParsePointsPrompt(ISString AccountName,ISString PointBalance,ISString LoyalLevel,ISString message)
{
  ISString pointsprompt;
  ISBuffer bufmessage = message;
  ISBuffer bufacctname = AccountName;
  ISBuffer bufpntbal  = PointBalance;
  ISBuffer bufloyallvl = LoyalLevel;
  Int startpoint, endpoint;
  for (int i = 0; i<41; i++)
  {
    if (message[i] == '%')
    {
      startpoint = i;
      if (message[i+1] == 'N')
      {
        while (i < 41 && (message[i] == 'N' || message[i] == '%'))
        {
          message[i] = ' ';
          i++;
        }
        bufmessage.release();
        bufmessage = message;
        endpoint = i;
        int x = 0;
        while (x < 20 && AccountName[x] != '\x00')
        {
          x++;
        }
        bufmessage.atPut(startpoint, x, AccountName);
        message = bufmessage.asString();
      }
      else if (message[i+1] == 'P')  //added else statement CME defect 4085
      {
        while (i < 41 && (message[i] == 'P' || message[i] == '%'))
        {
          message[i] = ' ';
          i++;
        }
        bufmessage.release();
        bufmessage = message;
        endpoint = i;
        int x = 0;
        while (x < 20 && PointBalance[x] != '\x00')
        {
          x++;
        }
        bufmessage.atPut(startpoint, x, PointBalance);
        message = bufmessage.asString();
      }
      else if (message[i+1] == 'L')  //added else statement CME defect 4085
      {
        while (i < 41 && (message[i] == 'L' || message[i] == '%'))
        {
          message[i] = ' ';
          i++;
        }
        bufmessage.release();
        bufmessage = message;
        endpoint = i;
        int x = 0;
        while (x < 20 && LoyalLevel[x] != '\x00')
        {
          x++;
        }
        bufmessage.atPut(startpoint, x, LoyalLevel);
        message = bufmessage.asString();
      }
    }
  }
  pointsprompt = message;
  return pointsprompt;
}
//ESHGR0007

//****************************************************************************
//  Initialize the Write Pipe ID
//****************************************************************************
Void ISGreenPointsApplication::initializeWritePipeID(Void)
{
  logInfo("in initializeWritePipeID", FullDebug);
  theWriteMessagePipeID = "";
  TheSystem.getControllerIDForTerminal("", theWriteMessagePipeID);
  Char theMessage[200];
  sprintf(theMessage, "in initializeWritePipeID..PipeID=%s", theWriteMessagePipeID);
  logInfo(theMessage, FullDebug);
  theWriteMessagePipeID = ISPipe::buildQualifiedPipeName(theWriteMessagePipeName,
                                                         theWriteMessagePipeID);
  theMessage[200];
  sprintf(theMessage, "in initializeWritePipeID..PipeID after=%s", theWriteMessagePipeID);
  logInfo(theMessage, FullDebug);
}

//****************************************************************************
//  Initialize the Read Pipe ID
//****************************************************************************
Void ISGreenPointsApplication::initializeReadPipeID(Void)
{
  logInfo("in initializeReadPipeID", FullDebug);
  theReadMessagePipeID = "";
  theReadMessagePipeID = TheSystem.nodeID();
  theReadMessagePipeID = ISPipe::buildQualifiedPipeName(theReadMessagePipeName,
                                                        theReadMessagePipeID);
}

//****************************************************************************
//  Respond to Errors that Occur
//****************************************************************************
Void ISGreenPointsApplication::respondToErrorOccurred(ULong *aResponse)
{
  logInfo("in respondToErrorOccurred", FullDebug);
  ULong *theResponse = aResponse;
}

//****************************************************************************
//  Price Verify
//****************************************************************************
Void ISGreenPointsApplication::priceVerify(ISItem * anItem)
{
  logInfo("in priceVerify", FullDebug);
  if (theAllowTerminalPriceVerifyMessageFlag)
  {
    ISString aModeFlag;
    ISString aUPC2Price;
    Int aRegisterId = 0;
    ISString anItemString = "";
    anItemString.append("00000000000000");
    anItemString.append(anItem->lookupKey());
    anItemString.append("?");
    anItemString.remove(0, (anItemString.length() - 14));

    // Item Mode Flag
    if (anItem->isQuantityRequired())
      aModeFlag = "01";
    else
    {
      if (anItem->isWeightRequired())
        aModeFlag = "02";
      else
      {
        if (anItem->lookupKey() = "")
          aModeFlag = "00";
      }
    }

    // UPC
    aUPC2Price = "";

    // pre-package item price
    if (anItem->isUPC2())
    {
      aUPC2Price = TheLocale.asString(anItem->pricingMethod().unitPrice());
    }

    ISGreenPointsPriceVerifyMessage* aMessage =
      ISGreenPointsMessageFactory::instance().makePriceVerifyMessage();
    //ASHGR0007
    if (expandedHeaderRecord)
    {
      aMessage->setExpandedHeader(expandedHeaderRecord);
      aMessage->setRetailerId(retailerID.asInteger());
      aMessage->setStoreId(storeID.asInteger());
    }
    //ESHGR0007
    TheLocale.stringToNum(ISApplication2x20::instance().terminal().getId(), aRegisterId);

    aMessage->setRegisterId(aRegisterId);
    aMessage->setTransactionNumber(0);

    aMessage->setPriceVerifyItemModeFlag(aModeFlag);
    aMessage->setPriceVerifyItemCode(anItemString);
    aMessage->setPriceVerifyItemDepartment(TheLocale.asString(anItem->departmentNumber()));
    aMessage->setPriceVerifyItemDescription(anItem->description());
    aMessage->setPriceVerifyDealQuantityWeight(TheLocale.asString(anItem->pricingMethod().baseQuantity()));
    aMessage->setPriceVerifyDealQuantityPrice(TheLocale.asString(anItem->pricingMethod().unitPrice()));
    aMessage->setPriceVerifyPrePackagePrice(aUPC2Price);
    writeMessage(aMessage);
    delete aMessage;
  }
}

//****************************************************************************
//  Register for Events
//****************************************************************************
Void ISGreenPointsApplication::registerEvents()
{
  logInfo("in registerEvents", FullDebug);
  subscribeToOperatorSignedOn(this);
  subscribeToOperatorSignedOff(this);
  subscribeToStartTransaction(this);
  subscribeToRecallTransaction(this);
  subscribeToEndTransaction(this);
  subscribeToRespondToErrorOccurred(this);
  subscribeToTransactionTotal(this);
  subscribeToFoodstampTotal(this);
  subscribeToVoidTransaction(this);
  subscribeToAddItemEntry(this, ISEventSubscriber::HighestPriority);
  subscribeToAddTenderEntry(this);
  //added to get manager override flag
  subscribeToOverrideEntered(this);
  subscribeToExceptionLogged(this);
  subscribeToIdle(this);
  subscribeToPriceVerify(this);
}

//****************************************************************************
//SNRV0002
//  Register for Events
//****************************************************************************
Void ISGreenPointsApplication::cancelEvents()
{
  logInfo("in cancelEvents", FullDebug);
  cancelSubscriptionToOperatorSignedOn(this);
  cancelSubscriptionToOperatorSignedOff(this);
  cancelSubscriptionToStartTransaction(this);
  cancelSubscriptionToRecallTransaction(this);
  cancelSubscriptionToEndTransaction(this);
  cancelSubscriptionToRespondToErrorOccurred(this);
  cancelSubscriptionToTransactionTotal(this);
  cancelSubscriptionToFoodstampTotal(this);
  cancelSubscriptionToVoidTransaction(this);
  cancelSubscriptionToAddItemEntry(this);
  cancelSubscriptionToAddTenderEntry(this);
  //added to get manager override flag
  cancelSubscriptionToOverrideEntered(this);
  cancelSubscriptionToExceptionLogged(this);
  cancelSubscriptionToIdle(this);
  cancelSubscriptionToPriceVerify(this);
}
//SNRV0002

//****************************************************************************
//  Suspend Event Subscription
//****************************************************************************
Void ISGreenPointsApplication::suspendEvents()
{
}

//****************************************************************************
//  Resume Event Subscription
//****************************************************************************
Void ISGreenPointsApplication::resumeEvents()
{
  logInfo("in resumeEvents", FullDebug);
  resumeSubscriptionToOperatorSignedOn(this);
  resumeSubscriptionToOperatorSignedOff(this);
  resumeSubscriptionToStartTransaction(this);
  resumeSubscriptionToRecallTransaction(this);
  resumeSubscriptionToEndTransaction(this);
  resumeSubscriptionToRespondToErrorOccurred(this);
  resumeSubscriptionToTransactionTotal(this);
  resumeSubscriptionToFoodstampTotal(this);
  resumeSubscriptionToVoidTransaction(this);
  resumeSubscriptionToAddItemEntry(this);
  resumeSubscriptionToAddTenderEntry(this);
  resumeSubscriptionToOverrideEntered(this);
  resumeSubscriptionToExceptionLogged(this);
  resumeSubscriptionToIdle(this);
  resumeSubscriptionToPriceVerify(this);
}

//****************************************************************************
//  Set the status of the option
//****************************************************************************
Void ISGreenPointsApplication::setOptionStatus(
                                              ISGreenPointsApplication::ISGreenPointsApplicationStatus aStatus)
{

  logInfo("in setOptionStatus", FullDebug);

  Char theMessage[200];
  sprintf(theMessage, "in setOptionStatus.. theOptionStatus = %d", theOptionStatus);
  logInfo(theMessage, FullDebug);
  theMessage[200];
  sprintf(theMessage, "in setOptionStatus.. Setting option status to %d", aStatus);
  logInfo(theMessage, FullDebug);
  switch (theOptionStatus)
  {
    case OFF:
      if (aStatus == ON)
      {
        registerEvents();
        theOptionStatus = ON;
      }
      if (aStatus == SUSPENDED)
      {
        theOptionStatus = SUSPENDED;
      }
      break;
    case ON:
      if (aStatus == SUSPENDED)
      {
        suspendEvents();
        theOptionStatus = SUSPENDED;
      }
      if (aStatus == ON)
      {
        registerEvents();
        theOptionStatus = ON;
      }
      if (aStatus == OFF)
        theOptionStatus = OFF;
      break;
    case SUSPENDED:
      if (aStatus == ON)
      {
        registerEvents();
        theOptionStatus = ON;
      }
      if (aStatus == OFF)
        theOptionStatus = OFF;
      break;
  }
  if (theOptionStatus == ON)
  {
    if (theWriteMessagePipe)
    {
      theWriteMessagePipe->close();
      initializeWritePipeID();
      theWriteMessagePipe->open(theWriteMessagePipeID);
    }
    else
    {
      initializeWritePipeID();
      theWriteMessagePipe = new ISWritePipe(theWriteMessagePipeID);
      ISASSERT(theWriteMessagePipe != 0);
    }
    if (!theWriteMessagePipe->isOpen())
    {
      suspendEvents();
      theOptionStatus = SUSPENDED;
    }
    if (theReadMessagePipe)
    {
      theReadMessagePipe->close();
      initializeReadPipeID();
      theReadMessagePipe->create(theReadMessagePipeID, theReadPipeSize);
      Boolean openReadPipe = theReadMessagePipe->isOpen();
    }
    else
    {
      initializeReadPipeID();
      theReadMessagePipe = new ISReadPipe(theReadMessagePipeID, theReadPipeSize);
      ISASSERT(theReadMessagePipe != 0);
      Char theMessage[200];
      sprintf(theMessage, "in setOptionStatus..theReadMessagePipe = %d", theReadMessagePipe);
    }
    if (!theReadMessagePipe->isOpen())
    {
      suspendEvents();
      theOptionStatus = SUSPENDED;
    }
  }
}

/****************************************************************************/
/*  Prompt For Customer Phone Number                                        */
/****************************************************************************/
Void ISGreenPointsApplication::collectData(Void)
{
  logInfo("in collectData", FullDebug);
  ISString theCapturedData = "";
  Boolean theValidInput = FALSE;
  Boolean theValidateInput = TRUE;
  if (!alreadyChecked)
  {
    while (theValidInput == FALSE)
    {
      if (theCapturedData.isNull() == TRUE)
      {
        theValidInput = FALSE;
        ISPromptDetails* thePrompt = ISGuidanceFactory::instance().makePromptDetails(thePhoneNumberMessage, "");
        thePrompt->setDataEntryRequired(TRUE);
        thePrompt->setRestoreDisplayRequired(TRUE);
        ISPromptUserEvent::raiseWith(thePrompt);
        UInt theKey = thePrompt->getKeyPressed();
        if (theKey == 0)
        {
          theCapturedData = "";
          theValidateInput = FALSE;
          if (theClearKeyAllowedFlag == 0)
          {
            ISPromptDetails* thePrompt = ISGuidanceFactory::instance().makePromptDetails(theDisabledClearKeyMessage, "");
            thePrompt->setClearKeyRequired(TRUE);
            thePrompt->setErrorToneRequired(TRUE);
            thePrompt->setRestoreDisplayRequired(TRUE);
            ISPromptUserEvent::raiseWith(thePrompt);
            delete thePrompt;
          }
          else
            theValidInput = TRUE;
        }
        else if (theKey == ISKeyboardDefinition::Enter)
        {
          theCapturedData= thePrompt->getDataEntered();
          theCapturedPhoneNumber = theCapturedData;
          theValidateInput = TRUE;
        }
        delete thePrompt;
      }
      if (theValidateInput == TRUE)
      {
        if (theCapturedData.length() == 10)
        {
          theValidInput = TRUE;
          theGreenPointsCardNum = theCapturedData;
        }
        else
        {
          ISPromptDetails* thePrompt = ISGuidanceFactory::instance().makePromptDetails(theInvalidDataMessage, "");
          thePrompt->setClearKeyRequired(TRUE);
          thePrompt->setErrorToneRequired(TRUE);
          thePrompt->setRestoreDisplayRequired(TRUE);
          ISPromptUserEvent::raiseWith(thePrompt);
          delete thePrompt;
          theCapturedData = "";
        }
      }
    }
    alreadyChecked = 1;
    clearScreen();
  }
}

//****************************************************************************
// Prompt user with a message
//****************************************************************************
void ISGreenPointsApplication::displayMessage(ISString aMessageString) const
{
  logInfo("in displayMessage", FullDebug);
  ISPromptDetails* pd =
    ISGuidanceFactory::instance().makePromptDetails(aMessageString);
  if (pd != 0)
  {
    pd->setRestoreDisplayRequired(TRUE);
    ISPromptUserEvent::raiseWith(pd);
    delete pd;
  }
}

/****************************************************************************/
/*  Clear the screen after the prompt.                                      */
/****************************************************************************/
void ISGreenPointsApplication::clearScreen(Void)
{
  logInfo("in clearScreen", FullDebug);
  ISPromptDetails* clearPrompt = ISGuidanceFactory::instance().
                                 makePromptDetails();
  if (clearPrompt != 0)
  {
    clearPrompt->setRestoreDisplayRequired(TRUE);
    clearPrompt->setClearKeyRequired(FALSE);
    ISPromptUserEvent::raiseWith(clearPrompt);
    delete clearPrompt;
  }
}

/****************************************************************************/
/*  Center lines for displaying                                             */
/****************************************************************************/
Void ISGreenPointsApplication::centerMessage(ISString & aString)
{
  logInfo("in centerMessage", FullDebug);
  ISString aTempString = "";
  UInt aLength = 0;
  aTempString = aString;
  aTempString.remove(20);
  for (int i = 19; i >=0; i--)
  {
    if (aTempString[i] == 0x20)
      aTempString.remove(i);
    else
      break;
  }

  aLength = ((20 - aTempString.length()) / 2);
  ISString aSpaceString(0x20, aLength);
  aTempString.prepend(aSpaceString);
  aString.remove(0, 20);
  aLength = (20 - aTempString.length());
  ISString SpaceString2(0x20, aLength);
  aTempString.append(SpaceString2);
  aString.prepend(aTempString);
  aTempString = aString;
  aTempString.remove(0, 20);
  for (i = aTempString.length(); i >=0; i--)
  {
    if (aTempString[i] == 0x20)
      aTempString.remove(i);
    else
      break;
  }

  aLength = ((20 - aTempString.length()) / 2);
  ISString aSpaceString3(0x20, aLength);
  aTempString.prepend(aSpaceString3);
  aString.remove(20);
  aLength = (20 - aTempString.length());
  ISString aSpaceString4(0x20, aLength);
  aTempString.append(aSpaceString4);
  aString.append(aTempString);
  aLength = aTempString.length();
}

/****************************************************************************/
/*  Center Receipt Message                                                  */
/****************************************************************************/
Void ISGreenPointsApplication::centerReceiptMessage(ISString & aString)
{
  logInfo("in centerReceiptMessage", FullDebug);
  Char theMessage[200];
  UInt aLength = ((38 - aString.length()) / 2);
  ISString aSpaceString(0x20, aLength);
  aString.prepend(aSpaceString);
}

/****************************************************************************/
/*  Read the INI file for settings.                                         */
/****************************************************************************/
Boolean ISGreenPointsApplication::readINISettings(Void)
{
  Char theMessage[200];
  UInt     theValue;
  ISString theEntry;
  ISResult theResult;
  ISINIFile theGreenPointsINIFile;
  if (theGreenPointsINIFile.openOn("<SAMGNPTI>"))
  {
    theEntry = theGreenPointsINIFile.get("Debug Level", "The.Green.Points.Trace.Level", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      TheLocale.stringToNum(theEntry, theValue);
      theDebugLevel = theValue;
      sprintf(theMessage, "in readINISettings..valid The.Debug.Level = %d", theDebugLevel);
      logInfo(theMessage, FullDebug);
    }

    theEntry = theGreenPointsINIFile.get("S&H Enhancement", "Enhancement.Enabled", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      TheLocale.stringToNum(theEntry, theValue);
      sprintf(theMessage, "in readINISettings.. Enhancement.Enabled = %d", theValue);
      logInfo(theMessage, FullDebug);
      if (theValue == 1)
        setOptionEnabled(TRUE);
      else
      {
        setOptionEnabled(FALSE);
        return FALSE;
      }
    }
    SAMGreenPointsCardEnhancement::instance().setGreenPointsCardMessageSent(FALSE);
    SAMGreenPointsCardEnhancement::instance().setGreenPointsCardNumber("");
    // the GreenPoints minimum card data length
    theEntry = theGreenPointsINIFile.get("General", "Card.Min.Length", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      TheLocale.stringToNum(theEntry, theValue);
      theMinCardLength = theValue;
    }

    // the GreenPoints maximum card data length
    theEntry = theGreenPointsINIFile.get("General", "Card.Max.Length", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      TheLocale.stringToNum(theEntry, theValue);
      theMaxCardLength = theValue;
    }
    // the Department Prefix for Department Items
    theEntry = theGreenPointsINIFile.get("General", "Department.Number.Prefix", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
      theDepartmentPrefix = theEntry;
    //the initial pipe read delay
    theEntry = theGreenPointsINIFile.get("General", "The.Initial.Pipe.Read.Delay", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      TheLocale.stringToNum(theEntry, theValue);
      theInitialPipeReadDelay = theValue;
    }

    //the pipe read delay
    theEntry = theGreenPointsINIFile.get("General", "The.Pipe.Read.Delay", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      TheLocale.stringToNum(theEntry, theValue);
      thePipeReadDelay = theValue;
    }
    // the GreenPoints Card Prompt
    theAllowCardPromptFlag = 0;
    theEntry = theGreenPointsINIFile.get("Card Messages", "Allow.Prompt.For.Card", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      if (theEntry == "YES")
        theAllowCardPromptFlag = 1;
      else
      {
        theMessage[200];
        sprintf(theMessage, "in readINISettings..valid card prompt value: %s", theEntry);
        logInfo(theMessage, FullDebug);
      }
    }
    theEntry = theGreenPointsINIFile.get("Card Messages", "Message.For.Customer.Card", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      centerMessage(theEntry);
      theGreenPointsCardMessage = theEntry;
    }
    //welcome GreenPoints Customer Prompt
    theEntry = theGreenPointsINIFile.get("Welcome Message", "Allow.Prompt.For.Card", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      if (theEntry == "YES")
        theAllowWelcomePromptFlag = 1;
    }

    theEntry = theGreenPointsINIFile.get("Welcome Message", "Message.For.Welcome.Customer", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      centerMessage(theEntry);
      theGreenPointsWelcomeMessage = theEntry;
    }

    //receipt welcome message
    theEntry = theGreenPointsINIFile.get("Receipt Welcome Message", "Allow.Prompt", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      if (theEntry == "YES")
        theAllowReceiptWelcomePromptFlag = 1;
    }

    theEntry = theGreenPointsINIFile.get("Receipt Welcome Message", "Message.For.Welcome.Customer", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      centerReceiptMessage(theEntry);
      theGreenPointsReceiptWelcomeMessage = theEntry;
    }

    //receipt card number message
    theEntry = theGreenPointsINIFile.get("Receipt Card Number Message", "Allow.Prompt", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      if (theEntry == "YES")
        theAllowCardNumberWelcomePromptFlag = 1;
    }

    theEntry = theGreenPointsINIFile.get("Receipt Card Number Message", "Message.For.Card.Number.Customer", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
      theGreenPointsReceiptCardNumberMessage = theEntry;

    theEntry = theGreenPointsINIFile.get("Customer Account Name Message", "Message.For.Valid.Account.Name", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
      theGreenPointsValidAccountNameMessage = theEntry;

    //GreenPoints Applying Discount Prompt
    theEntry = theGreenPointsINIFile.get("Discount Message", "Allow.Prompt.For.Card", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      if (theEntry == "YES")
        theAllowApplyingDiscountPromptFlag = 1;
    }

    theEntry = theGreenPointsINIFile.get("Discount Message", "Message.For.Applying.Discount", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      centerMessage(theEntry);
      theApplyingDiscountPromptMessage = theEntry;
    }
    theEntry = theGreenPointsINIFile.get("Invalid Card Message", "Allow.Prompt", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      if (theEntry == "YES")
        theAllowInvalidCardNumberPromptFlag = 1;
    }
    theEntry = theGreenPointsINIFile.get("Invalid Card Message", "Message.For.Invalid.Card.Number", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      centerMessage(theEntry);
      theGreenPointsInvalidCardMessage = theEntry;
    }
    theEntry = theGreenPointsINIFile.get("Price Verify", "Allow.Price.Verify.Message", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      if (theEntry == "YES")
        theAllowTerminalPriceVerifyMessageFlag = 1;
    }
    theEntry = theGreenPointsINIFile.get("Phone Messages", "Message.If.Card.Not.Found.From.Phone.Number", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      theNoCardFromPhoneNumberMessage = theEntry;
      theValue = 20 - theNoCardFromPhoneNumberMessage.length();;
      theNoCardFromPhoneNumberMessage.append(" ", theValue);
      theValue = theNoCardFromPhoneNumberMessage.length();
    }
    //AP1005357
    theEntry = theGreenPointsINIFile.get("Customer Card Already Entered", "Message.For.Customer.Card.Already.Entered", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      centerMessage(theEntry);
      theCardAlreadyAcceptedMessage = theEntry;
    }
    //EP1005357
    theEntry = theGreenPointsINIFile.get("Phone Messages", "Message.If.Network.Down.For.Phone.Number", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      centerMessage(theEntry);
      theNetworkDownForPhoneNumberMessage = theEntry;
    }
    theEntry = theGreenPointsINIFile.get("Phone Number", "The.Phone.Number.Read.Delay", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      TheLocale.stringToNum(theEntry, theValue);
      thePhoneNumberReadDelay = theValue;
    }
    //tender type/variety
    theEntry = theGreenPointsINIFile.get("Tender Types", "Green.Points.Total.Tenders", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      TheLocale.stringToNum(theEntry, theValue);
      theTotalAmountOfTenders = theValue;
    }
    ISString aTempDescriptor = "";
    for (int i = 1; i <= theTotalAmountOfTenders; i++)
    {
      aTempDescriptor = "Tender.Type.Descriptor.";
      aTempDescriptor.append(TheLocale.asString(i));
      theEntry = theGreenPointsINIFile.get("Tender Types", aTempDescriptor, "");
      if (theEntry.compareTo("") == 0)
      {
        theGreenPointsINIFile.closeINI();
        return FALSE;
      }
      else
        theGreenPointsTenderDescriptors[i] = theEntry;

      aTempDescriptor = "Tender.Type.Variety.For.Tender.";
      aTempDescriptor.append(TheLocale.asString(i));
      theEntry = theGreenPointsINIFile.get("Tender Types", aTempDescriptor, "");
      if (theEntry.compareTo("") == 0)
      {
        theGreenPointsINIFile.closeINI();
        return FALSE;
      }
      else
        theTenderTypeVarieties[i] = theEntry;
    }

    //write message pipe
    theEntry = theGreenPointsINIFile.get("Write Message Pipe", "The.Write.Message.Pipe.Name", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
      theWriteMessagePipeName = theEntry;

    //read message pipe
    theEntry = theGreenPointsINIFile.get("Read Message Pipe", "The.Read.Message.Pipe.Name", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
      theReadMessagePipeName = theEntry;

    theEntry = theGreenPointsINIFile.get("Read Message Pipe", "The.Read.Message.Pipe.At.Total.Delay", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      TheLocale.stringToNum(theEntry, theValue);
      theTotalReadDelay = theValue;
    }
    theEntry = theGreenPointsINIFile.get("Read Message Pipe", "The.Read.Message.Pipe.At.Item.Entry.Delay", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      TheLocale.stringToNum(theEntry, theValue);
      theItemReadDelay = theValue;
    }

    //ASHGR0007
    theEntry = theGreenPointsINIFile.get("Customer Display Information", "Display.Customer.Information", "");
    if (theEntry.compareTo("") != 0)
    {
      if (theEntry.compareTo("YES") == 0)
      {
        displayCustomerInfo = TRUE;
        theEntry = theGreenPointsINIFile.get("Customer Display Information", "Signon.Hotkey.Number", "");
        if (theEntry.compareTo("") != 0)
        {
          TheLocale.stringToNum(theEntry, theValue);
          hotKeyNumber = theValue;
        }
        theEntry = theGreenPointsINIFile.get("Customer Display Information", "Message.For.Account.Information", "");
        if (theEntry.compareTo("") != 0)
          messageForAccountInfo = theEntry;
        theEntry = theGreenPointsINIFile.get("Customer Display Information", "Message.If.Card.Not.Scanned.Before.Hotkey", "");
        if (theEntry.compareTo("") != 0)
          messageForIfCardNotScannedBeforeHotKey = theEntry;
      }
    }

    expandedHeaderRecord = FALSE;
    theEntry = theGreenPointsINIFile.get("General", "Retailer.ID.In.Header", "");
    if (theEntry.compareTo("") != 0)
    {
      expandedHeaderRecord = TRUE;
      retailerID = theEntry;
    }
    theEntry = theGreenPointsINIFile.get("General", "Store.ID.In.Header", "");
    if (theEntry.compareTo("") != 0)
    {
      expandedHeaderRecord = TRUE;
      storeID = theEntry;
    }
    //ESHGR0007

    //the Read Pipe Storage Size
    theEntry = theGreenPointsINIFile.get("Read Message Pipe", "The.Read.Pipe.Storage.Size", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      TheLocale.stringToNum(theEntry, theValue);
      theReadPipeSize = theValue;
    }

    // Close INI File
    theGreenPointsINIFile.closeINI();
    if (!isOptionEnabled)
      return FALSE;
    else
      return TRUE;
  }
  else
    return FALSE;
}

//ASHGR0007
/****************************************************************************/
/*  Get Input from Devices                                                  */
/****************************************************************************/
Void ISGreenPointsApplication::getInput(ISInputSequence * result)
{
  //ASHGR0012
#ifndef __SHACE_V6__
  if (result)
  {
    //AD02611
    ////AD06569
    ////if (result->motorKey() == ISKeyboardDefinition::Enter)
    //if ((result->motorKey() == ISKeyboardDefinition::Enter) && (!result->hasKey(ISKeyboardDefinition::Override)))    //D06569
    ////ED06569
    //{
    //  lastIOData = result->dataForKey(result->motorKey());
    //}
    //ED02611
#endif
    //ESHGR0012
    ISWindow2x20 *aWindow = &ISApplication2x20::instance().activeView();
    if ((result->motorKey() == ISKeyboardDefinition::SignOff) &&
        (result->dataForKey(result->motorKey()) == TheLocale.asString(hotKeyNumber)) &&
        !(ISApplication2x20::terminal().isInSpecialSignOff()))
    {
      ISPromptDetails* thePrompt;
      if (cardScanned)
      {
        if (displayCustomerInfo)
        {
          result->motorKey(ISKeyboardDefinition::Clear);
          ISString pointPrompt = ParsePointsPrompt(theCustomerAccountName,theCustomerPointBalance,theCustomerLoyalLevel,messageForAccountInfo);
          aWindow->show(pointPrompt);
        }
      }
      else
      {
        if (!cardScanned)
        {
          result->motorKey(ISKeyboardDefinition::Clear);
          aWindow->doPrompter(messageForIfCardNotScannedBeforeHotKey,0,0);
        }
      }
    }
#ifndef __SHACE_V6__
  } //SHGR0012
#endif
}
//ESHGR0007

//AD07952
UInt ISGreenPointsApplication::updateTransactionStateMessage(SHAddItemCommand* cmnd, UInt message)
{
  if (wasLastItemAnSHItem)
  {
    message = 0;
  }
  return message;
}
//ED07952

//AD26775
Boolean ISGreenPointsApplication::customerInTrans(ISString aCustomerNumber)
{
  Boolean aResult = FALSE;

  ISTransaction* aTransaction = ISApplication2x20::terminal().activeTransaction();
  ISSalesTransaction* aSalesTransaction=NULL;

  if (aTransaction &&
      ISRTTI_OBJECT_TYPE(*aTransaction).isKindOf(ISRTTI_CLASS_TYPE(ISSalesTransaction)))
  {
    aSalesTransaction=(ISSalesTransaction*)aTransaction;
    ISCustomer* aCustomer = aSalesTransaction->getCustomer();
    if (aCustomer)
    {
      ISString aCurrentCustomer = aCustomer->id();

      aCurrentCustomer = aCurrentCustomer.strip(ISString::leading, '0');
      aCustomerNumber = aCustomerNumber.strip(ISString::leading, '0');

      if (aCustomerNumber == aCurrentCustomer)
      {
        aResult = TRUE;
      }
    }
  }

  return aResult;
}
//ED26775
tenfold
