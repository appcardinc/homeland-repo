
/****************************************************************************/
/*                                                                          */
/*  Filename   : GNPTMAIN.HPP                                               */
/*                                                                          */
/*  Description: Migrate S&H GreenPoints to ACE                             */
/*                                                                          */
/*  Author:      JAI                                                        */
/*                                                                          */
/*  Date:        8-27-02                                                    */
/****************************************************************************/
/****************************************************************************/
/*                                                                          */
/*  CHANGES: Start                                                          */
/*   SNRV0002 29-Aug-07 'BJV' OPTION protect S&H Greenpoints code           */
/*   SHGR0007 11-Feb-09 'CME' Customer Display Info; M message added to     */
/*                            display a 2x20 message at anypoint in the     */
/*                            transaction.; Using any item code with the C  */
/*                            message.                                      */
/*   SHGR0012 22-Apr-10 'CME' Adding GS1 Barcode support                    */
/*   D07952   29-Sep-10 'CME' After a MFR or Store CPN from SH is played    */
/*                            then if an item is scanned the message 173    */
/*                            is displayed.  Made them look more like       */
/*                            electronic coupons.                           */
/*   D08346   11-Jan-11 'CME' When an alt ID was rung up it only notified   */
/*                            sh code that a card was rung.  This fix allows*/
/*                            EM entry of the customer card also.           */
/*   P1005357 11-Apr-05 'JM'  Support multiple accounts from phone number   */
/*   D02611   09-Feb-12 'RBS' Price required GS1 coupons causing terminal   */
/*                            to dump intermittently                        */
/*   D26775   31-Mar-20 'TDW' Duplicate customer number on receipt and in   */
/*                            TLOG after NTR code added                     */
/*  CHANGES: End                                                            */
/*                                                                          */
/****************************************************************************/

#ifndef GNPTMAIN_HPP
#define GNPTMAIN_HPP

/****************************************************************************/
/*  ACE Include Files                                                       */
/****************************************************************************/
#include <samincln.h>                                      //D07952
#include ISTime_i
#include ISBuffer_i
#include ISPipe_i
#include Magnitudes_i
#include ISString_i
#include Singleton_i
#include ISOptionsSubscriber_i
#include ISInputSequence_i                                 //SHGR0007
#include SHAddItemCommand_i                                //D07952

/****************************************************************************/
/*  Forward Declarations                                                    */
/****************************************************************************/
class ISOptions;
class ISItem;
class ISItemEntry;
class ISTenderEntry;
class ISPrintCollection;
class ISPromptDetails;
class ISTransaction;
class ISTransactionEntry;
class ISWorkstation;
class ISGreenPointsMessage;
class ISOverrideList;
class ISCommandCallbackEvent;
class ISApplicationException;

/****************************************************************************/
/*  SAMGardenRidgeTaskApplication Class Declaration                         */
/****************************************************************************/
class ISGreenPointsApplication : public ISOptionsSubscriber
{
  /**************************************************************************/
  /*  Singleton Declaration                                                 */
  /**************************************************************************/
  ISSINGLETON_DECLARE(ISGreenPointsApplication)

  public:
    /************************************************************************/
    /*  Event Subscription                                                  */
    /************************************************************************/
    virtual Void optionsLoaded(ISOptions*);
    virtual Void operatorSignedOn(ISWorkstation*);
    virtual Void operatorSignedOff(ISWorkstation*);
    virtual Void startTransaction(ISTransaction*);
    virtual Void recallTransaction(ISTransaction*);
    virtual Void endTransaction(ISTransaction*);
    virtual Void transactionTotal(ISTransaction*);
    virtual Void foodstampTotal(ISTransaction*);
    virtual Void voidTransaction(ISTransaction*);
    virtual Void addItemEntry(ISItemEntry*);
    virtual Void addTenderEntry(ISTenderEntry*);
    virtual Void overrideEntered(ISOverrideList*);
    virtual Void respondToErrorOccurred(ULong *aResponse);
    virtual Void exceptionLogged(ISApplicationException * anException);
    virtual Void idle(ULong*);
    virtual Boolean readINISettings(Void);
    virtual Void priceVerify(ISItem * anItem);
    virtual Boolean getAddingTotalDiscount(Void);
    virtual Void setAddingTotalDiscount(Boolean anAddingTotalFlag);
    virtual Boolean getAddedTotalDiscount(Void);
    virtual Void setAddedTotalDiscount(Boolean anAddedTotalFlag);

    //************************************************************************
    //  Register, Suspend and Resume event member functions
    //************************************************************************
    virtual Void registerEvents();
    virtual Void cancelEvents();
    virtual Void suspendEvents();
    virtual Void resumeEvents();

    virtual Void writeMessage(ISGreenPointsMessage* aMessage);
    //AD08426
    //virtual Void readMessage(UInt aReadDelay);
    virtual int readMessage(UInt aReadDelay);
    //ED08426
    virtual Void logInfo(Char * theMessage, UInt aDebugLevel) const;
    virtual Boolean seperateDiscount(Void);
    virtual Boolean seperateAccountInfo(Void);
    virtual Boolean getAddingItem(Void);
    virtual ISString getItemDescriptor(Void);
    virtual Boolean getBOTMessageSent(Void);
    virtual Void setBOTMessageSent(Boolean anAlreadySentBOTFlag);
    virtual Void centerMessage(ISString& aString);
    virtual Void centerReceiptMessage(ISString& aString);
    virtual Boolean isValidCheckDigit (ISString& aBarCode);
    virtual Void validCardReceived(ISString aLookUpFlag);

    //ASHGR0007
    virtual Void getInput(ISInputSequence * result);
    virtual Boolean isOptionEnabled(void);
    virtual Boolean isExpandedHeaderRecord();
    virtual ISBuffer getRetailerID();
    virtual ISBuffer getStoreID();
    virtual UInt getDebugLevel();
    //ESHGR0007

    virtual Boolean isTheItemEntered(void);                //P1005357
    virtual void setTheItemEntered(Boolean b);             //P1005357
    virtual UInt updateTransactionStateMessage(SHAddItemCommand* cmnd, UInt message); //D07952

    virtual Boolean customerInTrans(ISString aCustomerNumber); //D26775


    //************************************************************************
    //  enum used to determine the status of the option
    //************************************************************************
    typedef enum
    {
      OFF,
      ON,
      SUSPENDED
    } ISGreenPointsApplicationStatus;

    UInt aMessageSent;

    //************************************************************************
    //  enum used to set the debug level of the option
    //************************************************************************
    typedef enum
    {
      NoDebug = 0,
      EnterExitMethodDebug = 1,
      FullDebug = 2
    } ISGreenPointsDebugLevel;

    Boolean altIDHasBeenDone;                              //D08346

  protected:
    //************************************************************************
    //  State data set/accessor methods
    //************************************************************************
    virtual ISGreenPointsApplicationStatus optionStatus(Void) const;
    virtual Void setOptionStatus(ISGreenPointsApplicationStatus aStatus);
    virtual Void setAddingItem(Boolean anEntryFlag);
    virtual Void setItemDescriptor(ISString& anItemDescriptor);
    virtual Void collectData(Void);
    virtual Void clearScreen(Void);
    virtual Void addGreenPointsItem(ISString& aDiscountType, ISString& anItemCode, ISString& anItemDesc, UInt aQuantity, ISTransaction& transaction);
    virtual Void addItemsToStorage(ISString aDiscountType, ISString anItemCode, ISString anItemDesc, ISString aQuantity);
    virtual Void retrieveItemsFromStorage(Void);
    virtual Boolean addItemError(const ISCommandCallbackEvent& anEvent);
    virtual Void initializePipe(Void);
    virtual Void displayMessage(ISString aMessageString) const;
    virtual Void doTotal(Void);
    virtual void setOptionEnabled(Boolean b);
    virtual ISString ParsePointsPrompt(ISString AccountName,ISString PointBalance,ISString LoyalLevel,ISString message);  //SHGR0007

    //************************************************************************
    //  Pipe ID initialization
    //************************************************************************
    virtual Void initializeWritePipeID(Void);
    virtual Void initializeReadPipeID(Void);

  private:
    //************************************************************************
    //  State data
    //************************************************************************
    ISGreenPointsApplicationStatus theOptionStatus;

    ISWritePipe* theWriteMessagePipe;
    ISString     theWriteMessagePipeID;
    ISString     theWriteMessagePipeName;
    ISResult     theWriteMessagePipeResult;
    ISReadPipe*  theReadMessagePipe;
    ISString     theReadMessagePipeID;
    ISString     theReadMessagePipeName;
    ISResult     theReadMessagePipeResult;
    Boolean      theManagerOverrideFlag;
    UInt         theSentSignOnMessage;
    UInt         theInTrainingMode;
    ISAmount     theRunningTranTaxTotal;
    ISAmount     theRunningTranAmtTotal;
    UInt         promptForCard;
    UInt         alreadyPrompted;
    UInt         theMinCardLength;
    UInt         theMaxCardLength;
    ISString     thePhoneNumberMessage;
    ISString     theGreenPointsCardMessage;
    ISString     theDisabledClearKeyMessage;
    ISString     theInvalidDataMessage;
    #define      GNPT_MAX_TENDERS 50
    ISString     theGreenPointsTenderDescriptors[GNPT_MAX_TENDERS];
    ISString     theTenderTypeVarieties[GNPT_MAX_TENDERS];
    ISString     theGreenPointsDiscounts[GNPT_MAX_TENDERS];
    ISString     theItemDiscountDescriptor[GNPT_MAX_TENDERS];
    UInt         theTotalAmountOfTenders;
    UInt         theClearKeyAllowedFlag;
    UInt         theAllowCardPromptFlag;
    ISString     theSuspendedTransactionNumber;
    ISString     theSuspendedTotalAmount;
    ISString     theSuspendedRegisterID;
    ISString     theSuspendedTaxAmount;
    UInt32       theWriteMessagePipeKey;
    UInt32       theReadMessagePipeKey;
    UInt         alreadyLoaded;
    UInt         theTotalReadDelay;
    UInt         theItemReadDelay;
    UInt         theCardReadDelay;
    UInt         theWriteDelay;
    UInt         theDebugLevel;
    UInt         alreadySentCardInfo;
    Byte         theFieldSeperator;
    UInt         theItemDiscountCount;
    ISString     theGreenPointsAccountInfo;
    UInt         theMessageHeader;
    Boolean      theAlreadyPromptedFlag;
    ISString     theAddingItemDescriptor;
    Boolean      theAddingItemFlag;
    UInt         theAllowWelcomePromptFlag;
    ISString     theGreenPointsWelcomeMessage;
    UInt         theAllowReceiptWelcomePromptFlag;
    ISString     theGreenPointsReceiptWelcomeMessage;
    Boolean      theReceiptMessagePrinted;
    UInt         theAllowApplyingDiscountPromptFlag;
    ISString     theApplyingDiscountPromptMessage;
    Boolean      theAlreadyPromptedApplyingDiscount;
    ISTransaction *theSavedTransaction;
    UInt         theIdleCount;
    Boolean      theBOTMessageSent;
    Boolean      theFirstRead;
    UInt         theAllowCardNumberWelcomePromptFlag;
    ISString     theGreenPointsReceiptCardNumberMessage;
    Boolean      theReceipCardNumberPrinted;
    ISString     theDepartmentPrefix;
    UInt         theInitialPipeReadDelay;
    UInt         thePipeReadDelay;
    UInt         alreadyChecked;
    UInt        theReadPipeSize;
    ISString    theCustomerCardNumber;
    ISString    theCustomerAccountName;
    ISString    theCustomerPointBalance;
    ISAmount    thePreviousBalanceDue;
    ISString    theGreenPointsInvalidCardMessage;
    ISString    theCustomerPriceOfAPointInCent;
    ISString    theCustomerYearToDateSavings;
    ISString    theCustomerCardType;
    ISString    theCustomerLoyalLevel;
    UInt        theAllowInvalidCardNumberPromptFlag;
    ISString    theCustomerDateOfBirth;
    ISString    theNoCardFromPhoneNumberMessage;
    ISString    theNetworkDownForPhoneNumberMessage;
    UInt        theAllowTerminalPriceVerifyMessageFlag;
    UInt        thePhoneNumberReadDelay;
    ISString    theGreenPointsValidAccountNameMessage;
    ISString    theGreenPointsStoredItems[GNPT_MAX_TENDERS];
    UInt        theGreenPointsStoredItemsCount;
    Boolean     theAddingTotalDiscountFlag;
    Boolean     theTotalDiscountApplied;
    //temp variables for messages
    Byte      theTempSeperator;

    //signon
    ISString  theTempSignOnFlag;
    ISString  theTempSignOnString;
    ISString  theTempID;
    ISString  theTempDateTime;
    //item entry
    ISString   theGreenPointsCardNum;
    ISString   theCapturedPhoneNumber;
    Boolean theTempFlag;
    Boolean  theOptionEnabled;
    //ASHGR0007
    Boolean displayCustomerInfo;
    Int     hotKeyNumber;
    ISString messageForAccountInfo;
    ISString messageForIfCardNotScannedBeforeHotKey;
    Boolean cardScanned;
    ISBuffer retailerID;
    ISBuffer storeID;
    Boolean  expandedHeaderRecord;
    Boolean  setItemAsNonDiscountable;
    //ESHGR0007

    //ISString lastIOData;                                   //SHGR0012 //D02611

    Boolean wasLastItemAnSHItem;                           //D07952
    Boolean altIDCardNeedsEMEntry;                         //D08346
    Boolean andWeHaveACardNumberToEnter;                   //D08346
    Boolean butNotDuringOurOwnCardEntry;                   //D08346
    Boolean changedWaitTimeNowChangeitBack;                //D08346
    Boolean     showNextAccountFlag;                       //P1005357
    ISString    theCardAlreadyAcceptedMessage;             //P1005357
    Boolean     theItemEntered;                            //P1005357
};

//****************************************************************************
//  Inline Methods
//****************************************************************************
#include <gnptmain.inl>


//*********************************************************************
// Inline Functions
//*********************************************************************
//SNRV0002
inline void ISGreenPointsApplication::setOptionEnabled(Boolean b)
{
  theOptionEnabled = b;
}

inline Boolean ISGreenPointsApplication::isOptionEnabled()
{
  return theOptionEnabled;
}

//ASHGR0007
inline Boolean ISGreenPointsApplication::isExpandedHeaderRecord()
{
  return expandedHeaderRecord;
}

inline ISBuffer ISGreenPointsApplication::getRetailerID()
{
  return retailerID;
}

inline ISBuffer ISGreenPointsApplication::getStoreID()
{
  return storeID;
}

inline UInt ISGreenPointsApplication::getDebugLevel()
{
  return theDebugLevel;
}
//ESHGR0007

//AP1005357
inline void ISGreenPointsApplication::setTheItemEntered(Boolean b)
{
  theItemEntered = b;
}

inline Boolean ISGreenPointsApplication::isTheItemEntered()
{
  return theItemEntered;
}
//EP1005357

#endif
