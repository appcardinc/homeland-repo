/****************************************************************************/
/*                                                                          */
/*  Filename   : SAMSHCRD.HPP                                               */
/*                                                                          */
/*  Description:                                                            */
/*                                                                          */
/*  Author:      JAI                                                        */
/*                                                                          */
/****************************************************************************/
/****************************************************************************/
/*                                                                          */
/*  CHANGES: Start                                                          */
/*   SNRV0002 29-Aug-07 'BJV' OPTION protect S&H Greenpoints code           */
/*  CHANGES: End                                                            */
/*                                                                          */
/****************************************************************************/
#ifndef SAMSHCRD_HPP
#define SAMSHCRD_HPP

/****************************************************************************/
/*  ACE Include Files                                                       */
/****************************************************************************/
#include ISString_i
#include Singleton_i
#include ISCardData_i
#include ISOptionsSubscriber_i
#include ISEMCustomerEntry_i

/****************************************************************************/
/*  Forward Declarations                                                    */
/****************************************************************************/
class ISBuffer;
class ISOptions;
class ISItemEntry;
class ISTransaction;
class ISInputSequence;
class ISPrintCollection;
class ISWorkstation;
class ISMsrTrack2;
class ISEPSAuthTransaction;

/****************************************************************************/
/*  SAMGreenPointsCardEnhancement Class Declaration                         */
/****************************************************************************/
class SAMGreenPointsCardEnhancement : public ISOptionsSubscriber
{
  /**************************************************************************/
  /*  Singleton Declaration                                                 */
  /**************************************************************************/
  ISSINGLETON_DECLARE(SAMGreenPointsCardEnhancement)
  public:
    /************************************************************************/
    /*  greenPointsCardInput                                                */
    /************************************************************************/
    virtual void checAltGreenPointsCardInput(ISString);    //CHEC ALTID ISSUE
    virtual Boolean greenPointsCardInput(ISInputSequence&);
    virtual ISString getGreenPointsCardNumber(Void);
    virtual UInt getKeyedOrScannedFlag(Void);
    virtual Boolean getGreenPointsCardEntered(Void);
    virtual Boolean getGreenPointsCardMessageSent(Void);
    virtual Boolean getBOTMessageSent(Void);
    virtual UInt getOperationFlag(Void);
    virtual ISString getDataForEnter(Void);
    virtual Void setBOTMessageSent(Boolean anAlreadySentBOTFlag);
    virtual Boolean getInTraining(Void);
    virtual Void setSlashEnteredFlag(Boolean aFlag);
    virtual Boolean getSlashEnteredFlag(Void);
    virtual ISTransaction* getTransaction(Void);
    virtual Void logCardInfo(Char * theMessage, UInt aDebugLevel);
    virtual Boolean readINISettings(Void);
    virtual Void setGreenPointsCardMessageSent(Boolean anAlreadySentFlag);
    virtual Void setGreenPointsCardNumber(const ISString& aCardNumber);
    virtual Void setGreenPointsCardEntered(Boolean aCardNumberEntered);

    /************************************************************************/
    /*  Event Subscription                                                  */
    /************************************************************************/
    virtual Void optionsLoaded(ISOptions*);
    virtual Void startTransaction(ISTransaction*);
    virtual Void addItemEntry(ISItemEntry*);
    virtual Void endTransaction(ISTransaction*);
    virtual void addEMCustomerEntry(ISEMCustomerEntry*);   //CHEC ALTID ISSUE
    virtual Void operatorSignedOff(ISWorkstation*);
    virtual void setOptionEnabled(Boolean b);
    virtual Boolean isOptionEnabled(void);

    typedef enum
    {
      NoDebug = 0,
      EnterExitMethodDebug = 1,
      FullDebug = 2
    } ISGreenPointsDebugLevel;

  protected:

    /************************************************************************/
    /*  Support Methods                                                     */
    /************************************************************************/
    virtual Void printApplication(Void);
    virtual Void setKeyedOrScannedFlag(const UInt anEntryMode);
    virtual Void setInTraining(Boolean aTrainingFlag);
    virtual Void setTransaction(ISTransaction *aTransaction);
    virtual Void setOperationFlag(UInt aOperationFlag);
    virtual Void setDataForEnter(ISString& aKeyData);
    virtual Void clearScreen(Void);

    // helper methods
    virtual Boolean hasTrack2Data (ISCardData *aCardData);
    virtual ISMsrTrack2* getTrack2Data (ISCardData *aCardData);


    /************************************************************************/
    /*  Private Variables                                                   */
    /************************************************************************/
    Boolean     theGreenPointsCardFound;
    ISString    theGreenPointsCardNumber;
    UInt        theKeyedOrScannedFlag;
    Boolean     theGreenPointsCardNumberEntered;
    Boolean     theBeenToBOTFlag;
    Boolean     theGreenPointsCardMessageSent;
    Boolean     theBOTMessageSent;
    Boolean     theTrainingFlag;
    UInt        theNumberOfPrefixes;
    UInt        theNumberOfMSRPrefixes;
    ISString    theGreenPointsPrefixDescriptors[50];
    ISString    theGreenPointsMSRPrefixDescriptors[50];
    Boolean     theActiveFlag;
    UInt        theAllowWelcomePromptFlag;
    ISString    theGreenPointsWelcomeMessage;
    Boolean     aWelcomeMessageSent;
    UInt        thePhoneNumberKey;
    ISString    theGreenPointsReceiptWelcomeMessage;
    UInt        theAllowReceiptWelcomePromptFlag;
    ISTransaction * theTransaction;
    Boolean     theAddedItemFlag;
    UInt        theCardReadDelay;
    UInt        theOperationFlag;
    ISString    theEnterKeyData;
    UInt        alreadyChecked;
    UInt        theClearKeyAllowedFlag;
    ISString    thePhoneNumberMessage;
    ISString    theDisabledClearKeyMessage;
    ISString    theInvalidDataMessage;
    ISString    theCapturedPhoneNumber;
    Boolean     theSlashEnteredFlag;
    UInt        thePhoneNumberReadDelay;
    UInt        theWelcomeMessageDelay;
    ISString    theCustomerCardAlreadyEnteredMessage;
    UInt        theDebugLevel;
    Boolean  theOptionEnabled;
};

//*********************************************************************
// Inline Functions
//*********************************************************************
inline void SAMGreenPointsCardEnhancement::setOptionEnabled(Boolean b)
{
  theOptionEnabled = b;
}

inline Boolean SAMGreenPointsCardEnhancement::isOptionEnabled()
{
  return theOptionEnabled;
}

#endif
