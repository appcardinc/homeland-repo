/****************************************************************************/
/*                                                                          */
/*  Filename   :                                                            */
/*                                                                          */
/*  Description:                                                            */
/*                                                                          */
/*  Author:      JAI                                                        */
/*                                                                          */
/****************************************************************************/
/****************************************************************************/
/*                                                                          */
/*  CHANGES: Start                                                          */
/*   SNRV0002 29-Aug-07 'BJV' OPTION protect S&H Greenpoints code           */
/*   SHGR0007 11-Feb-09 'CME' Customer Display Info; M message added to     */
/*                            display a 2x20 message at anypoint in the     */
/*                            transaction.; Using any item code with the C  */
/*                            message.                                      */
/*   D08326   07-Dec-10 'CME' Problem where Large links did not execute     */
/*   D08346   11-Jan-11 'CME' When an alt ID was rung up it only notified   */
/*                            sh code that a card was rung.  This fix allows*/
/*                            EM entry of the customer card also.           */
/*   D08426   11-Jan-25 'CME' Tossed out messages that did not match the    */
/*                            current transaction number.  Also allowed     */
/*                            pipe reading during prompts.                  */
/*   D01125   11-Apr-22 'JM'  Issue with terminal stuck in a loop with Alt  */
/*                            ID key.                                       */
/*   D01127   11-May-09 'JM'  ALT ID at Beginning of Order                  */
/*   D08426.1 11-May-25 'RBS' Update transaction number when between        */
/*                            transactions.                                 */
/*  CHANGES: End                                                            */
/*                                                                          */
/****************************************************************************/

#include <swami.h>
#include <samincln.h>

#include SAMGreenPointsCardEnhancement_i
#include ISSystem_i
#include ISBuffer_i
#include ISLocale_i
#include ISINIFile_i
#include ISMsrCardData_i
#include ISCardDataFactory_i
#include ISInputSequence_i
#include ISGuidanceFactory_i
#include ISApplication2x20_i
#include ISPromptDetails_i
#include ISSalesTransaction_i
#include ISKeyboardDefinition_i
#include TransactionEventDefinitions_i
#include SalesTransactionEventDefinitions_i
#include TheLastThingIncluded_i
#include ISCommand_i
#include ISSalesTransactionDialog2x20_i
#include ISCommandFactory_i
#include ISAddItemCommand_i
#include ISOptions_i
#include ISItemEntry_i
#include ISOperator_i
#include ISFormattedStringList_i
#include CommonEventDefinitions_i
#include ISTenderEntry_i
#include ISWorkstation_i
#include EventDefinitions_i
#include PricingPolicy_i
#include OptionsTypes_i
#include ISNLSFactory_i
#include ISProcedureId_i
#include ISTenderEntry_i
#include ISTenderType_i
#include ISTransaction_i
#include ISOverrideList_i
#include ISOverride_i
#include ISTerminal_i
#include ISSignOnCommand_i
#include ISTenderTypeCache_i
#include ISTaxExemptEntry_i
#include ISInputState_i
#include ISEMCustomerOptionsCache_i                        //D08326
#include EventDefinitions_i                                //AGUI

#include ISGreenPointsMessageFactory_i
#include ISGreenPointsApplication_i
#include ISGreenPointsMessage_i

/****************************************************************************/
/*  Singleton Implementation                                                */
/****************************************************************************/
ISSINGLETON_IMPLEMENT_INSTANCE(SAMGreenPointsCardEnhancement);

/****************************************************************************/
/*  SAMQuickCreditEnhancement Constructor                                   */
/****************************************************************************/
SAMGreenPointsCardEnhancement::SAMGreenPointsCardEnhancement() :
  theGreenPointsCardNumber(""),
  theKeyedOrScannedFlag(0),
  theBeenToBOTFlag(FALSE),
  theGreenPointsCardFound(FALSE),
  theGreenPointsCardMessageSent(FALSE),
  theGreenPointsCardNumberEntered(FALSE),
  theBOTMessageSent(FALSE),
  theTrainingFlag(FALSE),
  theActiveFlag(FALSE),
  theAllowWelcomePromptFlag(0),
  theGreenPointsWelcomeMessage(""),
  aWelcomeMessageSent(FALSE),
  theGreenPointsReceiptWelcomeMessage(""),
  theAllowReceiptWelcomePromptFlag(0),
  theTransaction(0),
  alreadyChecked(0),
  theOperationFlag(0),
  theEnterKeyData(""),
  theClearKeyAllowedFlag(0),
  thePhoneNumberMessage(""),
  theDisabledClearKeyMessage(""),
  theInvalidDataMessage(""),
  theAddedItemFlag(FALSE),
  theSlashEnteredFlag(FALSE),
  theCustomerCardAlreadyEnteredMessage(""),
  theDebugLevel(0),
  thePhoneNumberReadDelay(0)
{
  theActiveFlag = FALSE;
}

/****************************************************************************/
/*  SAMQuickCreditEnhancement Destructor                                    */
/****************************************************************************/
SAMGreenPointsCardEnhancement::~SAMGreenPointsCardEnhancement()
{
  cancelSubscriptionToOptionsLoaded(this);
  cancelSubscriptionToStartTransaction(this);
  cancelSubscriptionToAddItemEntry(this);
  cancelSubscriptionToEndTransaction(this);
  cancelSubscriptionToOperatorSignedOff(this);
  cancelSubscriptionToEMCustomerEntry(this);

}

/****************************************************************************/
/*  Log Info                                                                */
/****************************************************************************/
Void SAMGreenPointsCardEnhancement::logCardInfo(Char * theMessage, UInt aDebugLevel)
{
  if (aDebugLevel <= theDebugLevel)
  {
    ISBuffer theLogInformation(theMessage);
    theLogInformation.append("\x0D\x0A");
    ISSequentialFile theLogFile("<GNPTLOG2>",
                                ISFile::AccessExclusiveReadWrite,
                                ISFile::PriorityLow,
                                ISFile::ActionOpenCreateOpen,
                                ISFile::DistMirroredAtClose);

    theLogFile.seek(0, ISFile::SeekFromEnd);
    theLogFile.write(theLogInformation.getDataLength(), theLogInformation);
    theLogFile.close();
  }
}

/****************************************************************************/
/*  readINISettings                                                         */
/****************************************************************************/
Boolean SAMGreenPointsCardEnhancement::readINISettings(Void)
{
  UInt     theValue;
  ISString theEntry;
  Char theMessage[200];

  ISINIFile theGreenPointsINIFile;
  if (theGreenPointsINIFile.openOn("<SAMGNPTI>"))
  {
    theEntry = theGreenPointsINIFile.get("Debug Level", "The.Green.Points.Trace.Level", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      TheLocale.stringToNum(theEntry, theValue);
      theDebugLevel = theValue;
      sprintf(theMessage, "in readINISettings..valid The.Debug.Level = %d", theDebugLevel);
      logCardInfo(theMessage, FullDebug);
    }

    theEntry = theGreenPointsINIFile.get("S&H Enhancement", "Enhancement.Enabled", "");
    if (theEntry.compareTo("") == 0)
    {
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      TheLocale.stringToNum(theEntry, theValue);
      sprintf(theMessage, "in readINISettings.. Enhancement.Enabled = %d", theValue);
      logCardInfo(theMessage, FullDebug);
      if (theValue == 1)
        setOptionEnabled(TRUE);
      else
      {
        setOptionEnabled(FALSE);
        return FALSE;
      }
    }

    // the GreenPoints card number prefixes
    theEntry = theGreenPointsINIFile.get("Card Number Prefixes", "Total.Card.Prefixes", "");
    if (theEntry.compareTo("") == 0)
    {
      logCardInfo("in readINISettings..no Total.Card.Prefixes value", FullDebug);
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      logCardInfo("in readINISettings..valid max card value", FullDebug);
      TheLocale.stringToNum(theEntry, theValue);
      theNumberOfPrefixes = theValue;
    }

    for (int i = 1; i <= theNumberOfPrefixes; i++)
    {
      ISString aTempDescriptor = "Card.Prefix.";
      aTempDescriptor.append(TheLocale.asString(i));
      theEntry = theGreenPointsINIFile.get("Card Number Prefixes", aTempDescriptor, "");
      if (theEntry.compareTo("") == 0)
      {
        theMessage[200];
        sprintf(theMessage, "in readINISettings..no Card.Prefix.%d", i);
        logCardInfo(theMessage, FullDebug);
        theGreenPointsINIFile.closeINI();
        return FALSE;
      }
      else
      {
        theMessage[200];
        sprintf(theMessage, "in readINISettings..valid Card.Prefix.%d", i);
        logCardInfo(theMessage, FullDebug);
        theGreenPointsPrefixDescriptors[i] = theEntry;
      }
    }

    //the number of MSR prefixes
    theEntry = theGreenPointsINIFile.get("MSR Card Number Prefixes", "Total.MSR.Card.Prefixes", "");
    if (theEntry.compareTo("") == 0)
    {
      logCardInfo("in readINISettings..no Total.MSR.Card.Prefixes", FullDebug);
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      logCardInfo("in readINISettings..valid Total.MSR.Card.Prefixes", FullDebug);
      TheLocale.stringToNum(theEntry, theValue);
      theNumberOfMSRPrefixes = theValue;
    }

    for (i = 1; i <= theNumberOfMSRPrefixes; i++)
    {
      ISString aTempDescriptor = "MSR.Card.Prefix.";
      aTempDescriptor.append(TheLocale.asString(i));
      theEntry = theGreenPointsINIFile.get("MSR Card Number Prefixes", aTempDescriptor, "");
      if (theEntry.compareTo("") == 0)
      {
        theMessage[200];
        sprintf(theMessage, "in readINISettings..no MSR Card.Prefix.%d", i);
        logCardInfo(theMessage, FullDebug);
        theGreenPointsINIFile.closeINI();
        return FALSE;
      }
      else
      {
        theMessage[200];
        sprintf(theMessage, "in readINISettings..valid MSR Card.Prefix.%d", i);
        logCardInfo(theMessage, FullDebug);
        theGreenPointsMSRPrefixDescriptors[i] = theEntry;
      }
    }

    //welcome GreenPoints Customer Prompt
    theEntry = theGreenPointsINIFile.get("Welcome Message", "Allow.Prompt.For.Card", "");
    if (theEntry.compareTo("") == 0)
    {
      logCardInfo("in readINISettings..no card prompt value", FullDebug);
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      if (theEntry == "YES")
      {
        logCardInfo("in readINISettings..valid card prompt value", FullDebug);
        theAllowWelcomePromptFlag = 1;
      }
      else
      {
        theMessage[200];
        sprintf(theMessage, "in readINISettings..invalid card prompt value..Allow.Prompt.For.Card: %s", theEntry);
        logCardInfo(theMessage, FullDebug);
      }
    }

    theEntry = theGreenPointsINIFile.get("Welcome Message", "Message.For.Welcome.Customer", "");
    if (theEntry.compareTo("") == 0)
    {
      logCardInfo("in readINISettings..no message defined for Welcome Message prompts", FullDebug);
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      logCardInfo("in readINISettings..valid message defined for Welcome Message prompts", FullDebug);
      ISGreenPointsApplication::instance().centerMessage(theEntry);
      theGreenPointsWelcomeMessage = theEntry;
    }

    theEntry = theGreenPointsINIFile.get("Personalization", "Phone.Number.Key", "");
    if (theEntry.compareTo("") == 0)
    {
      logCardInfo("in readINISettings..no Phone.Number.Key value", FullDebug);
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      logCardInfo("in readINISettings..valid Phone.Number.Key value", FullDebug);
      TheLocale.stringToNum(theEntry, theValue);
      thePhoneNumberKey = theValue;
    }

    //receipt welcome message
    theEntry = theGreenPointsINIFile.get("Receipt Welcome Message", "Allow.Prompt", "");
    if (theEntry.compareTo("") == 0)
    {
      logCardInfo("in readINISettings..no Receipt Welcome Message value", FullDebug);
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      if (theEntry == "YES")
      {
        logCardInfo("in readINISettings..valid Receipt Welcome Message value", FullDebug);
        theAllowReceiptWelcomePromptFlag = 1;
      }
      else
      {
        theMessage[200];
        sprintf(theMessage, "in readINISettings..invalid Receipt Welcome Value..Allow.Prompt.For.Card: %s", theEntry);
        logCardInfo(theMessage, FullDebug);
      }
    }

    theEntry = theGreenPointsINIFile.get("Receipt Welcome Message", "Message.For.Welcome.Customer", "");
    if (theEntry.compareTo("") == 0)
    {
      logCardInfo("in readINISettings..no message defined for Receipt Welcome Message", FullDebug);
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      logCardInfo("in readINISettings..valid message defined for Receipt Welcome Message", FullDebug);
      theGreenPointsReceiptWelcomeMessage = theEntry;
    }

    // the GreenPoints Phone Number Prompt
    theClearKeyAllowedFlag = 0;
    theEntry = theGreenPointsINIFile.get("Phone Messages", "Allow.Clear.Key.For.Bypass", "");
    if (theEntry.compareTo("") == 0)
    {
      logCardInfo("in readINISettings..no Allow.Clear.Key.For.Bypass", FullDebug);
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      if (theEntry == "YES")
      {
        logCardInfo("in readINISettings..valid Allow.Clear.Key.For.Bypass", FullDebug);
        theClearKeyAllowedFlag = 1;
      }
      else
      {
        theMessage[200];
        sprintf(theMessage, "in readINISettings..invalid Allow.Clear.Key.For.Bypass: %S", theEntry);
        logCardInfo(theMessage, FullDebug);
      }
    }

    theEntry = theGreenPointsINIFile.get("Phone Messages", "Message.For.Phone.Number", "");
    if (theEntry.compareTo("") == 0)
    {
      logCardInfo("in readINISettings..no Message.For.Phone.Number", FullDebug);
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      logCardInfo("in readINISettings..valid Message.For.Phone.Number", FullDebug);
      ISGreenPointsApplication::instance().centerMessage(theEntry);
      thePhoneNumberMessage = theEntry;
    }

    theEntry = theGreenPointsINIFile.get("Phone Messages", "Message.If.Clear.Key.Disabled", "");
    if (theEntry.compareTo("") == 0)
    {
      logCardInfo("in readINISettings..no Message.If.Clear.Key.Disabled", FullDebug);
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      logCardInfo("in readINISettings..valid Message.If.Clear.Key.Disabled", FullDebug);
      ISGreenPointsApplication::instance().centerMessage(theEntry);
      theDisabledClearKeyMessage = theEntry;
    }

    theEntry = theGreenPointsINIFile.get("Phone Messages", "Message.If.Invalid.Data.Entered", "");
    if (theEntry.compareTo("") == 0)
    {
      logCardInfo("in readINISettings..no Message.If.Invalid.Data.Entered", FullDebug);
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      logCardInfo("in readINISettings..valid Message.If.Invalid.Data.Entered", FullDebug);
      ISGreenPointsApplication::instance().centerMessage(theEntry);
      theInvalidDataMessage = theEntry;
    }

    //the welcome message delay
    theEntry = theGreenPointsINIFile.get("Read Message Pipe", "The.Welcome.Message.Delay", "");
    if (theEntry.compareTo("") == 0)
    {
      logCardInfo("in readINISettings..no The.Read.Message.Pipe.At.Total.Delay", FullDebug);
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      logCardInfo("in readINISettings..valid The.Read.Message.Pipe.Delay", FullDebug);
      TheLocale.stringToNum(theEntry, theValue);
      theWelcomeMessageDelay = theValue;
    }

    //phone number read delay
    theEntry = theGreenPointsINIFile.get("Phone Number", "The.Phone.Number.Read.Delay", "");
    if (theEntry.compareTo("") == 0)
    {
      logCardInfo("in readINISettings..no The.PhoneNumber.Read.Delay value", FullDebug);
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      logCardInfo("in readINISettings..valid The.PhoneNumber.Read.Delay value", FullDebug);
      TheLocale.stringToNum(theEntry, theValue);
      thePhoneNumberReadDelay = theValue;
    }

    theEntry = theGreenPointsINIFile.get("Customer Card Already Entered", "Message.For.Customer.Card.Already.Entered", "");
    if (theEntry.compareTo("") == 0)
    {
      logCardInfo("in readINISettings..noMessage.For.Customer.Card.Already.Entered", FullDebug);
      theGreenPointsINIFile.closeINI();
      return FALSE;
    }
    else
    {
      logCardInfo("in readINISettings..valid Message.For.Customer.Card.Already.Entered", FullDebug);
      ISGreenPointsApplication::instance().centerMessage(theEntry);
      theCustomerCardAlreadyEnteredMessage = theEntry;
    }

    theGreenPointsINIFile.closeINI();

    if (!isOptionEnabled)
      return FALSE;
    else
      return TRUE;
  }
  else
  {
    return FALSE;
  }
}

/****************************************************************************/
/*  set Card Number                                                         */
/****************************************************************************/
Void SAMGreenPointsCardEnhancement::setGreenPointsCardNumber(const ISString& aCardNumber)
{
  logCardInfo("in setGreenPointsCardNumber", FullDebug);
  if (aCardNumber.length() < 12)
  {
    ISString aTempSting(0x30, (13 -  aCardNumber.length()));
    ISBuffer aTempBuffer(aCardNumber);
    aTempBuffer.prepend(aTempSting);
    aTempBuffer.append("?");
    theGreenPointsCardNumber = aTempBuffer.asString();
  }
  else
    theGreenPointsCardNumber = aCardNumber;
}

/****************************************************************************/
/*  get Card number                                                         */
/****************************************************************************/
ISString SAMGreenPointsCardEnhancement::getGreenPointsCardNumber(Void)
{
  logCardInfo("in getGreenPointsCardNumber", FullDebug);
  return theGreenPointsCardNumber;
}

/****************************************************************************/
/*  set Card Number Entered                                                 */
/****************************************************************************/
Void SAMGreenPointsCardEnhancement::setGreenPointsCardEntered(Boolean aCardNumberEntered)
{
  logCardInfo("in setGreenPointsCardEntered", FullDebug);
  theGreenPointsCardNumberEntered = aCardNumberEntered;
}
/****************************************************************************/
/*  get Card number Entered                                                 */
/****************************************************************************/
Boolean SAMGreenPointsCardEnhancement::getGreenPointsCardEntered(Void)
{
  logCardInfo("in getGreenPointsCardEntered", FullDebug);
  return theGreenPointsCardNumberEntered;
}

/****************************************************************************/
/*  set Card Number message sent                                            */
/****************************************************************************/
Void SAMGreenPointsCardEnhancement::setGreenPointsCardMessageSent(Boolean anAlreadySentFlag)
{
  logCardInfo("in setGreenPointsCardMessageSent", FullDebug);
  theGreenPointsCardMessageSent = anAlreadySentFlag;
}

/****************************************************************************/
/*  get Card Number message sent                                            */
/****************************************************************************/
Boolean SAMGreenPointsCardEnhancement::getGreenPointsCardMessageSent(Void)
{
  logCardInfo("in getGreenPointsCardMessageSent", FullDebug);
  return theGreenPointsCardMessageSent;
}

/****************************************************************************/
/*  set Card Number message sent                                            */
/****************************************************************************/
Void SAMGreenPointsCardEnhancement::setBOTMessageSent(Boolean anAlreadySentBOTFlag)
{
  logCardInfo("in setBOTMessageSent", FullDebug);
  theBOTMessageSent = anAlreadySentBOTFlag;
}
/****************************************************************************/
/*  get Card Number message sent                                            */
/****************************************************************************/
Boolean SAMGreenPointsCardEnhancement::getBOTMessageSent(Void)
{
  logCardInfo("in getBOTMessageSent", FullDebug);
  return theBOTMessageSent;
}

/****************************************************************************/
/*  set Card Number                                                         */
/****************************************************************************/
Void SAMGreenPointsCardEnhancement::setKeyedOrScannedFlag(const UInt anEntryMode)
{
  logCardInfo("in setKeyedOrScannedFlag", FullDebug);
  theKeyedOrScannedFlag = anEntryMode;
}
/****************************************************************************/
/*  get Card number                                                         */
/****************************************************************************/
UInt SAMGreenPointsCardEnhancement::getKeyedOrScannedFlag(Void)
{
  logCardInfo("in getKeyedOrScannedFlag", FullDebug);
  return theKeyedOrScannedFlag;
}

/****************************************************************************/
/*  set Training Flag                                                       */
/****************************************************************************/
Void SAMGreenPointsCardEnhancement::setInTraining(Boolean aTrainingFlag)
{
  logCardInfo("in setInTraining", FullDebug);
  theTrainingFlag = aTrainingFlag;
}

/****************************************************************************/
/*  get Training Flag                                                       */
/****************************************************************************/
Boolean SAMGreenPointsCardEnhancement::getInTraining(Void)
{
  logCardInfo("in getInTraining", FullDebug);
  return theTrainingFlag;
}

/****************************************************************************/
/*  get MSR Data                                                            */
/****************************************************************************/
Boolean SAMGreenPointsCardEnhancement::hasTrack2Data (ISCardData *aCardData)
{
  logCardInfo("in hasTrack2Data", FullDebug);
  return((ISMsrCardData *)aCardData)->hasTrack(ISMsrTrack::Track2);
}

/****************************************************************************/
/*  get MSR Data                                                            */
/****************************************************************************/
ISMsrTrack2* SAMGreenPointsCardEnhancement::getTrack2Data (ISCardData *aCardData)
{
  logCardInfo("in getTrack2Data", FullDebug);
  if (hasTrack2Data (aCardData))
    return(ISMsrTrack2*) ((ISMsrCardData *)aCardData)->getTrack(ISMsrTrack::Track2);
  else
    return NULL;
}

/****************************************************************************/
/*  set MSR Data                                                            */
/****************************************************************************/
Void SAMGreenPointsCardEnhancement::setOperationFlag(UInt anOperationFlag)
{
  logCardInfo("in setOperationFlag", FullDebug);
  theOperationFlag = anOperationFlag;
}

/****************************************************************************/
/*  get MSR Data                                                            */
/****************************************************************************/
UInt SAMGreenPointsCardEnhancement::getOperationFlag(Void)
{
  logCardInfo("in getOperationFlag", FullDebug);
  return theOperationFlag;
}

Void SAMGreenPointsCardEnhancement::setDataForEnter(ISString & aKeyData)
{
  logCardInfo("in setDataForEnter", FullDebug);
  theEnterKeyData = aKeyData;
}

ISString SAMGreenPointsCardEnhancement::getDataForEnter(Void)
{
  logCardInfo("in getDataForEnter", FullDebug);
  return theEnterKeyData;
}

Void SAMGreenPointsCardEnhancement::setSlashEnteredFlag(Boolean aFlag)
{
  logCardInfo("in setSlashEnteredFlag", FullDebug);
  theSlashEnteredFlag = aFlag;
}

Boolean SAMGreenPointsCardEnhancement::getSlashEnteredFlag(Void)
{
  logCardInfo("in getSlashEnteredFlag", FullDebug);
  return theSlashEnteredFlag;
}

//A CHEC ALTID ISSUE

//***************************************************************************
// checAltGreenPointsCardInput
//
// This code is being put in as a workaround for an issue a customer is
// having with AppCard Alt IDs working with CHEC.  The customer will
// use the base ACE Alt ID functionality to determine the customer number
// and then this function will be called with the customer number being
// passed in as a parameter.
//***************************************************************************
void SAMGreenPointsCardEnhancement::checAltGreenPointsCardInput(ISString aCardNum)
{
  Boolean aMatchedPrefix = FALSE;
  for (int i = 1; i <= theNumberOfPrefixes; i++)
  {
    if (aCardNum.index(theGreenPointsPrefixDescriptors[i], 0) == 0)
    {
      aMatchedPrefix = TRUE;
      break;
    }
  }

  if (aMatchedPrefix == TRUE)
  {
    Int aRegisterId = 0;
    Int aTransactionNumber = 0;
    Char theMessage[200];
    setGreenPointsCardNumber(aCardNum);

    //card mode
    setKeyedOrScannedFlag(0);
    //training mode
    UInt inTrainingMode = ISApplication2x20::instance().isInTrainingMode();
    //register number
    ISString aRegister = ISApplication2x20::instance().terminal().getId();
    //operator number
    ISString aCurrentOperator = ISApplication2x20::instance().terminal().getCurrentOperatorId();
    //transaction number
    UInt aTransNumber = ISApplication2x20::instance().terminal().transactionNumber();
    //AD08426.1
    //if the card number is entered first, the transaction number
    //hasn't incremented yet. go ahead and increment by 1 here so the
    //messages will match up.
    UInt state = ISApplication2x20::terminal().state();
    if ((ISApplication2x20::terminal().state() == ISTerminal::BetweenTransactions) &&
        (!inTrainingMode))
      aTransNumber++;
    //ED08426.1

    //card type
    ISString aCardType = TheLocale.asString(getKeyedOrScannedFlag());
    //card number
    ISString aCardNumber = getGreenPointsCardNumber();
    if (getGreenPointsCardMessageSent() == FALSE)
    {
      if (theBeenToBOTFlag == FALSE)
      {
        if (getBOTMessageSent() == FALSE)
        {
          Boolean aRecoveryFlag = ISApplication2x20::instance().terminal().isRecoveryInProgress();
          if (aRecoveryFlag == FALSE)
          {
            //send BOT message if not sent yet
            ISString aBeginTransactionMode = TheLocale.asString(inTrainingMode).append("0");
            ISString aTempDateTime = (TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S")).remove(0, 2);
            ISGreenPointsBeginTransactionMessage * aMessage =
               ISGreenPointsMessageFactory::instance().makeBeginTransactionMessage();
            //ASHGR0007
            if (ISGreenPointsApplication::instance().isExpandedHeaderRecord())
            {
              aMessage->setExpandedHeader(ISGreenPointsApplication::instance().isExpandedHeaderRecord());
              aMessage->setRetailerId(ISGreenPointsApplication::instance().getRetailerID().asInteger());
              aMessage->setStoreId(ISGreenPointsApplication::instance().getStoreID().asInteger());
            }
            //ESHGR0007
            TheLocale.stringToNum(aRegister, aRegisterId);
            aMessage->setRegisterId(aRegisterId);
            aMessage->setTransactionNumber(aTransNumber);
            aMessage->setBeginTransactionFlag(aBeginTransactionMode);
            aMessage->setOperatorNumber(aCurrentOperator);
            aMessage->setDateTime(aTempDateTime);
            aMessage->setBeginTransactionNumber(TheLocale.asString(aTransNumber));
            aMessage->setSuspendedRegisterId("");
            aMessage->setSuspendedTransNumber("");
            aMessage->setSuspendedTransTotal("");
            aMessage->setSuspendedTaxTotal("");
            ISGreenPointsApplication::instance().writeMessage(aMessage);
            delete aMessage;
          }
          else
          {
            //send BOT message if not sent yet
            ISString aBeginTransactionMode = TheLocale.asString(inTrainingMode).append("0");
            ISString aTempDateTime = (TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S")).remove(0, 2);
            ISGreenPointsBeginTransactionMessage * aMessage =
            ISGreenPointsMessageFactory::instance().makeBeginTransactionMessage();
            //ASHGR0007
            if (ISGreenPointsApplication::instance().isExpandedHeaderRecord())
            {
              aMessage->setExpandedHeader(ISGreenPointsApplication::instance().isExpandedHeaderRecord());
              aMessage->setRetailerId(ISGreenPointsApplication::instance().getRetailerID().asInteger());
              aMessage->setStoreId(ISGreenPointsApplication::instance().getStoreID().asInteger());
            }
            //ESHGR0007
            TheLocale.stringToNum(aRegister, aRegisterId);
            aMessage->setRegisterId(aRegisterId);
            aMessage->setTransactionNumber(aTransNumber);
            aMessage->setBeginTransactionFlag(aBeginTransactionMode);
            aMessage->setOperatorNumber(aCurrentOperator);
            aMessage->setDateTime(aTempDateTime);
            aMessage->setBeginTransactionNumber(TheLocale.asString(aTransNumber));
            aMessage->setSuspendedRegisterId(aRegister);
            aMessage->setSuspendedTransNumber(TheLocale.asString(aTransNumber));
            aMessage->setSuspendedTransTotal("");
            aMessage->setSuspendedTaxTotal("");
            ISGreenPointsApplication::instance().writeMessage(aMessage);
            delete aMessage;
          }
          setBOTMessageSent(TRUE);  //D04073
        }
      }

      UInt anInputState = ISApplication2x20::instance().activeView().inputState().id();
      if (anInputState != ISInputState::PriceVerifyState)
      {
        ISGreenPointsFrequentBuyerMessage* aMessage =
        ISGreenPointsMessageFactory::instance().makeFrequentBuyerMessage();
        //ASHGR0007
        if (ISGreenPointsApplication::instance().isExpandedHeaderRecord())
        {
          aMessage->setExpandedHeader(ISGreenPointsApplication::instance().isExpandedHeaderRecord());
          aMessage->setRetailerId(ISGreenPointsApplication::instance().getRetailerID().asInteger());
          aMessage->setStoreId(ISGreenPointsApplication::instance().getStoreID().asInteger());
        }
        //ESHGR0007
        TheLocale.stringToNum(aRegister, aRegisterId);
        aMessage->setRegisterId(aRegisterId);
        aMessage->setTransactionNumber(aTransNumber);
        aMessage->setFrequentBuyerFlag(TheLocale.asString(inTrainingMode).append(aCardType));
        aMessage->setFrequentBuyerNumber(aCardNumber);
        ISGreenPointsApplication::instance().writeMessage(aMessage);
        delete aMessage;
        setGreenPointsCardMessageSent(TRUE);  //D04073
      }
      if (theAddedItemFlag == TRUE)
        ISTransaction *aTransaction = getTransaction();
    }
    setGreenPointsCardEntered(TRUE);
  }
}

//***************************************************************************
// addEMCustomerEntry event
//
// This event is being put in as a workaround for an issue a customer is
// having with AppCard Alt IDs working with CHEC.  The customer will
// use the base ACE Alt ID functionality to determine the customer number
// and then this event will be used to grab the customer number and if
// necessary pass it on to the new checAltGreenPointsCardInput function.  It
// will only be passed to that function if there is not previous card number
// or the previous card number is different.
//***************************************************************************
void SAMGreenPointsCardEnhancement::addEMCustomerEntry(ISEMCustomerEntry* aCustomerEntry)
{
  if (theOptionEnabled)
  {
    ISString aCardNum = aCustomerEntry->getID();

    // Determine if a card was previously entered.
    if (theGreenPointsCardNumberEntered)
    {
      // Since a card was previously entered, we need to format the card
      // number and compare it to the previously entered number.  If they
      // don't match, the new function should be called.
      ISString aTempCard = "";

      if (aCardNum.length() < 12)
      {
        ISString aTempSting(0x30, (13 -  aCardNum.length()));
        ISBuffer aTempBuffer(aCardNum);
        aTempBuffer.prepend(aTempSting);
        aTempBuffer.append("?");
        aTempCard = aTempBuffer.asString();
      }

      if (aTempCard != theGreenPointsCardNumber)
      {
        checAltGreenPointsCardInput(aCardNum);
      }      
    }
    else
    {
      // Since a card was NOT previously entere, the new function should be
      // called.
      checAltGreenPointsCardInput(aCardNum);
    }
  }
}

//E CHEC ALEID ISSUE

/****************************************************************************/
/*  quickCreditInput                                                        */
/****************************************************************************/
Boolean SAMGreenPointsCardEnhancement::greenPointsCardInput(ISInputSequence& anInputSequence)
{
  Boolean returnVar = FALSE;                               //D08326
  Char theMessage[200];
  Boolean aRecoveryFlag = FALSE;
  logCardInfo("entering greenPointsCardInput..", FullDebug);
  setSlashEnteredFlag(FALSE);
  if (anInputSequence.hasKey(ISKeyboardDefinition::Total) == TRUE)
    ISGreenPointsApplication::instance().setAddingTotalDiscount(TRUE);
  else
    ISGreenPointsApplication::instance().setAddingTotalDiscount(FALSE);

  if (anInputSequence.hasKey(78) == TRUE)
    setSlashEnteredFlag(TRUE);

  if (theActiveFlag == TRUE)
  {
    UInt anItemType = 0;
    if (anInputSequence.hasKey(ISKeyboardDefinition::VoidKey) == TRUE)
      anItemType += 1;
    if (anInputSequence.hasKey(ISKeyboardDefinition::Refund) == TRUE)
      anItemType += 1;
    if ((anInputSequence.hasKey(ISKeyboardDefinition::VoidKey) == TRUE) &&
        (anInputSequence.hasKey(ISKeyboardDefinition::Refund) == TRUE))
      anItemType += 1;

    setOperationFlag(anItemType);

    if (anInputSequence.source() == ISInputSequence::SourceMsr)
    {
      ISString aString = *anInputSequence.data(1);
      if ((aString != "") &&
          (aString.length() >= 12) &&
          (aString.length() <= 19))
      {
        Boolean aMatchedMSRPrefix = FALSE;
        for (int i = 1; i <= theNumberOfMSRPrefixes; i++)
        {
          if (aString.index(theGreenPointsMSRPrefixDescriptors[i], 0) == 0)
          {
            aMatchedMSRPrefix = TRUE;
            i = theNumberOfMSRPrefixes + 1;
          }
        }
        if (aMatchedMSRPrefix == TRUE)
        {
          if ((theAllowWelcomePromptFlag == 1) &&
              (aWelcomeMessageSent == FALSE))
          {
            ISPromptDetails* thePrompt = ISGuidanceFactory::instance().makePromptDetails(theGreenPointsWelcomeMessage, "");
            thePrompt->setClearKeyRequired(FALSE);
            thePrompt->setErrorToneRequired(FALSE);
            thePrompt->setRestoreDisplayRequired(FALSE);
            ISPromptUserEvent::raiseWith(thePrompt);
            delete thePrompt;
            aWelcomeMessageSent = TRUE;   //D04073
          }
          Int aRegisterId = 0;
          setGreenPointsCardNumber(aString);
          //training mode
          UInt inTrainingMode = ISApplication2x20::instance().isInTrainingMode();
          //register number
          ISString aRegister = ISApplication2x20::instance().terminal().getId();
          //operator number
          ISString aCurrentOperator = ISApplication2x20::instance().terminal().getCurrentOperatorId();
          //transaction number
          UInt aTransNumber = ISApplication2x20::instance().terminal().transactionNumber();
          //AD08426.1
          //if the card number is entered first, the transaction number
          //hasn't incremented yet. go ahead and increment by 1 here so the
          //messages will match up and only if we're not in training
          UInt state = ISApplication2x20::terminal().state();
          if ((ISApplication2x20::terminal().state() == ISTerminal::BetweenTransactions) &&
              (!inTrainingMode))
            aTransNumber++;
          //ED08426.1

          //card type
          ISString aCardType = TheLocale.asString(getKeyedOrScannedFlag());
          //card number
          ISString aCardNumber = getGreenPointsCardNumber();
          if (getGreenPointsCardMessageSent() == FALSE)
          {
            if (theBeenToBOTFlag == FALSE)
            {
              if (getBOTMessageSent() == FALSE)
              {
                aRecoveryFlag = ISApplication2x20::instance().terminal().isRecoveryInProgress();
                if (aRecoveryFlag == FALSE)
                {
                  //send BOT message if not sent yet
                  ISString aBeginTransactionMode = TheLocale.asString(inTrainingMode).append("0");
                  ISString aTempDateTime = (TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S")).remove(0, 2);
                  ISGreenPointsBeginTransactionMessage * aMessage =
                    ISGreenPointsMessageFactory::instance().makeBeginTransactionMessage();
                  //ASHGR0007
                  if (ISGreenPointsApplication::instance().isExpandedHeaderRecord())
                  {
                    aMessage->setExpandedHeader(ISGreenPointsApplication::instance().isExpandedHeaderRecord());
                    aMessage->setRetailerId(ISGreenPointsApplication::instance().getRetailerID().asInteger());
                    aMessage->setStoreId(ISGreenPointsApplication::instance().getStoreID().asInteger());
                  }
                  //ESHGR0007
                  TheLocale.stringToNum(aRegister, aRegisterId);
                  aMessage->setRegisterId(aRegisterId);
                  aMessage->setTransactionNumber(aTransNumber);
                  aMessage->setBeginTransactionFlag(aBeginTransactionMode);
                  aMessage->setOperatorNumber(aCurrentOperator);
                  aMessage->setDateTime(aTempDateTime);
                  aMessage->setBeginTransactionNumber(TheLocale.asString(aTransNumber));
                  aMessage->setSuspendedRegisterId("");
                  aMessage->setSuspendedTransNumber("");
                  aMessage->setSuspendedTransTotal("");
                  aMessage->setSuspendedTaxTotal("");
                  ISGreenPointsApplication::instance().writeMessage(aMessage);
                  delete aMessage;
                }
                else
                {
                  //send BOT message if not sent yet
                  ISString aBeginTransactionMode = TheLocale.asString(inTrainingMode).append("0");
                  ISString aTempDateTime = (TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S")).remove(0, 2);
                  ISGreenPointsBeginTransactionMessage * aMessage =
                    ISGreenPointsMessageFactory::instance().makeBeginTransactionMessage();
                  //ASHGR0007
                  if (ISGreenPointsApplication::instance().isExpandedHeaderRecord())
                  {
                    aMessage->setExpandedHeader(ISGreenPointsApplication::instance().isExpandedHeaderRecord());
                    aMessage->setRetailerId(ISGreenPointsApplication::instance().getRetailerID().asInteger());
                    aMessage->setStoreId(ISGreenPointsApplication::instance().getStoreID().asInteger());
                  }
                  //ESHGR0007
                  TheLocale.stringToNum(aRegister, aRegisterId);
                  aMessage->setRegisterId(aRegisterId);
                  aMessage->setTransactionNumber(aTransNumber);
                  aMessage->setBeginTransactionFlag(aBeginTransactionMode);
                  aMessage->setOperatorNumber(aCurrentOperator);
                  aMessage->setDateTime(aTempDateTime);
                  aMessage->setBeginTransactionNumber(TheLocale.asString(aTransNumber));
                  aMessage->setSuspendedRegisterId(aRegister);
                  aMessage->setSuspendedTransNumber(TheLocale.asString(aTransNumber));
                  aMessage->setSuspendedTransTotal("");
                  aMessage->setSuspendedTaxTotal("");
                  ISGreenPointsApplication::instance().writeMessage(aMessage);
                  delete aMessage;
                }
                setBOTMessageSent(TRUE);  //D04073
              }
            }
            UInt anInputState = ISApplication2x20::instance().activeView().inputState().id();
            if (anInputState != ISInputState::PriceVerifyState)
            {
              ISGreenPointsFrequentBuyerMessage* aMessage =
                ISGreenPointsMessageFactory::instance().makeFrequentBuyerMessage();
              //ASHGR0007
              if (ISGreenPointsApplication::instance().isExpandedHeaderRecord())
              {
                aMessage->setExpandedHeader(ISGreenPointsApplication::instance().isExpandedHeaderRecord());
                aMessage->setRetailerId(ISGreenPointsApplication::instance().getRetailerID().asInteger());
                aMessage->setStoreId(ISGreenPointsApplication::instance().getStoreID().asInteger());
              }
              //ESHGR0007
              TheLocale.stringToNum(aRegister, aRegisterId);
              aMessage->setRegisterId(aRegisterId);
              aMessage->setTransactionNumber(aTransNumber);
              aMessage->setFrequentBuyerFlag(TheLocale.asString(inTrainingMode).append(aCardType));
              aMessage->setFrequentBuyerNumber(aCardNumber);
              ISGreenPointsApplication::instance().writeMessage(aMessage);
              delete aMessage;
              setGreenPointsCardMessageSent(TRUE);  //D04073
            }
          }
          //AD08326
          //return TRUE;
          returnVar = TRUE;
          //ED08326
        }
      }
    }

    ISString dataForKey = anInputSequence.dataForKey(ISKeyboardDefinition::SignOn);
    if ((dataForKey != "") &&
        (dataForKey == "7") &&
        (theBeenToBOTFlag == FALSE) &&
        (anInputSequence.hasKey(thePhoneNumberKey) == FALSE))
    {
      setInTraining(TRUE);
      //register number
      ISString aRegister = ISApplication2x20::instance().terminal().getId();
      //transaction number
      UInt aTransNumber = ISApplication2x20::instance().terminal().transactionNumber();
      //AD08426.1
      //if the card number is entered first, the transaction number
      //hasn't incremented yet. go ahead and increment by 1 here so the
      //messages will match up.
      UInt state = ISApplication2x20::terminal().state();
      if ((ISApplication2x20::terminal().state() == ISTerminal::BetweenTransactions) &&
          (!getInTraining()))
        aTransNumber++;
      //ED08426.1

      //operator number
      ISString aCurrentOperator = ISApplication2x20::instance().terminal().getCurrentOperatorId();
      ISString theTempDateTime = TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S").remove(0, 2);
      UInt aTrainingMode = getInTraining();
      ISString aSignOnMode = TheLocale.asString(aTrainingMode).append("0");
      Int aRegisterId = 0;
      ISGreenPointsSignOnMessage* aMessage =
        ISGreenPointsMessageFactory::instance().makeSignOnMessage();
      //ASHGR0007
      if (ISGreenPointsApplication::instance().isExpandedHeaderRecord())
      {
        aMessage->setExpandedHeader(ISGreenPointsApplication::instance().isExpandedHeaderRecord());
        aMessage->setRetailerId(ISGreenPointsApplication::instance().getRetailerID().asInteger());
        aMessage->setStoreId(ISGreenPointsApplication::instance().getStoreID().asInteger());
      }
      //ESHGR0007
      TheLocale.stringToNum(aRegister, aRegisterId);
      aMessage->setRegisterId(aRegisterId);
      aMessage->setTransactionNumber(aTransNumber);
      aMessage->setSignOnFlag(aSignOnMode);
      aMessage->setOperatorNumber(aCurrentOperator);
      aMessage->setDateTime(theTempDateTime);
      ISGreenPointsApplication::instance().writeMessage(aMessage);
      delete aMessage;
      //AD08326
      //return FALSE;
      returnVar = FALSE;
      //ED08326
    }

    dataForKey = anInputSequence.dataForKey(ISKeyboardDefinition::Enter);
    setDataForEnter(dataForKey);
    if ((dataForKey != "") &&
        (anInputSequence.hasKey(thePhoneNumberKey) == FALSE))
    {
      if ((dataForKey.length() == 11) ||
          (dataForKey.length() == 12))
      {
        Boolean aMatchedPrefix = FALSE;
        for (int i = 1; i <= theNumberOfPrefixes; i++)
        {
          if (dataForKey.index(theGreenPointsPrefixDescriptors[i], 0) == 0)
          {
            aMatchedPrefix = TRUE;
            break;
          }
        }
        if (aMatchedPrefix == TRUE)
        {
          Int aRegisterId = 0;
          Int aTransactionNumber = 0;
          Char theMessage[200];
          setGreenPointsCardNumber(dataForKey);
          if ((theAllowWelcomePromptFlag == 1) &&
              (aWelcomeMessageSent == FALSE))
          {
            ISPromptDetails* thePrompt = ISGuidanceFactory::instance().makePromptDetails(theGreenPointsWelcomeMessage, "");
            thePrompt->setClearKeyRequired(FALSE);
            thePrompt->setErrorToneRequired(FALSE);
            thePrompt->setRestoreDisplayRequired(FALSE);
            ISPromptUserEvent::raiseWith(thePrompt);
            delete thePrompt;
            aWelcomeMessageSent = TRUE;  //D04073
          }

          //card mode
          if (anInputSequence.source() == ISInputSequence::SourceMsr)
            setKeyedOrScannedFlag(1);
          else
          {
            if ((anInputSequence.source() == ISInputSequence::SourceScanner) ||
                (anInputSequence.source() == ISInputSequence::SourceKeyboard))
              setKeyedOrScannedFlag(0);
          }

          //training mode
          UInt inTrainingMode = ISApplication2x20::instance().isInTrainingMode();
          //register number
          ISString aRegister = ISApplication2x20::instance().terminal().getId();
          //operator number
          ISString aCurrentOperator = ISApplication2x20::instance().terminal().getCurrentOperatorId();
          //transaction number
          UInt aTransNumber = ISApplication2x20::instance().terminal().transactionNumber();
          //AD08426.1
          //if the card number is entered first, the transaction number
          //hasn't incremented yet. go ahead and increment by 1 here so the
          //messages will match up.
          UInt state = ISApplication2x20::terminal().state();
          if ((ISApplication2x20::terminal().state() == ISTerminal::BetweenTransactions) &&
              (!inTrainingMode))
            aTransNumber++;
          //ED08426.1

          //card type
          ISString aCardType = TheLocale.asString(getKeyedOrScannedFlag());
          //card number
          ISString aCardNumber = getGreenPointsCardNumber();
          if (getGreenPointsCardMessageSent() == FALSE)
          {
            if (theBeenToBOTFlag == FALSE)
            {
              if (getBOTMessageSent() == FALSE)
              {
                aRecoveryFlag = ISApplication2x20::instance().terminal().isRecoveryInProgress();
                if (aRecoveryFlag == FALSE)
                {
                  //send BOT message if not sent yet
                  ISString aBeginTransactionMode = TheLocale.asString(inTrainingMode).append("0");
                  ISString aTempDateTime = (TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S")).remove(0, 2);
                  ISGreenPointsBeginTransactionMessage * aMessage =
                    ISGreenPointsMessageFactory::instance().makeBeginTransactionMessage();
                  //ASHGR0007
                  if (ISGreenPointsApplication::instance().isExpandedHeaderRecord())
                  {
                    aMessage->setExpandedHeader(ISGreenPointsApplication::instance().isExpandedHeaderRecord());
                    aMessage->setRetailerId(ISGreenPointsApplication::instance().getRetailerID().asInteger());
                    aMessage->setStoreId(ISGreenPointsApplication::instance().getStoreID().asInteger());
                  }
                  //ESHGR0007
                  TheLocale.stringToNum(aRegister, aRegisterId);
                  aMessage->setRegisterId(aRegisterId);
                  aMessage->setTransactionNumber(aTransNumber);
                  aMessage->setBeginTransactionFlag(aBeginTransactionMode);
                  aMessage->setOperatorNumber(aCurrentOperator);
                  aMessage->setDateTime(aTempDateTime);
                  aMessage->setBeginTransactionNumber(TheLocale.asString(aTransNumber));
                  aMessage->setSuspendedRegisterId("");
                  aMessage->setSuspendedTransNumber("");
                  aMessage->setSuspendedTransTotal("");
                  aMessage->setSuspendedTaxTotal("");
                  ISGreenPointsApplication::instance().writeMessage(aMessage);
                  delete aMessage;
                }
                else
                {
                  //send BOT message if not sent yet
                  ISString aBeginTransactionMode = TheLocale.asString(inTrainingMode).append("0");
                  ISString aTempDateTime = (TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S")).remove(0, 2);
                  ISGreenPointsBeginTransactionMessage * aMessage =
                    ISGreenPointsMessageFactory::instance().makeBeginTransactionMessage();
                  //ASHGR0007
                  if (ISGreenPointsApplication::instance().isExpandedHeaderRecord())
                  {
                    aMessage->setExpandedHeader(ISGreenPointsApplication::instance().isExpandedHeaderRecord());
                    aMessage->setRetailerId(ISGreenPointsApplication::instance().getRetailerID().asInteger());
                    aMessage->setStoreId(ISGreenPointsApplication::instance().getStoreID().asInteger());
                  }
                  //ESHGR0007
                  TheLocale.stringToNum(aRegister, aRegisterId);
                  aMessage->setRegisterId(aRegisterId);
                  aMessage->setTransactionNumber(aTransNumber);
                  aMessage->setBeginTransactionFlag(aBeginTransactionMode);
                  aMessage->setOperatorNumber(aCurrentOperator);
                  aMessage->setDateTime(aTempDateTime);
                  aMessage->setBeginTransactionNumber(TheLocale.asString(aTransNumber));
                  aMessage->setSuspendedRegisterId(aRegister);
                  aMessage->setSuspendedTransNumber(TheLocale.asString(aTransNumber));
                  aMessage->setSuspendedTransTotal("");
                  aMessage->setSuspendedTaxTotal("");
                  ISGreenPointsApplication::instance().writeMessage(aMessage);
                  delete aMessage;
                }
                setBOTMessageSent(TRUE);  //D04073
              }
            }

            UInt anInputState = ISApplication2x20::instance().activeView().inputState().id();
            if (anInputState != ISInputState::PriceVerifyState)
            {
              ISGreenPointsFrequentBuyerMessage* aMessage =
                ISGreenPointsMessageFactory::instance().makeFrequentBuyerMessage();
              //ASHGR0007
              if (ISGreenPointsApplication::instance().isExpandedHeaderRecord())
              {
                aMessage->setExpandedHeader(ISGreenPointsApplication::instance().isExpandedHeaderRecord());
                aMessage->setRetailerId(ISGreenPointsApplication::instance().getRetailerID().asInteger());
                aMessage->setStoreId(ISGreenPointsApplication::instance().getStoreID().asInteger());
              }
              //ESHGR0007
              TheLocale.stringToNum(aRegister, aRegisterId);
              aMessage->setRegisterId(aRegisterId);
              aMessage->setTransactionNumber(aTransNumber);
              aMessage->setFrequentBuyerFlag(TheLocale.asString(inTrainingMode).append(aCardType));
              aMessage->setFrequentBuyerNumber(aCardNumber);
              ISGreenPointsApplication::instance().writeMessage(aMessage);
              delete aMessage;
              setGreenPointsCardMessageSent(TRUE);  //D04073
            }
            if (theAddedItemFlag == TRUE)
              ISTransaction *aTransaction = getTransaction();
          }
          setGreenPointsCardEntered(TRUE);
          //AD08326
          //return TRUE;
          returnVar = TRUE;
          //ED08326
        }
      }
    }

    if (anInputSequence.hasKey(thePhoneNumberKey) == TRUE)
    {
      ISString theCapturedData = "";
      Boolean theValidInput = FALSE;
      Boolean theValidateInput = TRUE;
      if (getGreenPointsCardMessageSent() == FALSE)
      {
        while (theValidInput == FALSE)
        {
          if (theCapturedData.isNull() == TRUE)
          {
            theValidInput = FALSE;
            ISPromptDetails* thePrompt = ISGuidanceFactory::instance().makePromptDetails(thePhoneNumberMessage, "");
            thePrompt->setDataEntryRequired(TRUE);
            thePrompt->setRestoreDisplayRequired(TRUE);
            //AGUI
            UInt aState = 601001;
            ISSequencingStateChangedEvent::raiseWith(&aState);
            //EGUI
            ISPromptUserEvent::raiseWith(thePrompt);
            UInt theKey = thePrompt->getKeyPressed();
            if (theKey == 0)
            {
              theCapturedData = "";
              theValidateInput = FALSE;
              if (theClearKeyAllowedFlag == 0)
              {
                ISPromptDetails* thePrompt = ISGuidanceFactory::instance().makePromptDetails(theDisabledClearKeyMessage, "");
                thePrompt->setClearKeyRequired(TRUE);
                thePrompt->setErrorToneRequired(TRUE);
                thePrompt->setRestoreDisplayRequired(TRUE);
                ISPromptUserEvent::raiseWith(thePrompt);
                delete thePrompt;
              }
              else
              {
                theValidInput = TRUE;
                anInputSequence.motorKey(73);    //D08346
              }
            }
            else if (theKey == ISKeyboardDefinition::Enter)
            {
              theCapturedData= thePrompt->getDataEntered();
              if (theCapturedData.isNull() == TRUE)
              {
                theCapturedPhoneNumber = "";
                theValidateInput = FALSE;
                theValidInput = TRUE;
              }
              else
              {
                theCapturedPhoneNumber = theCapturedData;
                theValidateInput = TRUE;
              }
            }
            delete thePrompt;
          }
          if (theValidateInput == TRUE)
          {
            if ((theCapturedData.length() == 10) &&
                (theCapturedData.first('0') != 0))
            {
              theValidInput = TRUE;
              theCapturedPhoneNumber = theCapturedData;
            }
            else
            {
              ISPromptDetails* thePrompt = ISGuidanceFactory::instance().makePromptDetails(theInvalidDataMessage, "");
              thePrompt->setClearKeyRequired(TRUE);
              thePrompt->setErrorToneRequired(TRUE);
              thePrompt->setRestoreDisplayRequired(TRUE);
              ISPromptUserEvent::raiseWith(thePrompt);
              delete thePrompt;
              theCapturedData = "";
            }
          }
        }

        clearScreen();
        if ((theCapturedData.isNull() == FALSE) &&
            (theCapturedData.length() == 10) &&
            (theValidInput == TRUE))
        {
          //operator number
          ISString aCurrentOperator = ISApplication2x20::instance().terminal().getCurrentOperatorId();
          Int aRegisterId = 0;
          //training mode
          UInt inTrainingMode = ISApplication2x20::instance().isInTrainingMode();
          //register number
          ISString aRegister = ISApplication2x20::instance().terminal().getId();
          //transaction number
          UInt aTransNumber = ISApplication2x20::instance().terminal().transactionNumber();
          //AD08426.1
          //if the card number is entered first, the transaction number
          //hasn't incremented yet. go ahead and increment by 1 here so the
          //messages will match up.
          UInt state = ISApplication2x20::terminal().state();
          if ((ISApplication2x20::terminal().state() == ISTerminal::BetweenTransactions) &&
              (!inTrainingMode))
            aTransNumber++;
          //ED08426.1
          if (theBeenToBOTFlag == FALSE)
          {
            if (getBOTMessageSent() == FALSE)
            {
              aRecoveryFlag = ISApplication2x20::instance().terminal().isRecoveryInProgress();
              if (aRecoveryFlag = FALSE)
              {
                //send BOT message if not sent yet
                ISString aBeginTransactionMode = TheLocale.asString(inTrainingMode).append("0");
                ISString aTempDateTime = (TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S")).remove(0, 2);
                ISGreenPointsBeginTransactionMessage * aMessage =
                  ISGreenPointsMessageFactory::instance().makeBeginTransactionMessage();
                //ASHGR0007
                if (ISGreenPointsApplication::instance().isExpandedHeaderRecord())
                {
                  aMessage->setExpandedHeader(ISGreenPointsApplication::instance().isExpandedHeaderRecord());
                  aMessage->setRetailerId(ISGreenPointsApplication::instance().getRetailerID().asInteger());
                  aMessage->setStoreId(ISGreenPointsApplication::instance().getStoreID().asInteger());
                }
                //ESHGR0007
                TheLocale.stringToNum(aRegister, aRegisterId);
                aMessage->setRegisterId(aRegisterId);
                aMessage->setTransactionNumber(aTransNumber);
                aMessage->setBeginTransactionFlag(aBeginTransactionMode);
                aMessage->setOperatorNumber(aCurrentOperator);
                aMessage->setDateTime(aTempDateTime);
                aMessage->setBeginTransactionNumber(TheLocale.asString(aTransNumber));
                aMessage->setSuspendedRegisterId("");
                aMessage->setSuspendedTransNumber("");
                aMessage->setSuspendedTransTotal("");
                aMessage->setSuspendedTaxTotal("");
                ISGreenPointsApplication::instance().writeMessage(aMessage);
                delete aMessage;
              }
              else
              {
                //send BOT message if not sent yet
                ISString aBeginTransactionMode = TheLocale.asString(inTrainingMode).append("0");
                ISString aTempDateTime = (TheLocale.asString(ISTime::now(),"%Y%m%d%H%M%S")).remove(0, 2);

                ISGreenPointsBeginTransactionMessage * aMessage =
                  ISGreenPointsMessageFactory::instance().makeBeginTransactionMessage();
                //ASHGR0007
                if (ISGreenPointsApplication::instance().isExpandedHeaderRecord())
                {
                  aMessage->setExpandedHeader(ISGreenPointsApplication::instance().isExpandedHeaderRecord());
                  aMessage->setRetailerId(ISGreenPointsApplication::instance().getRetailerID().asInteger());
                  aMessage->setStoreId(ISGreenPointsApplication::instance().getStoreID().asInteger());
                }
                //ESHGR0007
                TheLocale.stringToNum(aRegister, aRegisterId);
                aMessage->setRegisterId(aRegisterId);
                aMessage->setTransactionNumber(aTransNumber);
                aMessage->setBeginTransactionFlag(aBeginTransactionMode);
                aMessage->setOperatorNumber(aCurrentOperator);
                aMessage->setDateTime(aTempDateTime);
                aMessage->setBeginTransactionNumber(TheLocale.asString(aTransNumber));
                aMessage->setSuspendedRegisterId(aRegister);
                aMessage->setSuspendedTransNumber(TheLocale.asString(aTransNumber));
                aMessage->setSuspendedTransTotal("");
                aMessage->setSuspendedTaxTotal("");
                ISGreenPointsApplication::instance().writeMessage(aMessage);
                delete aMessage;
              }
              setBOTMessageSent(TRUE);  //D04073
            }
          }
          ISGreenPointsPhoneNumberMessage * aMessage =
            ISGreenPointsMessageFactory::instance().makePhoneNumberMessage();
          //ASHGR0007
          if (ISGreenPointsApplication::instance().isExpandedHeaderRecord())
          {
            aMessage->setExpandedHeader(ISGreenPointsApplication::instance().isExpandedHeaderRecord());
            aMessage->setRetailerId(ISGreenPointsApplication::instance().getRetailerID().asInteger());
            aMessage->setStoreId(ISGreenPointsApplication::instance().getStoreID().asInteger());
          }
          //ESHGR0007
          TheLocale.stringToNum(aRegister, aRegisterId);
          aMessage->setRegisterId(aRegisterId);
          aMessage->setTransactionNumber(aTransNumber);
          aMessage->setPhoneNumberFlag(TheLocale.asString(inTrainingMode).append("0"));
          aMessage->setPhoneNumber(theCapturedPhoneNumber);
          ISGreenPointsApplication::instance().writeMessage(aMessage);
          delete aMessage;

          ISGreenPointsApplication::instance().readMessage(thePhoneNumberReadDelay);
          //AD08346
          anInputSequence.motorKey(73);
          if (ISEMCustomerOptionsCache::instance().isEMPreferredActive())
          {
            ISGreenPointsApplication::instance().altIDHasBeenDone = TRUE;
          }
          //ED08346
        }
        //AD08326
        //return TRUE;
        returnVar = TRUE;
        //ED08326
      }
      else
      {
        ISPromptDetails* thePrompt = ISGuidanceFactory::instance().makePromptDetails(theCustomerCardAlreadyEnteredMessage, "");
        thePrompt->setClearKeyRequired(TRUE);
        thePrompt->setErrorToneRequired(TRUE);
        thePrompt->setRestoreDisplayRequired(TRUE);
        ISPromptUserEvent::raiseWith(thePrompt);
        delete thePrompt;
      }
      //AD08326
      //return TRUE;
      returnVar = TRUE;
      //ED08326
    }
    //AD08326
    //return FALSE;
    //ED08326
  }
  //AD08326
  //return FALSE;
  if (ISEMCustomerOptionsCache::instance().isEMPreferredActive())
  {
    returnVar = FALSE;
  }
  return returnVar;
  //ED08326
}

/****************************************************************************/
/*  Clear the screen after the prompt.                                      */
/****************************************************************************/
void SAMGreenPointsCardEnhancement::clearScreen(Void)
{
  logCardInfo("in clearScreen", FullDebug);
  ISPromptDetails* clearPrompt = ISGuidanceFactory::instance().
                                 makePromptDetails();
  if (clearPrompt != 0)
  {
    clearPrompt->setRestoreDisplayRequired(TRUE);
    clearPrompt->setClearKeyRequired(FALSE);
    ISPromptUserEvent::raiseWith(clearPrompt);
    delete clearPrompt;
  }
}

/****************************************************************************/
/*  printApplication                                                        */
/****************************************************************************/
Void SAMGreenPointsCardEnhancement::printApplication()
{
  if (theActiveFlag == TRUE)
  {
  }
}

/****************************************************************************/
/*  optionsLoaded                                                           */
/****************************************************************************/
Void SAMGreenPointsCardEnhancement::optionsLoaded(ISOptions* options)
{
  cancelSubscriptionToStartTransaction(this);
  cancelSubscriptionToAddItemEntry(this);
  cancelSubscriptionToEndTransaction(this);
  cancelSubscriptionToOperatorSignedOff(this);
  cancelSubscriptionToEMCustomerEntry(this);

  // Read Configuration
  if (readINISettings() == TRUE)
  {
    theActiveFlag = TRUE;
    subscribeToStartTransaction(this);
    subscribeToAddItemEntry(this);
    subscribeToEndTransaction(this);
    subscribeToEMCustomerEntry(this);
    subscribeToOperatorSignedOff(this, ISEventSubscriber::LowestPriority);
    logCardInfo("Option is Enabled", FullDebug);
  }
  else
  {
    logCardInfo("Option is Disabled", FullDebug);
    theActiveFlag = FALSE;
  }
}

/****************************************************************************/
/*  save Transaction                                                        */
/****************************************************************************/
Void SAMGreenPointsCardEnhancement::setTransaction(ISTransaction *aTransaction)
{
  logCardInfo("in setTransaction", FullDebug);
  theTransaction = aTransaction;
}

/****************************************************************************/
/*  get Transaction                                                        */
/****************************************************************************/
ISTransaction* SAMGreenPointsCardEnhancement::getTransaction(Void)
{
  logCardInfo("in getTransaction", FullDebug);
  return theTransaction;
}

/****************************************************************************/
/*  startTransaction                                                        */
/****************************************************************************/
Void SAMGreenPointsCardEnhancement::startTransaction(ISTransaction* aTransaction)
{
  if (theActiveFlag == TRUE)
  {
    logCardInfo("in SAMGreenPointsCardEnhancement::startTransaction", FullDebug);
    theBeenToBOTFlag = TRUE;
    setTransaction(aTransaction);
  }
}

/****************************************************************************/
/*  Add Item Entry                                                          */
/****************************************************************************/
Void SAMGreenPointsCardEnhancement::addItemEntry(ISItemEntry * anItemEntry)
{
  logCardInfo("in addItemEntry..theAddedItemFlag = TRUE", FullDebug);
  theAddedItemFlag = TRUE;
  ISGreenPointsApplication::instance().setTheItemEntered(TRUE); //D01125
}

/****************************************************************************/
/*  endTransaction                                                          */
/****************************************************************************/
Void SAMGreenPointsCardEnhancement::endTransaction(ISTransaction* aTransaction)
{
  if (theActiveFlag == TRUE)
  {
    logCardInfo("in SAMGreenPointsCardEnhancement::endTransaction", FullDebug);
    theGreenPointsCardFound = FALSE;
    setGreenPointsCardEntered(FALSE);
    setGreenPointsCardMessageSent(FALSE);
    setBOTMessageSent(FALSE);
    setGreenPointsCardNumber("");
    theKeyedOrScannedFlag = 0;
    alreadyChecked = 0;
    theBeenToBOTFlag = FALSE;
    aWelcomeMessageSent = FALSE;
    theAddedItemFlag = FALSE;
    theCapturedPhoneNumber = "";
    ISGreenPointsApplication::instance().setTheItemEntered(FALSE); //D01125
  }
}

/****************************************************************************/
/*  endTransaction                                                          */
/****************************************************************************/

Void SAMGreenPointsCardEnhancement::operatorSignedOff(ISWorkstation* aWorkstation)
{
  logCardInfo("in SAMGreenPointsCardEnhancement::operatorSignedOff", FullDebug);
  setInTraining(FALSE);
}
tenfold
